package data.daos;

import business.interfaces.data.DAO;
import data.database.DatabaseQueryExecutor;
import data.database.EntityFromResultSetCreator;
import data.database.generators.TicketSaleItemQuerySentenceGenerator;
import entities.TicketSaleItem;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Allows database interaction with persistent entity TicketSaleItem.
 *
 * @author Erick López
 */
public class TicketSaleItemDAO implements DAO<TicketSaleItem> {

    public static String TABLE_TICKETSALE_ITEMS = "TICKETSALE_ITEMS";
    private final TicketSaleItemQuerySentenceGenerator sentenceGenerator;
    private final DatabaseQueryExecutor queryExecutor;
    
    public TicketSaleItemDAO() {
        sentenceGenerator = new TicketSaleItemQuerySentenceGenerator(TABLE_TICKETSALE_ITEMS);
        queryExecutor = new DatabaseQueryExecutor();
    }
    
    @Override
    public boolean insert(TicketSaleItem newItem) {
        String insertTicketSaleItemSentence;
        insertTicketSaleItemSentence = sentenceGenerator.generateInsertTicketSaleItemSentence(newItem);  
        System.out.println(insertTicketSaleItemSentence);
        boolean insertDone = true;
        try {
            queryExecutor.close();
            queryExecutor.executeInsertTicketItemQuery(insertTicketSaleItemSentence);            
        } catch (SQLException ex) {
            insertDone = false;
            Logger.getLogger(TicketSaleItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertDone;
    }  
    
    @Override
    public boolean delete(int existingItemID) {
        String deleteItemSentence;
        deleteItemSentence = sentenceGenerator.generateDeleteItemByIdSentence(existingItemID);
        boolean deleteDone = false;
        try {
            deleteDone = queryExecutor.executeDeleteOperation(deleteItemSentence);
        } catch (SQLException ex) {
            Logger.getLogger(TicketSaleItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return deleteDone;
    }    

    public TicketSaleItem[] getList(String findAllItemsSentence) {       
        System.out.println(findAllItemsSentence);
        ArrayList<TicketSaleItem> items = new ArrayList<>();                                    
        try {
            ResultSet resultSet = queryExecutor.executeFindFromTableOperation(findAllItemsSentence);
            while (resultSet.next()) {        
                TicketSaleItem item = EntityFromResultSetCreator.createTicketSaleItemFromResultSet(resultSet);
                items.add(item);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TicketSaleItem.class.getName()).log(Level.SEVERE, null, ex);
        }

        return items.toArray(new TicketSaleItem[items.size()]);
    }   
    
    //TODO: Evaluate whether this methods are relevant for this class abstraction.    
    @Override
    public boolean update(TicketSaleItem updatedItem) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public TicketSaleItem getById(int entityID) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public TicketSaleItem[] getList() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

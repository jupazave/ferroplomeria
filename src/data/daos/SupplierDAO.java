package data.daos;

import business.interfaces.data.DAO;
import data.database.DatabaseQueryExecutor;
import data.database.EntityFromResultSetCreator;
import data.database.generators.SupplierQuerySentenceGenerator;
import entities.Supplier;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Allows database interaction with persistent entity Supplier.
 *
 * @author Erick López
 */
public class SupplierDAO implements DAO<Supplier> {

    public static String TABLE_SUPPLIERS = "SUPPLIERS";    
    
    private final SupplierQuerySentenceGenerator sentenceGenerator;
    private final DatabaseQueryExecutor queryExecutor;

    public SupplierDAO() {
        sentenceGenerator = new SupplierQuerySentenceGenerator(TABLE_SUPPLIERS);
        queryExecutor = new DatabaseQueryExecutor();
    }
    
    @Override
    public boolean insert(Supplier newSupplier) {
        String insertSupplierSentence;
        insertSupplierSentence = sentenceGenerator.generateInsertSupplierSentence(newSupplier);               
        boolean insertDone = false;
        try {
            insertDone = queryExecutor.executeInsertQuery(insertSupplierSentence);
        } catch (SQLException ex) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertDone;
    }

    @Override
    public boolean delete(int existingSupplierID) {
        String deleteSupplierSentence;
        deleteSupplierSentence = sentenceGenerator.generateDeleteSupplierByIdSentence(existingSupplierID);
        boolean deleteDone = false;
        try {
            deleteDone = queryExecutor.executeDeleteOperation(deleteSupplierSentence);
        } catch (SQLException ex) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return deleteDone;
    }

    @Override
    public boolean update(Supplier updatedSupplier) {
        String updateSupplierSentence;
        updateSupplierSentence = sentenceGenerator.generateUpdateSupplierSentence(updatedSupplier);        
        boolean updateDone = false;
        try {
            updateDone = queryExecutor.executeUpdateQuery(updateSupplierSentence);
        } catch (SQLException ex) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return updateDone;
    }

    @Override
    public Supplier getById(int supplierID) {
        String findSupplierByIdSentence;
        findSupplierByIdSentence = sentenceGenerator.generateFindSupplierByIdSentence(supplierID);        
        Supplier supplier = null;
        try {
            ResultSet resultSet = queryExecutor.executeFindByIdQuery(findSupplierByIdSentence);
            while (resultSet.next()) {
                supplier = EntityFromResultSetCreator.createSupplierFromResultSet(resultSet);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return supplier;
    }

    @Override
    public Supplier[] getList() {
        String findAllSuppliersSentence;
        findAllSuppliersSentence = sentenceGenerator.generateFindAllSuppliersSentence();
        ArrayList<Supplier> suppliers = new ArrayList<>();
        Supplier currentSupplier;
        try {
            ResultSet resultSet = queryExecutor.executeFindFromTableOperation(findAllSuppliersSentence);
            while (resultSet.next()) {
                currentSupplier = EntityFromResultSetCreator.createSupplierFromResultSet(resultSet);
                suppliers.add(currentSupplier);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return suppliers.toArray(new Supplier[suppliers.size()]);
    }

}

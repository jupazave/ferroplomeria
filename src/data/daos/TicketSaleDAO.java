package data.daos;

import business.interfaces.data.DAO;
import data.database.DatabaseQueryExecutor;
import data.database.EntityFromResultSetCreator;
import data.database.generators.TicketSaleQuerySentenceGenerator;
import entities.TicketSale;
import entities.TicketSaleItem;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Allows database interaction with persistent entity TicketSale.
 *
 * @author Erick López
 */
public class TicketSaleDAO implements DAO<TicketSale> {

    public static String TABLE_TICKETSALES = "TICKETSALES";
    private final TicketSaleQuerySentenceGenerator sentenceGenerator;
    private final DatabaseQueryExecutor queryExecutor;
    
    public TicketSaleDAO() {
        sentenceGenerator = new TicketSaleQuerySentenceGenerator(TABLE_TICKETSALES);
        queryExecutor = new DatabaseQueryExecutor();
    }
       
    @Override
    public boolean insert(TicketSale newTicketSale) {
        String insertTicketSaleQuery = sentenceGenerator.generateInsertTicketSaleSentence(newTicketSale);
        insertTicketSaleItems(newTicketSale);
        boolean insertDone = false;
        try {
            insertDone = queryExecutor.executeInsertQuery(insertTicketSaleQuery);
        } catch (SQLException ex) {
            Logger.getLogger(TicketSaleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertDone;
    }
    
    public boolean insertSimpleTicket(TicketSale newTicketSale) {
        String insertTicketSaleQuery = sentenceGenerator.generateInsertTicketSaleSentence(newTicketSale);        
        boolean insertDone = false;
        try {
            insertDone = queryExecutor.executeInsertQuery(insertTicketSaleQuery);
        } catch (SQLException ex) {
            Logger.getLogger(TicketSaleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertDone;
    }
    
     @Override
    public boolean delete(int existingTicketSaleID) {
        String deleteTicketSentence;
        deleteTicketSentence = sentenceGenerator.generateDeleteTicketSaleByIdSentence(existingTicketSaleID);
        boolean deleteDone = false;
        try {
            deleteDone = queryExecutor.executeDeleteOperation(deleteTicketSentence);
        } catch (SQLException ex) {
            Logger.getLogger(TicketSaleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return deleteDone;
    }

    @Override
    public boolean update(TicketSale updatedTicketSale) {
        String updateTicketSentence;
        updateTicketSentence = sentenceGenerator.generateUpdateTicketSaleSentence(updatedTicketSale);        
        boolean updateDone = false;;
        try {
            updateDone = queryExecutor.executeUpdateQuery(updateTicketSentence);
        } catch (SQLException ex) {
            Logger.getLogger(TicketSaleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return updateDone;
    }

    @Override
    public TicketSale getById(int ticketSaleID) {
        String findTicketByIdQuerySentence = sentenceGenerator.createFindTicketSaleByIdSentence(ticketSaleID);             
        TicketSale currentTicketSale = null;
        try {
            ResultSet resultSet = queryExecutor.executeFindByIdQuery(findTicketByIdQuerySentence);
            while (resultSet.next()) {        
                currentTicketSale = EntityFromResultSetCreator.createTicketSaleFromResultSet(resultSet);
                ArrayList<TicketSaleItem> items = requestAllItemsToTicketSaleItemDAO(currentTicketSale.getId());
                addItemsToTicketSale(items, currentTicketSale);
            }
        } catch (SQLException | ParseException ex) {
            Logger.getLogger(TicketSaleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return currentTicketSale;
    }

    @Override
    public TicketSale[] getList() {
        String findAllTicketSales;
        findAllTicketSales = sentenceGenerator.generateSelectAllFromTableSentence();              
        ArrayList<TicketSale> ticketSales = new ArrayList<>();
        TicketSale currentTicketSale;
        try {
            ResultSet resultSet = queryExecutor.executeFindFromTableOperation(findAllTicketSales);
            while (resultSet.next()) {
                currentTicketSale = EntityFromResultSetCreator.createTicketSaleFromResultSet(resultSet);
                ArrayList<TicketSaleItem> items = requestAllItemsToTicketSaleItemDAO(currentTicketSale.getId());
                System.out.println(items);
                addItemsToTicketSale(items, currentTicketSale);
                ticketSales.add(currentTicketSale);
            }
        } catch (SQLException | ParseException ex) {
            Logger.getLogger(TicketSaleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ticketSales.toArray(new TicketSale[ticketSales.size()]);
    }
   
    private void insertTicketSaleItems(TicketSale ticketSale) {
        TicketSaleItemDAO itemsDAO = new TicketSaleItemDAO();
        for(TicketSaleItem item : ticketSale.getItems()){
            itemsDAO.insert(item);
        }
    }

    private ArrayList<TicketSaleItem> requestAllItemsToTicketSaleItemDAO(int id) {
        String findAllTicketsFromCategory = sentenceGenerator.createFindAllFromTicketSaleId("idTicketSale", id);                  
        TicketSaleItem[] items = new TicketSaleItemDAO().getList(findAllTicketsFromCategory);
        return new ArrayList<>(Arrays.asList(items));
    }

    private void addItemsToTicketSale(ArrayList<TicketSaleItem> items, TicketSale currentTicketSale) {
        for(TicketSaleItem item : items){
            currentTicketSale.addItem(item);
        }
    }

    public int insertWithReturn(TicketSale ticketSale) {               
        int idRetrieved = 0;
        if(insertSimpleTicket(ticketSale)){
            idRetrieved = getLastTicketSaleIDInsert();
        }       
        return idRetrieved;
    }

    private int getLastTicketSaleIDInsert() {
        int lastID = 0;
        try {
            lastID = queryExecutor.getLastIDInsertedAtTable(TABLE_TICKETSALES);                       
        } catch (SQLException ex) {
            Logger.getLogger(TicketSaleDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            return lastID;
        }
    }
    
}

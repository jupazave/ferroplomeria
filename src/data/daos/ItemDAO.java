package data.daos;

import business.interfaces.data.DAO;
import data.ItemNotFoundException;
import data.database.DatabaseQueryExecutor;
import data.database.EntityFromResultSetCreator;
import data.database.generators.ItemQuerySentenceGenerator;
import entities.Item;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Allows database interaction with persistent entity DemandItem.
 *
 * @author Kirebyte
 */
public class ItemDAO implements DAO<Item> {

    public static final String TABLE_ITEM = "ITEM";
    private final ItemQuerySentenceGenerator sentenceGenerator;
    private final DatabaseQueryExecutor queryExecutor;
    private static final int ALL_ITEMS = 0;
    private static final int ONLY_DEMANDS = 1;
    
    public ItemDAO() {
        sentenceGenerator = new ItemQuerySentenceGenerator(TABLE_ITEM);
        queryExecutor = new DatabaseQueryExecutor();
    }

     @Override
    public boolean insert(Item newItem) {
        String insertCategorySentence;
        insertCategorySentence = sentenceGenerator.generateInsertItemSentence(newItem);               
        boolean insertDone = false;
        try {
            insertDone = queryExecutor.executeInsertQuery(insertCategorySentence);
        } catch (SQLException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertDone;
    }

    @Override
    public boolean delete(int existingProductId) {
        return false;
        /*String deleteCategorySentence;
        deleteCategorySentence = sentenceGenerator.generateDeleteCategoryByIdSentence(existingProductID);
        boolean deleteDone = false;
        try {
            deleteDone = queryExecutor.executeDeleteOperation(deleteCategorySentence);
        } catch (SQLException ex) {
            Logger.getLogger(DemandListItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return deleteDone;*/
    }

    @Override
    public boolean update(Item updatedItem) {
        String updateItemSentence;
        updateItemSentence = sentenceGenerator.generateUpdateItemSentence(updatedItem);        
        boolean updateDone = false;
        try {
            updateDone = queryExecutor.executeUpdateQuery(updateItemSentence);
        } catch (SQLException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return updateDone;
    }
    
    @Override
    public Item getById(int itemId) {
        String findCategoryByIdSentence;
        findCategoryByIdSentence = sentenceGenerator.generateFindItemByIdSentence(itemId);
        Item item = null;
        try {
            ResultSet resultSet = queryExecutor.executeFindByIdQuery(findCategoryByIdSentence);
            while (resultSet.next()) {
                item = EntityFromResultSetCreator.createItemFromResultSet(resultSet);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return item;
    }

    @Override
    public Item[] getList() {
        return getAnyLists(ALL_ITEMS);
    }
    
    public Item[] getDemands(){
        return getAnyLists(ONLY_DEMANDS);
    }
    
    public Item findByNameBrand(String name, String brand) throws ItemNotFoundException{
        String findSentence;
        findSentence = sentenceGenerator.generateFindItemByNameBrandSentence(name, brand);
        Item item;
        try{
            ResultSet resultSet = queryExecutor.executeFindFromTableOperation(findSentence);
            if(resultSet.next()) {
                item = EntityFromResultSetCreator.createItemFromResultSet(resultSet);
                return item;
            }
            else {
                throw new ItemNotFoundException();
            }
        } catch (SQLException ex){
            throw new ItemNotFoundException();
        }
    }
    
    public boolean isProduct(int itemId){
        String checkSentence;
        checkSentence = sentenceGenerator.generateIsProductSentence(itemId);
        try {
            ResultSet resultSet = queryExecutor.executeFindFromTableOperation(checkSentence);
            return resultSet.next();
        } catch (SQLException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public void close(){
        queryExecutor.close();
    }
    
    private Item[] getAnyLists(int option){
        String findAllItems = generateGetListSentence(option);
        ArrayList<Item> items = new ArrayList<>();
        Item currentItem;
        try {
            ResultSet resultSet = queryExecutor.executeFindFromTableOperation(findAllItems);
            while (resultSet.next()) {
                currentItem = EntityFromResultSetCreator.createItemFromResultSet(resultSet);
                items.add(currentItem);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items.toArray(new Item[items.size()]);
    }
    
    private String generateGetListSentence(int option){
        String sentence;
        switch(option){
            case ALL_ITEMS:
                sentence = sentenceGenerator.generateGetListSentence();
                break;
            case ONLY_DEMANDS:
                sentence = sentenceGenerator.generateGetDemandsSentence();
                break;
            default:
                sentence = sentenceGenerator.generateGetListSentence();
                break;
        }
        return sentence;
    }
    
}

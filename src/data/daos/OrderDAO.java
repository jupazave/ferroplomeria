package data.daos;

import business.interfaces.data.DAO;
import data.database.DatabaseQueryExecutor;
import data.database.EntityFromResultSetCreator;
import data.database.generators.OrderQuerySentenceGenerator;
import entities.Order;
import entities.OrderItem;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Allows database interaction with persistent entity DeliveredOrder.
 *
 * @author Erick López
 */
public class OrderDAO implements DAO<Order> {
    
    public static final int INVALID_ORDER_ID = -1;
    
    private static final String TABLE_ORDERS = "ORDERS";
    private final OrderQuerySentenceGenerator sentenceGenerator;
    private final DatabaseQueryExecutor queryExecutor;
    private final OrderItemDAO orderItemDAO;
    
    public OrderDAO(){
        sentenceGenerator = new OrderQuerySentenceGenerator(TABLE_ORDERS);
        queryExecutor = new DatabaseQueryExecutor();
        orderItemDAO = new OrderItemDAO();
    }

    @Override
    public boolean insert(Order newEntity) {
        boolean insertDone = false;
        String query;
        query = sentenceGenerator.generateInsertOrderSentence(newEntity);
        try{
            insertDone = queryExecutor.executeInsertQuery(query);
        } catch(SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            return insertDone;
        }
    }

    @Override
    public boolean delete(int existingEntityID) {
        boolean deleteDone = false;
        String query;
        query = sentenceGenerator.generateDeleteOrderSentence(existingEntityID);
        try{
            queryExecutor.executeDeleteOperation(query);
            deleteDone = true;
        } catch(SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            return deleteDone;
        }
    }

    @Override
    public boolean update(Order updatedEntity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Order getById(int entityID) {
        String findByIDSentence;
        findByIDSentence = sentenceGenerator.generateGetOrderByIdSentence(entityID);
        Order foundOrder=null;
        try {
            ResultSet resultSet = queryExecutor.executeFindFromTableOperation(findByIDSentence);
            if(resultSet.next()){
                foundOrder = EntityFromResultSetCreator.createOrderFromResultSet(resultSet);
                OrderItem[] orderItems = orderItemDAO.getSpecificOrderList(foundOrder.getId());
                foundOrder.addAllItems((List<OrderItem>) Arrays.asList(orderItems));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return foundOrder;
    }

    @Override
    public Order[] getList() {
        String findAllSentence;
        findAllSentence = sentenceGenerator.generateGetOrderListSentence();
        List<Order> orders = new ArrayList<>();
        Order current;
        try {
            ResultSet resultSet = queryExecutor.executeFindFromTableOperation(findAllSentence);
            while (resultSet.next()) {
                current = EntityFromResultSetCreator.createOrderFromResultSet(resultSet);
                OrderItem[] orderItems = orderItemDAO.getSpecificOrderList(current.getId());
                current.addAllItems((List<OrderItem>) Arrays.asList(orderItems));
                orders.add(current);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*for(Order currentt:orders){
            for(OrderItem currenttt: currentt.getItems()){
                System.out.println(currenttt.getID());
            }
        }*/
        return orders.toArray(new Order[orders.size()]);
    }
    
    public int getLastInsertedIdAtTable(){
        int lastOrderID = INVALID_ORDER_ID;
        try {
            lastOrderID = queryExecutor.getLastIDInsertedAtTable(TABLE_ORDERS);
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            return lastOrderID;
        }
    }

    public void close(){
        queryExecutor.close();
    }
}

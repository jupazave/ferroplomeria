package data;

import presentation.resources.StringResources;

public class ItemNotFoundException extends Exception {
    
    public ItemNotFoundException(){
        this(StringResources.ITEM_NOT_FOUND_EXCEPTION);
    }
    
    public ItemNotFoundException(String message) {
        super(message);
    }

    public ItemNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }
    
}

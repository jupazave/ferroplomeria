package data;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class FileHandler {
    public FileOutputStream createNewDocumentFile(String newDocumentFilePath) throws FileNotFoundException {
        FileOutputStream reportFile;
        reportFile = new FileOutputStream(newDocumentFilePath);
        return reportFile;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.database;

import static business.reports.ReportsSentencesResources.DATE_TIME_FORMAT;
import static business.reports.ReportsSentencesResources.YEAR_MONTH_DAY_FORMAT;
import data.daos.CategoryDAO;
import data.daos.ItemDAO;
import data.daos.OrderItemDAO;
import data.daos.SupplierDAO;
import entities.Category;
import entities.Item;
import entities.Order;
import entities.OrderItem;
import entities.Product;
import entities.Supplier;
import entities.TicketSale;
import entities.TicketSaleItem;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author luisburgos
 */
public class EntityFromResultSetCreator {

    public static Product createProductFromResultSet(ResultSet resultSet) throws SQLException {
        Product newProduct = new Product();
        
        newProduct.setItem(new Item(
                resultSet.getInt("idItem"),
                resultSet.getString("name"),
                resultSet.getString("brand")
        ));
        
        newProduct.setId(resultSet.getInt(1));
        newProduct.setDescription(resultSet.getString("description"));
        newProduct.setCost(resultSet.getDouble("cost"));
        newProduct.setPrice(resultSet.getDouble("price"));
        newProduct.setStock(resultSet.getInt("stock"));

        int categoryID = resultSet.getInt("idCategory");
        Category category;
        category = new CategoryDAO().getById(categoryID);

        newProduct.setCategory(category);
        return newProduct;
    }

    public static TicketSale createTicketSaleFromResultSet(ResultSet resultSet) throws SQLException, ParseException {
        TicketSale newTicketSale = new TicketSale();
        newTicketSale.setId(resultSet.getInt("id"));        
        
        //TODO: Date operations may be part of DateReportCalculator.
        DateFormat df = new SimpleDateFormat(YEAR_MONTH_DAY_FORMAT);
        Date date = df.parse(resultSet.getString("date"));        
        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); 
        Date newDate = formatter.parse(formatter.format(date)); 
        
        newTicketSale.setDate(newDate);
        return newTicketSale;
    }

    public static Category createCategoryFromResultSet(ResultSet resultSet) throws SQLException {
        Category newCategory = new Category();
        newCategory.setId(resultSet.getInt("id"));
        newCategory.setName(resultSet.getString("name"));
        newCategory.setDescription(resultSet.getString("description"));
        return newCategory;
    }

    public static Supplier createSupplierFromResultSet(ResultSet resultSet) throws SQLException {
        Supplier newSupplier = new Supplier();
        newSupplier.setId(resultSet.getInt("id"));
        newSupplier.setName(resultSet.getString("name"));
        newSupplier.setPhone(resultSet.getString("telephone"));
        newSupplier.setEmail(resultSet.getString("email"));
        newSupplier.setAddress(resultSet.getString("address"));
        return newSupplier;
    }

    public static TicketSaleItem createTicketSaleItemFromResultSet(ResultSet resultSet) throws SQLException {        
        TicketSaleItem item = new TicketSaleItem();        
        item.setTicketSaleID(resultSet.getInt("idTicketSale"));        
        Item currentItem = new ItemDAO().getById(resultSet.getInt("idItem"));
        Product product = new Product();
        product.setName(currentItem.getName()); //TODO: Fix this 
        item.setProduct(product);
        item.setQuantity(resultSet.getInt("quantity"));
        item.setUnitPrice(resultSet.getDouble("unitPrice"));
        return item;
    }
    
    public static Item createItemFromResultSet(ResultSet resultSet) throws SQLException {
        Item item = new Item();        
        item.setID(resultSet.getInt("id"));
        item.setName(resultSet.getString("name"));
        item.setBrand(resultSet.getString("brand"));
        return item;
    }
    
    public static Order createOrderFromResultSet(ResultSet resultSet) throws SQLException{
        Order order = new Order();
        order.setId(resultSet.getInt("id"));
        
        SupplierDAO supplierDAO = new SupplierDAO();
        Supplier orderSupplier = supplierDAO.getById(resultSet.getInt("idSupplier"));
        
        OrderItemDAO orderItemDAO = new OrderItemDAO();
        
        order.setSupplier(orderSupplier);
        return order;
    }
    
    public static OrderItem createOrderItemFromResultSet(ResultSet resultSet) throws SQLException{
        OrderItem orderItem = new OrderItem();
        orderItem.setID(resultSet.getInt("id"));
        orderItem.setQuantity(resultSet.getInt("quantity"));
        orderItem.setOrderID(resultSet.getInt("idOrder"));
        
        ItemDAO itemDAO = new ItemDAO();
        Item item = itemDAO.getById(resultSet.getInt("idItem"));
        
        orderItem.setItem(item);
        
        return orderItem;
    }

}

package data.database.generators;

import static data.database.QueryStringResources.*;
import entities.Item;

public class ItemQuerySentenceGenerator {
    
    private final String tableName;
    
    public ItemQuerySentenceGenerator(String tableName){
       this.tableName = tableName; 
    }
    
    public String generateInsertItemSentence(Item newItem){
         String generatedSentence;
        generatedSentence = INSERT_INTO + SPACE
                + tableName + SPACE
                + "(name, brand)" + SPACE 
                + VALUES + SPACE + OPEN_PARENTHESIS                
                + SINGLE_QUOTE + newItem.getName()+ SINGLE_QUOTE +  COMMA
                + SINGLE_QUOTE + newItem.getBrand() + SINGLE_QUOTE + CLOSE_PARENTHESIS
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }
    
    public String generateUpdateItemSentence(Item updatedItem){
        return "UPDATE ITEM SET name = '" + updatedItem.getName() + "', brand = '" + updatedItem.getBrand() + "' WHERE id = " + updatedItem.getID() + ";";
    }
    
    public String generateGetDemandsSentence(){
        return "SELECT * FROM ITEM EXCEPT SELECT ITEM.* FROM ITEM JOIN PRODUCTS WHERE ITEM.id = PRODUCTS.idItem;";
    }
    
    public String generateGetListSentence(){
        return "SELECT * FROM ITEM;";
    }
    
    public String generateFindItemByIdSentence(int entityID) {
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE
                + tableName + SPACE
                + WHERE + SPACE
                + ID + EQUALS
                + entityID
                + SEMICOLON;
        return generatedSentence;
    }
    
    public String generateFindItemByNameBrandSentence(String name, String brand){
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE
                + tableName + SPACE
                + WHERE + SPACE
                + "name" + EQUALS + SINGLE_QUOTE + name + SINGLE_QUOTE + AND
                + "brand" + EQUALS + SINGLE_QUOTE + brand + SINGLE_QUOTE
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }
    
    public String generateIsProductSentence(int itemId){
        return "SELECT * FROM ITEM JOIN PRODUCTS ON ITEM.id = PRODUCTS.idItem WHERE idItem = " + itemId + ";";
    }
}

package data.database.generators;

import static data.database.QueryStringResources.*;
import entities.Order;

public class OrderQuerySentenceGenerator {
    
    private String tableName;
    
    public OrderQuerySentenceGenerator(String tableName){
        this.tableName = tableName;
    }
    
    public String generateGetOrderListSentence(){
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE + tableName
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }
    
    public String generateGetOrderByIdSentence(int orderId){
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE + tableName
                + SPACE + WHERE + SPACE + "id" + EQUALS + orderId
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }
    
    public String generateInsertOrderSentence(Order order){
        String generatedSentence;
        generatedSentence = INSERT_INTO + SPACE
                + tableName + SPACE
                + "(idSupplier)" + SPACE
                + VALUES + SPACE + OPEN_PARENTHESIS
                + SINGLE_QUOTE + order.getSupplier().getId() + SINGLE_QUOTE
                + CLOSE_PARENTHESIS + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }
    
    public String generateDeleteOrderSentence(int orderId){
        String generatedSentence;
        generatedSentence = DELETE
                + SPACE + FROM
                + SPACE + tableName
                + SPACE + WHERE + SPACE + "id" + EQUALS + orderId 
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }
    
    
}

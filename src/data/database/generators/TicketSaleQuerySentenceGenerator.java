/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.database.generators;

import static business.reports.ReportsSentencesResources.YEAR_MONTH_DAY_FORMAT;
import static data.database.QueryStringResources.*;
import entities.TicketSale;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author luisburgos
 */
public class TicketSaleQuerySentenceGenerator {

    private final String tableName;

    public TicketSaleQuerySentenceGenerator(String tableName) {
        this.tableName = tableName;
    }

    public String generateInsertTicketSaleSentence(TicketSale newEntity) {
        String dateFormatted = new SimpleDateFormat(YEAR_MONTH_DAY_FORMAT).format(newEntity.getDate());
        String generatedSentence;
        generatedSentence = INSERT_INTO + SPACE
                + tableName + SPACE
                + "(date)" + SPACE
                + VALUES + SPACE + OPEN_PARENTHESIS
                + SINGLE_QUOTE + dateFormatted + SINGLE_QUOTE + CLOSE_PARENTHESIS
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }

    public String createFindTicketSaleByIdSentence(int ticketSaleID) {
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE
                + tableName + SPACE
                + WHERE + SPACE
                + ID + EQUALS
                + ticketSaleID
                + SEMICOLON;
        return generatedSentence;
    }

    public String createFindAllFromTicketSaleId(String idTicketSale, int id) {
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE
                + "TICKETSALE_ITEMS" + SPACE //TODO: Extract method
                + WHERE + SPACE
                + idTicketSale + EQUALS
                + SINGLE_QUOTE + id + SINGLE_QUOTE
                + SEMICOLON;
        return generatedSentence;
    }

    public String generateUpdateTicketSaleSentence(TicketSale updatedTicketSale) {
        String condition = ID + EQUALS + updatedTicketSale.getId();
        String generatedSentence;
        generatedSentence = UPDATE + SPACE
                + tableName + SPACE + SET + SPACE
                + "date=" + SINGLE_QUOTE + updatedTicketSale.getDate() + SINGLE_QUOTE
                + SPACE
                + WHERE + SPACE
                + condition
                + SEMICOLON;
        return generatedSentence;
    }

    public String generateDeleteTicketSaleByIdSentence(int existingTicketSaleID) {
        String generatedSentence;
        generatedSentence = DELETE + SPACE
                + FROM + SPACE
                + tableName + SPACE
                + WHERE + SPACE
                + ID + EQUALS
                + existingTicketSaleID
                + SEMICOLON;
        return generatedSentence;
    }

    public String generateSelectAllFromTableSentence() {
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE
                + tableName
                + SEMICOLON;
        return generatedSentence;
    }

    public String createSelectLastID() {
        return "SELECT seq from sqlite_sequence where name =" 
                + SINGLE_QUOTE + tableName + SINGLE_QUOTE + SEMICOLON;
    }

}

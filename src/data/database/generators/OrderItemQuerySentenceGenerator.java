package data.database.generators;

import static data.database.QueryStringResources.*;
import entities.OrderItem;

public class OrderItemQuerySentenceGenerator {
    
    private String tableName;
    
    public OrderItemQuerySentenceGenerator(String tableName){
        this.tableName = tableName;
    }
    
    public String generateInsertOrderItemSentence(OrderItem orderItem){
        String generatedSentence;
        generatedSentence = INSERT_INTO + SPACE
                + tableName + SPACE
                + "(idItem,quantity,idOrder)" + SPACE
                + VALUES + SPACE + OPEN_PARENTHESIS
                + SINGLE_QUOTE + orderItem.getItem().getID() + SINGLE_QUOTE + COMMA
                + SINGLE_QUOTE + orderItem.getQuantity() + SINGLE_QUOTE + COMMA
                + SINGLE_QUOTE + orderItem.getOrderID() + SINGLE_QUOTE
                + CLOSE_PARENTHESIS + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }
    
    public String generateGetOrderItemListSentence(){
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE + tableName
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }
    
    public String generateDeleteOrderItemSentence(int orderItemId){
        String generatedSentence;
        generatedSentence = DELETE
                + SPACE + FROM
                + SPACE + tableName
                + SPACE + WHERE + SPACE + "id" + EQUALS + orderItemId 
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }
    
}

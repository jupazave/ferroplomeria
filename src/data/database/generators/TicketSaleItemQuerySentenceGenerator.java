/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.database.generators;

import static data.database.QueryStringResources.*;
import entities.TicketSaleItem;

/**
 *
 * @author luisburgos
 */
public class TicketSaleItemQuerySentenceGenerator {

    private final String tableName;

    public TicketSaleItemQuerySentenceGenerator(String tableName) {
        this.tableName = tableName;
    }

    public String generateInsertTicketSaleItemSentence(TicketSaleItem newItem) {
        String generatedSentence;
        generatedSentence = INSERT_INTO + SPACE
                + tableName + SPACE
                + "(idTicketSale, idItem, quantity, unitPrice)" + SPACE
                + VALUES + SPACE + OPEN_PARENTHESIS
                + SINGLE_QUOTE + newItem.getTicketSaleID() + SINGLE_QUOTE + COMMA
                + SINGLE_QUOTE + newItem.getProduct().getItem().getID() + SINGLE_QUOTE + COMMA
                + SINGLE_QUOTE + newItem.getQuantity() + SINGLE_QUOTE + COMMA
                + SINGLE_QUOTE + newItem.getUnitPrice() + SINGLE_QUOTE + CLOSE_PARENTHESIS
                + SEMICOLON;        
        return generatedSentence;
    }

    public String generateDeleteItemByIdSentence(int existingItemID) {
        String generatedSentence;
        generatedSentence = DELETE + SPACE
                + FROM + SPACE
                + tableName + SPACE
                + WHERE + SPACE
                + ID + EQUALS
                + existingItemID
                + SEMICOLON;
        return generatedSentence;
    }    

    public String generateSelectAllFromTableSentence() {
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE
                + tableName
                + SEMICOLON;
        return generatedSentence;
    }
    
}

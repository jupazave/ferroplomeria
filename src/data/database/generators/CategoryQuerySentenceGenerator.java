/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.database.generators;

import static data.database.QueryStringResources.*;
import entities.Category;

/**
 *
 * @author luisburgos
 */
public class CategoryQuerySentenceGenerator {

    private final String tableName;

    public CategoryQuerySentenceGenerator(String tableName) {
        this.tableName = tableName;
    }

    public String generateInsertCategorySentence(Category newCategory) {
         String generatedSentence;
        generatedSentence = INSERT_INTO + SPACE
                + tableName + SPACE
                + "(name, description)" + SPACE 
                + VALUES + SPACE + OPEN_PARENTHESIS                
                + SINGLE_QUOTE + newCategory.getName()+ SINGLE_QUOTE +  COMMA
                + SINGLE_QUOTE + newCategory.getDescription() + SINGLE_QUOTE + CLOSE_PARENTHESIS
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }

    public String generateDeleteCategoryByIdSentence(int existingCategoryID) {
        String generatedSentence;
        generatedSentence = DELETE + SPACE
                + FROM + SPACE
                + tableName + SPACE
                + WHERE + SPACE
                + ID + EQUALS 
                + existingCategoryID
                + SEMICOLON;
        return generatedSentence;
    }

    public String generateUpdateCategorySentence(Category updatedCategory) {
        String condition = ID + EQUALS + updatedCategory.getId();        
        String generatedSentence;
        generatedSentence = UPDATE + SPACE 
                + tableName + SPACE + SET + SPACE                                 
                + "name=" + SINGLE_QUOTE + updatedCategory.getName() + SINGLE_QUOTE + COMMA
                + "description=" + SINGLE_QUOTE + updatedCategory.getDescription() + SINGLE_QUOTE
                + SPACE 
                + WHERE + SPACE 
                + condition 
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;        
    }

    public String generateFindCategoryByIdSentence(int entityID) {
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE
                + tableName + SPACE
                + WHERE + SPACE
                + ID + EQUALS
                + entityID
                + SEMICOLON;
        return generatedSentence;
    }

    public String generateSelectAllFromTableSentence() {
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE
                + tableName
                + SEMICOLON;
        return generatedSentence;
    }

}

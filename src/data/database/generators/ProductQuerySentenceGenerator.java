/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.database.generators;

import static data.database.QueryStringResources.*;
import entities.Product;

/**
 *
 * @author luisburgos
 */
public class ProductQuerySentenceGenerator {

    private final String tableName;

    public ProductQuerySentenceGenerator(String tableName) {
        this.tableName = tableName;
    }

    public String generateInsertProductSentence(Product product) {
        String generatedSentence;
        generatedSentence = INSERT_INTO + SPACE
                + tableName + SPACE
                + "(idItem, description, price, cost, stock, idCategory)" + SPACE
                + VALUES + SPACE + OPEN_PARENTHESIS                
                + SINGLE_QUOTE + product.getItem().getID() + SINGLE_QUOTE + COMMA
                + SINGLE_QUOTE + product.getDescription() + SINGLE_QUOTE + COMMA
                + SINGLE_QUOTE + product.getPrice() + SINGLE_QUOTE + COMMA
                + SINGLE_QUOTE + product.getCost() + SINGLE_QUOTE + COMMA
                + SINGLE_QUOTE + product.getStock() + SINGLE_QUOTE + COMMA
                + SINGLE_QUOTE + product.getCategory().getId() + SINGLE_QUOTE + CLOSE_PARENTHESIS
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }

    public String generateDeleteProductByIdSentence(int productID) {
        String generatedSentence;
        generatedSentence = DELETE + SPACE
                + FROM + SPACE
                + tableName + SPACE
                + WHERE + SPACE
                + ID + EQUALS
                + productID
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }

    public String generateUpdateProductSentence(Product updatedProduct) {
        String condition = ID + EQUALS + updatedProduct.getId();
        String generatedSentence;
        generatedSentence = UPDATE + SPACE
                + tableName + SPACE + SET + SPACE
                + "description=" + SINGLE_QUOTE + updatedProduct.getDescription() + SINGLE_QUOTE + COMMA
                + "cost=" + SINGLE_QUOTE + updatedProduct.getCost() + SINGLE_QUOTE + COMMA
                + "price=" + SINGLE_QUOTE + updatedProduct.getPrice() + SINGLE_QUOTE + COMMA
                + "stock=" + SINGLE_QUOTE + updatedProduct.getStock() + SINGLE_QUOTE + COMMA
                + "idCategory=" + SINGLE_QUOTE + updatedProduct.getCategory().getId() + SINGLE_QUOTE
                + SPACE
                + WHERE + SPACE
                + condition
                + SEMICOLON;
        return generatedSentence;
    }

    public String generateFindProductByIdSentence(int entityID) {
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE
                + tableName + SPACE
                + JOIN + SPACE + "ITEM" + SPACE
                + ON + SPACE + "ITEM.id" + EQUALS + tableName + ".idItem"
                + SPACE + WHERE + SPACE
                + tableName + ".id" + EQUALS
                + entityID
                + SEMICOLON;
        return generatedSentence;
    }

    public String generateFindAllProductsSentence() {
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE + tableName
                + SPACE + JOIN + SPACE + "ITEM"
                + SPACE + ON + SPACE + "ITEM.id" + EQUALS + tableName + ".idItem"
                + SEMICOLON;
        return generatedSentence;
    }

    public String generateFindAllProductsFromCategory(int id) {
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE + tableName
                + SPACE + JOIN + SPACE + "ITEM"
                + SPACE + ON + SPACE + "ITEM.id" + EQUALS + tableName + ".idItem"
                + SPACE + WHERE + SPACE + "idCategory" + EQUALS + id
                + SEMICOLON;
        return generatedSentence;
    }

    public String generateUpdateStockSentence(int newValue, int id) {
        String generatedSentence;
        generatedSentence = UPDATE + SPACE
                + tableName + SPACE + SET + SPACE
                + "stock" + EQUALS + newValue
                + SPACE + WHERE + SPACE
                + ID + EQUALS + id
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }
    
    public String generateFindProductByItemID(int itemId){
        String generatedSentence;
        generatedSentence = SELECT + SPACE
                + ASTERISK + SPACE + FROM + tableName + SPACE
                + JOIN + SPACE + "ITEM" + SPACE
                + ON + SPACE + "ITEM.id" + EQUALS + "PRODUCTS.idItem"
                + SPACE + WHERE + "idItem" + EQUALS + itemId
                + SEMICOLON;
        return generatedSentence;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.database;

/**
 *
 * @author luisburgos
 */
public class QueryStringResources {
 
    public static final String ASTERISK = "*";
    public static final String CLOSE_PARENTHESIS = ")";
    public static final String DELETE = "DELETE";
    public static final String DROP_TABLE_IF_EXISTS = "DROP TABLE IF EXISTS";
    public static final String EMPTY_STRING = "";
    public static final String FROM = "FROM";
    public static final String EQUALS = "=";
    public static final String INSERT_INTO = "INSERT INTO";
    public static final String LIKE = "LIKE";
    public static final String MAX = "MAX";
    public static final String OPEN_PARENTHESIS = "(";
    public static final String PERCENT = "%";
    public static final String PRAGMA = "PRAGMA";
    public static final String QUOTES = "\"";
    public static final String SELECT = "SELECT";
    public static final String SEMICOLON = ";";
    public static final String SINGLE_QUOTE = "\'";
    public static final String SPACE = " ";
    public static final String TABLE_INFO = "table_info";
    public static final String UPDATE = "UPDATE";
    public static final String SET = "SET";
    public static final String VALUES = "VALUES";
    public static final String WHERE = "WHERE";
    public static final String AND = SPACE + "AND" + SPACE;
    public static final String OR = SPACE + "OR" + SPACE;
    public static final String COMMA = "," + SPACE;
    public static final String ID = "id";
    public static final String JOIN = "JOIN";
    public static final String ON = "ON";
    
}

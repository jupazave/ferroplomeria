package data.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implements DatabaseConnectionManager. Manages connection to the database.
 *
 * @author Erick Lopez
 */
public class DatabaseConnectionManager {

    private static final String DATABASE_JDBC_NAME = "org.sqlite.JDBC";
    private static final String DATABASE_SOURCE_TYPE = "jdbc:sqlite:";    
    private static final String DATABASE_PATH = "./active-database-new.sqlite";
    private static DatabaseConnectionManager connectionManager;
    private Connection connection;    
   
    private DatabaseConnectionManager() { }
    
    public synchronized static DatabaseConnectionManager getConnectionManager(){
        if(connectionManager == null){
            connectionManager = new DatabaseConnectionManager();
        }
        return connectionManager;
    }

    public Connection getConnection(){
        openConnection();
        return connection;
    }    
      
    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void openConnection() {
        try {
            Class.forName(DATABASE_JDBC_NAME);
            connection = DriverManager.getConnection(DATABASE_SOURCE_TYPE + DATABASE_PATH);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DatabaseConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
}

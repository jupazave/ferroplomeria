/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author luisburgos
 */
public class DatabaseQueryExecutor {

    private final DatabaseConnectionManager connectionManager;
    private final String GETLASTID_SENTENCE_BEGIN = "SELECT seq from sqlite_sequence where name ='";
    private final String GETLASTID_SENTENCE_END = "';";

    public DatabaseQueryExecutor() {
        connectionManager = DatabaseConnectionManager.getConnectionManager();
    }

    public boolean executeInsertQuery(String querySentence) throws SQLException {
        boolean queryDone = false;
        if (isQueryExecutedWithoutError(querySentence)) {
            queryDone = true;
        }
        return queryDone;
    }

    public boolean executeDeleteOperation(String deleteCategoryQuerySentence) throws SQLException {
        boolean queryDone = false;
        if (isQueryExecutedWithoutError(deleteCategoryQuerySentence)) {
            queryDone = true;
        }
        return queryDone;
    }

    public ResultSet executeFindFromTableOperation(String querySetence) throws SQLException {
        ResultSet queryResultSet = null;
        Statement statement = getPreparateStatement();
        queryResultSet = statement.executeQuery(querySetence);
        return queryResultSet;
    }

    public ResultSet executeFindByIdQuery(String findCategoryByIdQuerySentence) throws SQLException {
        ResultSet queryResultSet = null;
        Statement statement = getPreparateStatement();
        queryResultSet = statement.executeQuery(findCategoryByIdQuerySentence);
        return queryResultSet;
    }

    public boolean executeUpdateQuery(String querySentence) throws SQLException {
        boolean queryDone = false;
        if (isQueryExecutedWithoutError(querySentence)) {
            queryDone = true;
        }
        return queryDone;
    }

    public int executeUpdateQuery(String querySentence, int modifier) throws SQLException {
        ResultSet queryResultSet = null;
        Statement statement = getPreparateStatement();
        statement.executeUpdate(querySentence, Statement.RETURN_GENERATED_KEYS);
        queryResultSet = statement.getGeneratedKeys();
        if (queryResultSet.next()) {
            return queryResultSet.getInt(1);
        } else {
            return 0;
        }
    }

    public boolean executeInsertTicketItemQuery(String querySentence) throws SQLException {
        Statement statement = getPreparateStatement();
        return statement.execute(querySentence);
    }

    public void close() {
        connectionManager.closeConnection();
    }

    public int getLastIDInsertedAtTable(String tableName) throws SQLException {
        ResultSet queryResultSet = null;
        Statement statement = getPreparateStatement();
        String query = GETLASTID_SENTENCE_BEGIN + tableName + GETLASTID_SENTENCE_END;
        System.out.println(query);
        queryResultSet = statement.executeQuery(query);
        return queryResultSet.getInt("seq");
    }

    private boolean isQueryExecutedWithoutError(String query) throws SQLException {
        Statement statement = getPreparateStatement();
        return statement.executeUpdate(query) != 0;
    }

    private Statement getPreparateStatement() throws SQLException {
        Statement statement;
        statement = connectionManager.getConnection().createStatement();
        statement.closeOnCompletion();
        return statement;
    }
    
}

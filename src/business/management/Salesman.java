/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.management;

import business.coordinators.BusinessManagement;
import business.coordinators.TicketSaleCoordinator;
import entities.TicketSale;
import entities.TicketSaleItem;

/**
 *
 * @author luisburgos
 */
public class Salesman {

    private TicketSale currentTicketSale;
    private double totalPurchase;

    public Salesman() {
        initializeNewSale();
    }

    private void initializeNewSale() {
        this.currentTicketSale = new TicketSale();
        this.totalPurchase = 0;
    }

    public void addItemToTicketSale(TicketSaleItem item) {
        currentTicketSale.addItem(item);
    }   

    public void removeItemToTicketSale(TicketSaleItem item) {
        removeItemByProductName(item.getProduct().getName());
    }

    public void finishCurrentSale() {
        boolean thereIsSOmethingToPurchase = currentTicketSale.getItems().size() > 0;
        if (thereIsSOmethingToPurchase) {
            BusinessManagement.getManager().getTicketSaleCoordinator().add(currentTicketSale);
        }
    }

    public void updateTotalPayment() {       
        totalPurchase = calculateTotalPurchaseCost();
    }

    public double getCurrentPaymentAmount() {
        return totalPurchase;
    }
    
    private void removeItemByProductName(String name) {
        for (TicketSaleItem item : currentTicketSale.getItems()) {
            if (name.equalsIgnoreCase(item.getProduct().getName())) {                
                currentTicketSale.removeItem(item);
            }
        }
    
    }

    private double calculateTotalPurchaseCost() {
        Double newTotalPurchaseCost = 0.0;
         for (TicketSaleItem ticketSaleItem : currentTicketSale.getItems()) {            
            newTotalPurchaseCost += getTotalItemCost(ticketSaleItem);
        }
        return newTotalPurchaseCost;
    }
    
    private double getTotalItemCost(TicketSaleItem ticketSaleItem){
        double currentTicketItemQuantity = ticketSaleItem.getQuantity();
        double currentTicketItemPrice = ticketSaleItem.getProduct().getPrice();
        return currentTicketItemQuantity * currentTicketItemPrice;
    }

}

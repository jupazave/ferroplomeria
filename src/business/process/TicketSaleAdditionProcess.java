/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.process;

import business.coordinators.ProductCoordinator;
import business.reports.date.DateCalculator;
import data.daos.ProductDAO;
import data.daos.TicketSaleDAO;
import data.daos.TicketSaleItemDAO;
import entities.Product;
import entities.TicketSale;
import entities.TicketSaleItem;
import java.util.Date;

/**
 *
 * @author luisburgos
 */
public class TicketSaleAdditionProcess {

    private final TicketSale ticketSale;
    private TicketSaleDAO ticketDAO;
    private TicketSaleItemDAO itemDAO;
    private ProductCoordinator productCoordinator;
    private ProductDAO productDAO;

    public TicketSaleAdditionProcess(TicketSale ticketSale) {
        this.ticketSale = ticketSale;
        ticketDAO = new TicketSaleDAO();
        itemDAO = new TicketSaleItemDAO();
        productDAO = new ProductDAO();
        productCoordinator = new ProductCoordinator();
    }

    public boolean execute() {
        boolean additionProcessDone = false;                
        
        establishTicketDate();
        updateTicketSaleID();
        updateTicketSaleItemsID();
        
        boolean insertTicketItemsDone = insertTicketSaleItems();
        if(!insertTicketItemsDone){            
            return additionProcessDone = false;
        };
        
        boolean updateProductsDone = updateProductsStock();
        if(!updateProductsDone){
             return additionProcessDone = false;
        }
        return additionProcessDone;
    }

    private void establishTicketDate() {
        Date ticketSaleDate = DateCalculator.getTodayDateWithoutTime();
        ticketSale.setDate(ticketSaleDate);
    }

    private void updateTicketSaleID() {
        int idTicketSale = requestIDFromtInsertOperation();
        System.out.println("New TICKET id: " + idTicketSale);
        ticketSale.setId(idTicketSale);
    }

    private int requestIDFromtInsertOperation() {
        return ticketDAO.insertWithReturn(ticketSale);
    }

    private void updateTicketSaleItemsID() {
        int newID = ticketSale.getId();
        System.out.println("UPDATE ITEMS ID FOR : " + ticketSale.getItems().size());
        for (TicketSaleItem item : ticketSale.getItems()) {
            item.setTicketSaleID(newID);
        }
    }

    private boolean insertTicketSaleItems() {
        boolean insertOperations = true;
        
        System.out.println("INSERTING ITEMS ID FOR : " + ticketSale.getItems().size());
        for (TicketSaleItem item : ticketSale.getItems()) {
            boolean currentInsertDone = itemDAO.insert(item);            
            if(!currentInsertDone){
                insertOperations = false;
                break; //TODO: Verify this process.
            };
        }
        return insertOperations;
    }

    private boolean updateProductsStock() {
        boolean updateStockOperations = true;
        Product currentProduct;
        for (TicketSaleItem item : ticketSale.getItems()) {
            currentProduct = item.getProduct();
            int currentItemStock = currentProduct.getStock();
            int newItemStock = currentItemStock - item.getQuantity(); 
            System.out.println(newItemStock);
            boolean currentUpdateDone = updateParticularProductStock(currentProduct, newItemStock);
            if(!currentUpdateDone){
                updateStockOperations = false;
                break; //TODO: Verify this process.
            }
        }
        return updateStockOperations;
    }

    private boolean updateParticularProductStock(Product currentProduct, int newStock) {
        Product newProduct = currentProduct;
        newProduct.setStock(newStock);        
        return productCoordinator.edit(newProduct);
    }

}

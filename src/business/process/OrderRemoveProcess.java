package business.process;

import data.daos.OrderDAO;
import data.daos.OrderItemDAO;
import data.database.DatabaseConnectionManager;
import data.database.DatabaseQueryExecutor;
import entities.Order;
import entities.OrderItem;

public class OrderRemoveProcess {
    
    private final Order deleteOrder;
    private final OrderItemDAO orderItemDAO;
    private final OrderDAO orderDAO;
    
    public OrderRemoveProcess(Order order) {
        this.deleteOrder = order;
        orderItemDAO = new OrderItemDAO();
        orderDAO = new OrderDAO();
    }
    
    public boolean execute(){
         boolean isProcessSucessfull;
         
        isProcessSucessfull = removeOrderItems();
        if(!isProcessSucessfull){
            return false;
        }
        
        new DatabaseQueryExecutor().close();
        isProcessSucessfull = removeOrder();
        return isProcessSucessfull;        
        
    }
    
    private boolean removeOrderItems(){
        boolean isRemoveDone = true;
        for(OrderItem current : deleteOrder.getItems()){
            boolean isCurrentOK = orderItemDAO.delete(current.getID());
            isRemoveDone = isRemoveDone && isCurrentOK;
            orderItemDAO.close();
        }
        return isRemoveDone;
    }
    
    private boolean removeOrder(){
        return orderDAO.delete(deleteOrder.getId());
    }
    
    
    
    
}

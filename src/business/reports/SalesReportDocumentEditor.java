package business.reports;

import static business.reports.ReportsSentencesResources.*;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SalesReportDocumentEditor {

    private final Document document;
    private final Paragraph BREAKLINE = new Paragraph(DOCUMENT_BREAK_LINE);

    public SalesReportDocumentEditor() {
        document = new Document();
    }

    public void openDocument(
            FileOutputStream destinationFile
    ) throws FileNotFoundException, DocumentException {
        PdfWriter.getInstance(document, destinationFile);
        document.open();
    }

    public void pasteTitleSection(String title) throws DocumentException {
        Paragraph titleParagraph = new Paragraph(DOCUMENT_TITLE_HEADER + title);
        document.add(titleParagraph);
        document.add(BREAKLINE);
    }

    public void pasteSection(PdfPTable section) throws DocumentException {
        document.add(section);
        document.add(BREAKLINE);
    }

    public void closeDocument() {
        document.close();
    }

    public void pasteLineSeparator() {
        LineSeparator ls = new LineSeparator();
        try {
            document.add(new Chunk(ls));
        }
        catch ( DocumentException ex ) {
            Logger.getLogger(SalesReportDocumentEditor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

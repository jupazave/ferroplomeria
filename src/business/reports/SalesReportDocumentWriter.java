package business.reports;

import static business.reports.ReportsSentencesResources.*;
import com.itextpdf.text.pdf.PdfPTable;
import entities.TicketSale;
import entities.TicketSaleItem;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

public class SalesReportDocumentWriter {

    private static final String DOLLAR_SIGN = "$";
    private static final int TWO_COLUMNS = 2;
    private static final int TRHEE_COLUMNS = 3;
    private static final String SALE_TOTAL = "Total De Venta";
    private static final String ALL_SALES_TOTAL = "Total De Todas Las Ventas";
    private final String TITLE_BEGINNING = "Del ";
    private final String TITLE_MIDDLE = " hasta ";
    private final String[] header = DOCUMENT_TICKET_TABLE_HEADER;

    public String writeDocumentTitle(Date dateFrom, Date dateTo) {
        if ( dateTo.equals(dateFrom) ) {
            return TITLE_BEGINNING + getFormattedDate(dateTo);
        }
        return TITLE_BEGINNING + getFormattedDate(dateFrom) + TITLE_MIDDLE + getFormattedDate(dateTo);
    }

    public PdfPTable writeSectionTableFromTicket(TicketSale ticket) {
        Set<TicketSaleItem> ticketItems = ticket.getItems();

        PdfPTable ticketSectionTable = new PdfPTable(TRHEE_COLUMNS);
        setHeaderToSectionTable(ticketSectionTable);

        Double total = 0.0;
        for ( TicketSaleItem item : ticketItems ) {
            addSoldProductRow(item, ticketSectionTable);
        }

        return ticketSectionTable;
    }

    public PdfPTable writeSaleTotalAmountSection(TicketSale involvedTicket) {
        PdfPTable totalTicketSaleSection = new PdfPTable(TWO_COLUMNS);
        totalTicketSaleSection.addCell(SALE_TOTAL);

        Double total = 0.0;
        for ( TicketSaleItem currentTicketSaleItem : involvedTicket.getItems() ) {
            total += ( currentTicketSaleItem.getQuantity() * currentTicketSaleItem.getUnitPrice() );
        }

        totalTicketSaleSection.addCell(DOLLAR_SIGN + String.valueOf(total));
        return totalTicketSaleSection;
    }

    public PdfPTable writeSalesTotalAmountSection(TicketSale[] involvedTickets) {
        PdfPTable totalSalesSection = new PdfPTable(TWO_COLUMNS);
        totalSalesSection.addCell(ALL_SALES_TOTAL);

        Double total = 0.0;
        for ( TicketSale sale : involvedTickets ) {
            for ( TicketSaleItem currentTicketSaleItem : sale.getItems() ) {
                total += currentTicketSaleItem.getQuantity() * currentTicketSaleItem.getUnitPrice();
            }
        }

        totalSalesSection.addCell(DOLLAR_SIGN + String.valueOf(total));
        return totalSalesSection;
    }

    private void setHeaderToSectionTable(PdfPTable ticketSectionTable) {
        ticketSectionTable.addCell(header[0]);
        ticketSectionTable.addCell(header[1]);
        ticketSectionTable.addCell(header[2]);
    }

    private void addSoldProductRow(TicketSaleItem item, PdfPTable ticketSectionTable) {
        ticketSectionTable.addCell(item.getProduct().getName());
        ticketSectionTable.addCell(String.valueOf(item.getQuantity()));
        ticketSectionTable.addCell(String.valueOf(item.getUnitPrice()));
    }

    private String getFormattedDate(Date date) {
        DateFormat formatter = new SimpleDateFormat(YEAR_MONTH_DAY_FORMAT);
        return formatter.format(date);
    }
}

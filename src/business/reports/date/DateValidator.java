/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.reports.date;

import java.util.Date;

/**
 *
 * @author luisburgos
 */
public class DateValidator {

    public static boolean validateDateRange(Date dateFrom, Date dateTo) {
        boolean isValidReportDate = false;           
                        
        if (dateFrom.before(dateTo) || dateFrom.equals(dateTo)) {
            isValidReportDate = true;
        } 
        
        return isValidReportDate;
    }

    public static boolean isDateInRange(Date dateFrom, Date dateTo, Date evaluatedDate) {
        boolean isDateInRange = false; 
        
        boolean inRangeBottom = evaluatedDate.before(dateTo) || evaluatedDate.equals(dateFrom);
        boolean inRangeUpper = evaluatedDate.after(dateFrom) || evaluatedDate.equals(dateFrom);
        if(inRangeBottom && inRangeUpper){
            isDateInRange = true;
        }        
        
        return isDateInRange;
    }
    
}

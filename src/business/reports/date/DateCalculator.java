package business.reports.date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DateCalculator {      
    
    public static Date getTodayDateWithoutTime() {                
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");        
        Date today = new Date();
        Date todayWithZeroTime = today;
        try {            
            todayWithZeroTime = formatter.parse(formatter.format(today));            
        } catch (ParseException ex) {
            Logger.getLogger(DateCalculator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return todayWithZeroTime;
    }
    
    public static Date getTodayDate() {        
        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");               
        Date today = new Date();
        Date todayWithZeroTime = today;
        try {            
            todayWithZeroTime = formatter.parse(formatter.format(today));            
        } catch (ParseException ex) {
            Logger.getLogger(DateCalculator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return todayWithZeroTime;
    }

    public static Date getMonthBeginningDate() {
        Date today = getTodayDate();                        
        Calendar calendar = new GregorianCalendar();        
        calendar.setTime(today);
        calendar.set(Calendar.DAY_OF_MONTH, 1);         
        Date beginningDateOfMonth = calendar.getTime();
        return beginningDateOfMonth;
    }
    
    public static Date incrementMillsOfDate(Date date) {
        Date today = date;                     
        Calendar calendar = new GregorianCalendar();        
        calendar.setTime(today);        
        long currentMillis = calendar.getTimeInMillis();        
        calendar.setTimeInMillis(currentMillis + 1);
        Date beginningDateOfMonth = calendar.getTime();
        return beginningDateOfMonth;
    }
}

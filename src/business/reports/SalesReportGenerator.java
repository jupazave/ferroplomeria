package business.reports;

import business.coordinators.BusinessManagement;
import business.reports.date.DateValidator;
import business.coordinators.TicketSaleCoordinator;
import static business.reports.ReportsSentencesResources.DATE_TIME_FORMAT;
import static business.reports.ReportsSentencesResources.DOCUMENT_NAME_DELIMETER;
import static business.reports.ReportsSentencesResources.NO_TICKETS_FROM_DATES;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPTable;
import data.FileHandler;
import entities.TicketSale;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import presentation.exceptions.InvalidDatesSelectionException;
import presentation.exceptions.ReportGeneratioErrorException;

public class SalesReportGenerator {

    private TicketSale[] involvedTickets;
    private final SalesReportDocumentWriter writer;
    private final SalesReportDocumentEditor editor;
    private final TicketSaleCoordinator ticketsCoordinator;
    private Date finalDate;
    private Date initialDate;
    private String destinationFolderPath;
    private FileHandler fileHandler = new FileHandler();

    public SalesReportGenerator() {
        ticketsCoordinator = BusinessManagement.getManager().getTicketSaleCoordinator();
        editor = new SalesReportDocumentEditor();
        writer = new SalesReportDocumentWriter();
    }

    public void createReport(Date initialDate, Date finalDate, String destinationFolderPath) throws InvalidDatesSelectionException, ReportGeneratioErrorException {

        setInitialDate(initialDate);
        setFinalDate(finalDate);
        setDestinationFolderPath(destinationFolderPath);
        String documentPath = generateDocumentPath();

        try {
            //Validation re-throws specific exceptions
            validateReportGenerationRequisites();
            editor.openDocument(fileHandler.createNewDocumentFile(documentPath));
            fillReportDocumentContents();
            editor.closeDocument();
        }
        catch ( DocumentException | FileNotFoundException ex ) {
            //This happens on Input/Output errors.
            ex.printStackTrace();
            throw new ReportGeneratioErrorException();
        }
        catch ( InvalidDatesSelectionException ex ) {
            //This is re-thrown when date range is wrong.
            throw ex;
        }
        catch ( ReportGeneratioErrorException ex ) {
            //This is re-thrown when requsites have failed.
            throw ex;
        }
    }

    public String getDestinationFolderPath() {
        return destinationFolderPath;
    }

    private Date getInitialDate() {
        return initialDate;
    }

    private void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    private void setInvolvedSaleTickets(TicketSale[] involvedTickets) {
        this.involvedTickets = involvedTickets;
    }

    private Date getFinalDate() {
        return finalDate;
    }

    private void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    private TicketSale[] getCorrespondingTicketSales(Date initialDate, Date finalDate) {
        return ticketsCoordinator.getListFromDates(initialDate, finalDate);
    }

    private void fillReportDocumentContents() throws DocumentException, FileNotFoundException, ReportGeneratioErrorException {
        String title = writer.writeDocumentTitle(initialDate, finalDate);

        //Title
        editor.pasteTitleSection(title);

        //Body
        editor.pasteLineSeparator();

        for ( TicketSale ticket : involvedTickets ) {
            PdfPTable ticketTable = writer.writeSectionTableFromTicket(ticket);
            editor.pasteSection(ticketTable);
            PdfPTable totalTicketSale = writer.writeSaleTotalAmountSection(ticket);
            editor.pasteSection(totalTicketSale);
        }

        editor.pasteLineSeparator();

        //Footer
        PdfPTable totalSalesTable = writer.writeSalesTotalAmountSection(involvedTickets);
        editor.pasteSection(totalSalesTable);
    }

    private void setDestinationFolderPath(String destinationFolderPath) {
        this.destinationFolderPath = destinationFolderPath;
    }

    private void validateReportGenerationRequisites() throws InvalidDatesSelectionException, ReportGeneratioErrorException {

        //Date requisites
        boolean dateRangeIsValid;
        dateRangeIsValid = DateValidator.validateDateRange(getInitialDate(), getFinalDate());

        if ( !dateRangeIsValid ) {
            throw new InvalidDatesSelectionException();
        }

        //Involved ticket requisites
        setInvolvedSaleTickets(getCorrespondingTicketSales(getInitialDate(), getFinalDate()));

        boolean ticketSalesHeldLesserThanZero;
        ticketSalesHeldLesserThanZero = involvedTickets.length <= 0;

        if ( ticketSalesHeldLesserThanZero ) {
            throw new ReportGeneratioErrorException(NO_TICKETS_FROM_DATES);
        }

    }

    private String generateDocumentPath() {
        String documentName = new SimpleDateFormat(DATE_TIME_FORMAT).format(new Date());
        documentName = getDestinationFolderPath() + DOCUMENT_NAME_DELIMETER + documentName;
        return documentName;
    }

}

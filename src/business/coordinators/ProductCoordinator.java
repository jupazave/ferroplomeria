
package business.coordinators;

import business.interfaces.business.Coordinator;
import data.ItemNotFoundException;
import data.ProductNotFoundException;
import data.daos.ProductDAO;
import entities.Item;
import entities.Product;

public class ProductCoordinator extends Coordinator<Product> {
    
    private final ProductDAO dataAccessObject;
    private final ItemCoordinator itemCoordinator;

    public ProductCoordinator() {
        dataAccessObject = new ProductDAO();
        itemCoordinator = new ItemCoordinator();
    }

    @Override
    public boolean add(Product newProduct) {
        Item linkedItem;
        try {
            linkedItem = itemCoordinator.getByProperties(newProduct.getName(), newProduct.getBrand());
        }
        catch ( ItemNotFoundException ex ) {
            linkedItem = new Item();
        }
        if ( isValidItem(linkedItem) ) {
            newProduct.setItem(linkedItem);
            return addProductExistingItem(newProduct);
        }
        else {
            return addProductNewItem(newProduct);
        }
    }

    @Override
    public boolean remove(int id) {
        return dataAccessObject.delete(id);
    }

    @Override
    public boolean edit(Product modifiedEntity) {
        return itemCoordinator.edit(modifiedEntity.getItem()) && dataAccessObject.update(modifiedEntity);
    }

    @Override
    public Product get(int id) {
        return (Product) dataAccessObject.getById(id);
    }

    @Override
    public Product[] getList() {
        return (Product[]) dataAccessObject.getList();
    }

    public Product[] getProductListByCategory(int categoryID) {
        return dataAccessObject.getProductListByCategoryId(categoryID);
    }

    public boolean updateProductStock(int productID, int newStock) {
        return dataAccessObject.updateProductStock(productID, newStock);
    }

    private Product getItemProduct(int itemId) throws ProductNotFoundException {
        return dataAccessObject.getProductByItemId(itemId);
    }

    private boolean addProductNewItem(Product newProduct) {
        itemCoordinator.add(newProduct.getItem());
        Item linkedItem;
        try {
            linkedItem = itemCoordinator.getByProperties(newProduct.getName(), newProduct.getBrand());
        }
        catch ( ItemNotFoundException ex ) {
            linkedItem = new Item();
        }
        if ( isValidItem(linkedItem) ) {
            newProduct.setItem(linkedItem);
            itemCoordinator.close();
            return dataAccessObject.insert(newProduct);
        }
        else {
            return false;
        }
    }

    private boolean addProductExistingItem(Product newProduct) {
        itemCoordinator.close();
        if ( itemCoordinator.isProduct(newProduct.getItem()) ) { //ES PRODUCTO
            return false;
        }
        else {
            return dataAccessObject.insert(newProduct);
        }
    }

    private boolean isValidItem(Item checkItem) {
        return checkItem.getID() > 0;
    }
}

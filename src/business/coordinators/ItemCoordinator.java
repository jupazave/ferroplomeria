
package business.coordinators;

import business.interfaces.business.Coordinator;
import data.ItemNotFoundException;
import data.daos.ItemDAO;
import entities.Item;

public class ItemCoordinator extends Coordinator<Item> {
    
    private final ItemDAO dataAccessObject;


    public ItemCoordinator() {
        dataAccessObject = new ItemDAO();
    }

    @Override
    public boolean add(Item entity) {
        return dataAccessObject.insert(entity);
    }

    @Override
    public boolean remove(int id) {
        return dataAccessObject.delete(id);
    }

    @Override
    public boolean edit(Item entity) {
        return dataAccessObject.update(entity);
    }

    @Override
    public Item get(int id) {
        return ( Item ) dataAccessObject.getById(id);
    }

    @Override
    public Item[] getList() {
        return dataAccessObject.getList();
    }

    public Item[] getDemands() {
        return dataAccessObject.getDemands();
    }

    public Item getByProperties(String name, String brand) throws ItemNotFoundException {
        return dataAccessObject.findByNameBrand(name, brand);
    }

    public boolean isProduct(Item checkItem) {
        return dataAccessObject.isProduct(checkItem.getID());
    }

    public void close() {
        dataAccessObject.close();
    }

}

package business.coordinators;

public class BusinessManagement {
    
    private static BusinessManagement management;

    private CategoryCoordinator categoryCoordinator;
    private ItemCoordinator itemCoordinator;
    private OrderCoordinator orderCoordinator;
    private ProductCoordinator productCoordinator;
    private SupplierCoordinator supplierCoordinator;
    private TicketSaleCoordinator ticketSaleCoordinator;

    private BusinessManagement() {
        this.categoryCoordinator = new CategoryCoordinator();
        this.itemCoordinator = new ItemCoordinator();
        this.orderCoordinator = new OrderCoordinator();
        this.productCoordinator = new ProductCoordinator();
        this.supplierCoordinator = new SupplierCoordinator();
        this.ticketSaleCoordinator = new TicketSaleCoordinator();
    }
    
    public static BusinessManagement getManager() {
        if (management == null) {
             management = new BusinessManagement();
        }
        return management;
    }
    
    public CategoryCoordinator getCategoryCoordinator() {
        return categoryCoordinator;
    }

    public ItemCoordinator getItemCoordinator() {
        return itemCoordinator;
    }

    public OrderCoordinator getOrderCoordinator() {
        return orderCoordinator;
    }

    public ProductCoordinator getProductCoordinator() {
        return productCoordinator;
    }

    public SupplierCoordinator getSupplierCoordinator() {
        return supplierCoordinator;
    }

    public TicketSaleCoordinator getTicketSaleCoordinator() {
        return ticketSaleCoordinator;
    }
    
    
    
    
}

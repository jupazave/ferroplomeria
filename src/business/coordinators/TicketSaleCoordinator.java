/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package business.coordinators;

import business.process.TicketSaleAdditionProcess;
import business.interfaces.business.Coordinator;
import business.reports.date.DateValidator;
import data.daos.TicketSaleDAO;
import entities.TicketSale;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author luisburgos
 */
public class TicketSaleCoordinator extends Coordinator<TicketSale> {
    
    private final TicketSaleDAO dataAccessObject;

    public TicketSaleCoordinator() {
        dataAccessObject = new TicketSaleDAO();
    }


    @Override
    public boolean add(TicketSale entity) {
        TicketSaleAdditionProcess additionProcess = new TicketSaleAdditionProcess(entity);
        return additionProcess.execute();
    }

    @Override
    public boolean remove(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean edit(TicketSale entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TicketSale get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TicketSale[] getList() {
        return ( TicketSale[] ) dataAccessObject.getList();
    }

    //TODO: Convert to a process.
    public TicketSale[] getListFromDates(Date dateFrom, Date dateTo) {
        ArrayList<TicketSale> filteredList = new ArrayList<>();
        TicketSale[] completeList = ( TicketSale[] ) dataAccessObject.getList();

        for ( TicketSale currentSale : completeList ) {
            System.out.println(currentSale.getDate());
            Date currentSaleDate = currentSale.getDate();
            if ( DateValidator.isDateInRange(dateFrom, dateTo, currentSaleDate) ) {
                filteredList.add(currentSale);
            }
        }
        
        return filteredList.toArray(new TicketSale[filteredList.size()]);
    }

}


package business.coordinators;

import business.interfaces.business.Coordinator;
import data.daos.SupplierDAO;
import entities.Supplier;

public class SupplierCoordinator extends Coordinator<Supplier> {

    private final SupplierDAO dataAccessObject;
    
    public SupplierCoordinator() {
        dataAccessObject = new SupplierDAO();
    }


    @Override
    public boolean add(Supplier entity) {
        return dataAccessObject.insert(entity);
    }

    @Override
    public boolean remove(int id) {
        return dataAccessObject.delete(id);
    }

    @Override
    public boolean edit(Supplier entity) {
        return dataAccessObject.update(entity);
    }

    @Override
    public Supplier get(int id) {
        return (Supplier) dataAccessObject.getById(id);
    }

    @Override
    public Supplier[] getList() {
        return (Supplier[]) dataAccessObject.getList();
    }

}

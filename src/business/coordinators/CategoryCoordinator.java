package business.coordinators;

import business.interfaces.business.Coordinator;
import data.daos.CategoryDAO;
import entities.Category;
import entities.Product;

public class CategoryCoordinator extends Coordinator<Category> {
    
    private final CategoryDAO dataAccessObject;

    public CategoryCoordinator() {
        dataAccessObject = new CategoryDAO();
    }

    @Override
    public boolean add(Category entity) {
        return dataAccessObject.insert(entity);
    }

    @Override
    public boolean remove(int id) {
        return dataAccessObject.delete(id);
    }

    @Override
    public boolean edit(Category entity) {
        return dataAccessObject.update(entity);
    }

    @Override
    public Category get(int id) {
        return (Category) dataAccessObject.getById(id);
    }

    @Override
    public Category[] getList() {
        return (Category[]) dataAccessObject.getList();
    }

    public boolean isCategoryEmpty(int categoryID){ 
        Product[] categoryProducts;       
        categoryProducts = BusinessManagement.getManager().getProductCoordinator().getProductListByCategory(categoryID);
        return !(categoryProducts.length > 0);
    }
    
}

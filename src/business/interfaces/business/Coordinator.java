/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.interfaces.business;

import business.interfaces.data.DAO;

/**
 *
 * @author luisburgos
 * @param <Entity>
 */
public abstract class Coordinator<Entity> {
    
    public abstract boolean add(Entity entity);
    public abstract boolean remove(int id);
    public abstract boolean edit(Entity entity);
    public abstract Entity get(int id);
    public abstract Entity[] getList();

}

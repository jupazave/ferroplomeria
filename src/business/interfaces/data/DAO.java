/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.interfaces.data;

/**
 *
 * @author luisburgos
 * @param <Entity>
 */
public interface DAO<Entity> {

    public boolean insert(Entity newEntity);

    public boolean delete(int existingEntityID);

    public boolean update(Entity updatedEntity);

    public Entity getById(int entityID);

    public Entity[] getList();

}


package application;

import javax.swing.JFrame;
import presentation.control.LooksLoader;
import presentation.control.WindowManager;
import presentation.control.WindowReference;

public class ApplicationLauncher {

    public static void main(String[] args) {
        LooksLoader.load();
        JFrame mainWindow;
        mainWindow = WindowManager.getInstance().getWindow(WindowReference.MAIN);
        mainWindow.setVisible(true);
    }

}

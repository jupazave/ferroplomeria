package entities;

/**
 * Represents an item of a DeliveredOrder object ({@link DeliveredOrder}) and holds 
 * information about the order to which it belongs, the product's name 
 * of which wants supplies, the desired quantity of the product and
 * the price given by the supplier.
 * 
 */
public class OrderItem {
        
    private int orderID;
    private Item item;
    private int quantity;
    private int id;
    
    public OrderItem(){}

    /**
     * Creates a new order of products to be supplied.
     * @param id the id of this order item.
     * @param orderID the associated order identifier.
     * @param item the associated order item.
     * @param quantity the number of products to be supplied.
     */
    public OrderItem(
            int id,
            int orderID, 
            Item item,
            int quantity
    ) {
        this.id = id;
        this.orderID = orderID;
        this.quantity = quantity;
        this.item = item;
    }

    /**
     * @return a DeliveredOrder  associated with this item  
     */
    public int getOrderID() {
        return orderID;
    }

    /**
     * @param orderID the DeliveredOrder associated with this item
     */
    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    /**
     * @return a String representing the product's name of which 
     * wants supplies.
     */
    public String getName() {
        return item.getName();
    }

    /**
     * @return a Integer representing the number of products to be supplied.
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the number of products to be supplied.
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public String getBrand(){
        return item.getBrand();
    }
    
    public void setItem(Item item){
        this.item = item;
    }
    
    public Item getItem(){
        return item;
    }
    
    public void setID(int id){
        this.id = id;
    }
    
    public int getID(){
        return id;
    }
    
    
}

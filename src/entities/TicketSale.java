package entities;

import java.util.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents a particular sales note, holds information about a
 * purchase of a client, like the total of the purchase, the discount
 * applied and the date. 
 * The products associated with this ticket sale are represented by 
 * a set of {@link Sale} class items.
 * 
 * @author luisburgos
 */
public class TicketSale {
    
    private int id;
    private Date date;
    private final Set<TicketSaleItem> items = new HashSet<>();
   
    public TicketSale(){}
    /**
     * Creates a new ticket sale for a client purchase.
     * @param id the ticket sale unique identifier.
     * @param date the date of the purchase.
     */
    public TicketSale(
            int id,  
            Date date
    ) {
        this.id = id;
        this.date = date;
    }

    /**     
     * @return an Integer representing the ticket's sale identifier.
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the ticket's sale identifier.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return a Date object representing the date of the purchase.
     * The format of the date will be "yyyy/mm/dd".
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date represents the date of a client purchase.
     * The format of the date should be "yyyy/mm/dd".
     */
    public void setDate(Date date) {
        this.date = date;
    }
    
    public void addItem(TicketSaleItem item) {
        items.add(item);
    }
    
    public void removeItem(TicketSaleItem item) {
        items.remove(item);
    }
    
    public Set<TicketSaleItem> getItems() {
       return Collections.unmodifiableSet(items);        
    }    
    
}

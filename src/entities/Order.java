package entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents an order to a supplier of various desired products and
 * holds all the information about it. The order can contain products
 * that already exists on the inventory and new products that the 
 * business wants to start distributing.
 */
public class Order {
    
    protected int id;
    protected Supplier supplier;
    protected List<OrderItem> items = new ArrayList<>();

    public Order(){}
    
    /**
     * Creates a new order of a product's set.
     * @param id the order identifier.
     * @param supplier the supplier's identifier associated.
     */
    public Order(int id,Supplier supplier){
        
    }
    
    /**
     * @return an Integer representing the order identifier.
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the order identifier.
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * @return an Supplier representing the associated supplier
     * to this order
     */
    public Supplier getSupplier() {
        return supplier;
    }

    /**
     * @param supplier the associated supplier to this order
     */
    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }
    
    public void addItem(OrderItem item){
        items.add(item);
    }
    
    public void removeItem(OrderItem item){
        items.remove(item);
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void addAllItems(List<OrderItem> items) {
        this.items.addAll(items);
    }
    
    private OrderItem getItemAt(int index){
        return items.get(index);
    }
    
}

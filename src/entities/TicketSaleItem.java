package entities;

/**
 * Represents an item of a TicketSale object and holds information about
 the ticket to which it belongs, the product sold, the quantity of the
 product sold and the unitPrice price of this item.
 * 
 * @author luisburgos
 */
public class TicketSaleItem {
    
    private int ticketSaleID;
    private Product product;
    private int quantity;
    private Double unitPrice;

    public TicketSaleItem (){}
    
    /**
     * Creates a new item sale
     * @param ticketSaleID ticket sale identifier to which it belongs.
     * @param product product identifier to which it belongs.
     * @param quantity number of products to be sold.
     * @param unitPrice the unitPrice price of the item sale.
     */
    public TicketSaleItem(
            int ticketSaleID, 
            Product product, 
            int quantity, 
            Double unitPrice
    ) {
        this.ticketSaleID = ticketSaleID;
        this.product = product;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }

    /**
     * @return an TicketSale representing the ticket sale  associated with
     * this item sale 
     */
    public int getTicketSaleID() {
        return ticketSaleID;
    }

    /**  
     * @param ticketSale the ticket sale associated with
     * this item sale 
     */
    public void setTicketSaleID(int ticketSale) {
        this.ticketSaleID = ticketSale;
    }

    /**
     * @return a Product representing the associated 
     * product contain in the item sale.
     */
    public Product getProduct() {
        return product;
    }

    /**     
     * @param product product associated to be sold.
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * @return an Integer representing the number of products to
     * be sold.
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the number of products to be sold in
     * the item sale.
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**     
     * @return a Double representing the unitPrice price of the item sale.
     * The unitPrice is calculated by multiplying the product quantity 
     * {@link Sale#productQuantity} by the product's outtake price
     * {@link Product#outtakePrice} which is in "0.00" decimal
     * format.
     */
    public Double getUnitPrice() {
        return unitPrice;
    }

    /**    
     * @param unitPrice the unitPrice price of the item sale.
     * The unitPrice price should be in "0.00" decimal format.
     * The unitPrice is calculated by multiplying the product quantity
     * to be sold {@link Sale#productQuantity} by the product's outtake
     * price {@link Product#outtakePrice}
     */
    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }
    
}

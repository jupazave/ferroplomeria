package entities;

/**
 * Represents an already supplied order to a supplier
 */
public class DeliveredOrder extends Order {
    

    private Double totalCost;
    private Double balance;
    private Double amountPaid;
    //private final Set<OrderItem> items = new HashSet<>();

    public DeliveredOrder () {}
    
    /**
     * Creates a new order of a product's set.
     * @param id the order identifier.
     * @param supplier the supplier's identifier associated.
     * @param total the totalCost amount to be paid.
     * @param totalAmountPaid the current amount already paid.     
     */
    public DeliveredOrder(
            int id, 
            Supplier supplier, 
            Double total, 
            Double totalAmountPaid  
    ) {
        this.id = id;
        this.supplier = supplier;
        this.totalCost = total;
        this.amountPaid = totalAmountPaid;
        updateBalance();
    }

    /**
     * @return a Double representing the totalCost amount to be paid to the 
     * supplier for this particular order.
     * The totalCost will be in "0.00" decimal format.
     */
    public Double getTotal() {
        return totalCost;
    }

    /**
     * @param total the totalCost amount to be paid to the supplier for 
     * this particular order.
     * The totalCost should be in "0.00" decimal format.
     */
    public void setTotal(Double total) {
        this.totalCost = total;
        updateBalance();
    }

    /**
     * @return a Double representing the remaining amount to be paid.
     * The balance will be in "0.00" decimal format.
     */
    public Double getBalance() {
        return balance;
    }

    /**
     * @return a Double representing the current amount already paid.
     * The amountPaid amount will be in "0.00" decimal format.
     */
    public Double getTotalAmountPaid() {
        return amountPaid;
    }

    /**
     * @param totalAmountPaid the current amount already paid. 
 The amountPaid amount should be in "0.00" decimal format.
     */
    public void setTotalAmountPaid(Double totalAmountPaid) {
        this.amountPaid = totalAmountPaid;
        updateBalance();
    }
      
    public void addItem(OrderItem item) {
        items.add(item);
    }
    
    public void removeItem(OrderItem item) {
        items.remove(item);
    }
    
    private void updateBalance(){
        balance = totalCost - amountPaid;
    }
    
}

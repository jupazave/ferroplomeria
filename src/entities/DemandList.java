package entities;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents the list of missing products, this could be because the
 * product stock is equal to cero or because the business wants to
 * start distributing a new product.
 * 
 * @author luisburgos
 */
public class DemandList {
  
    private static DemandList demandsList;
    private final Set<Item> items = new HashSet<>();
   
    private DemandList() { }
    
    /**
     * @return the Instance of the DemandsList
     */
    public synchronized static DemandList getList(){
        if(demandsList == null){
            demandsList = new DemandList();
        }
        return demandsList;
    }
    
    public void addItem(Item item) {
        items.add(item);
    }
    
    public void removeItem(Item item) {
        items.remove(item);
    }
    
    public Set getItems() {
       return Collections.unmodifiableSet(items);        
    }
    
    public void initializeDemandsListItems(Set<Item> items) {
        items.addAll(items);
    }
    
}

package entities;

/**
 * Holds information about a particular product in stock.
 */
public class Product {
    
    private Item item;
    private int id;
    private String description;
    private Double cost;
    private Double price;
    private int stock;
    private Category category;

    public Product(){
        item = new Item();
    }
    
    /**
     * Creates a new product
     * @param id product's identifier
     * @param productItem the product's item behind it.
     * @param description brief details of the product, e,g. "Red, model K-11"
     * @param cost the expense that a business incurs in bringing a product or 
     * service to market.
     * @param price is the amount a customer pays for that product or service.
     * @param stock number of items in the inventory
     * @param category  the product's classification, e.g. "Pipelines"
     */
    public Product(
            int id, 
            Item productItem, 
            String description, 
            Double cost,
            Double price,
            int stock,
            Category category
    ) {
        this.id = id;
        this.item = productItem;
        this.description = description;
        this.cost = cost;
        this.price = price;
        this.stock = stock;
        this.category = category;
    }
    
    /**
     * Creates a new product not linked with the database
     * @param name the product's name, e.g. "Hammer"
     * @param description brief details of the product, e,g. "Red, model K-11"
     * @param brand e.g. "Truper"
     * @param cost the expense that a business incurs in bringing a product or 
     * service to market.
     * @param price is the amount a customer pays for that product or service.
     * @param stock number of items in the inventory
     * @param category  the product's classification, e.g. "Pipelines"
     */    
    public Product(
            String name, 
            String description, 
            String brand, 
            Double cost,
            Double price,
            int stock,
            Category category
    ) {
        item = new Item();
        setName(name);
        this.description = description;
        setBrand(brand);
        this.cost = cost;
        this.price = price;
        this.stock = stock;
        this.category = category;
    }

    /**
     * Creates a new product
     * @param id product's identifier
     * @param name the product's name, e.g. "Hammer"
     * @param description brief details of the product, e,g. "Red, model K-11"
     * @param brand e.g. "Truper"
     * @param cost the expense that a business incurs in bringing a product or 
     * service to market.
     * @param price is the amount a customer pays for that product or service.
     * @param stock number of items in the inventory
     * @param category  the product's classification, e.g. "Pipelines"
     */
    public Product(
            int id, 
            String name, 
            String description, 
            String brand, 
            Double cost,
            Double price,
            int stock,
            Category category
    ) {
        item = new Item();
        this.id = id;
        setName(name);
        this.description = description;
        setBrand(brand);
        this.cost = cost;
        this.price = price;
        this.stock = stock;
        this.category = category;
    }

    /**
     * @return an Integer representing the product's unique identifier.
     */
    public int getId() {
        return id;
    }

    /**
     * @param id product's identifier.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return a String representing the product's name.
     */
    public String getName() {
        return item.getName();
    }

    /**
     * @param name the product's name.
     */
    public void setName(String name) {
        item.setName(name);
    }

    /**
     * @return a String representing the product's description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description a brief product's explanation.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return a String representing the product's brand.
     */
    public String getBrand() {
        return item.getBrand();
    }

    /**
     * @param brand the product's brand.
     */
    public void setBrand(String brand) {
        item.setBrand(brand);
    }

    /**
     * @return a Double representing the expense that a business incurs in 
     * bringing a product or service to market.
     * The price is in "0.00" decimal format.
     */
    public Double getCost() {
        return cost;
    }

    /**
     * @param cost the expense that a business incurs in bringing a product or 
     * service to market.
     * The price is in "0.00" decimal format.
     */
    public void setCost(Double cost) {
        this.cost = cost;
    }

    /**
     * @return a Double representing the amount a customer pays for that 
     * product or service.
     * The price is in "0.00" decimal format.
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price the amount a customer pays for that product or service.
     * The price is in "0.00" decimal format.
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * @return a Integer representing the current number of
     * items in the inventory.
     */
    public int getStock() {
        return stock;
    }

    /**
     * @param stock the number of items in the inventory.
     */
    public void setStock(int stock) {
        this.stock = stock;
    }
    
    /**    
     * @param category the category to which the product belongs
     */
    public void setCategory(Category category) {
        this.category = category;
    }
    
    /**
     * @return an Category representing the category to which the
     * product belongs.
     */
    public Category getCategory() {
        return category;
    }
    
    public void setItem(Item newItem){
        item = newItem;
    }
    
    public Item getItem(){
        return item;
    }

    @Override
    public String toString() {
        return item.getName() + " - " + item.getBrand();
    }

    
}

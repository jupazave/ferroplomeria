package entities;

/**
 * Represents the category that a product can belong.
 * A category is defined by the business and it is assigned to 
 * an item when it is entered to the inventory.
 */
public class Category {
    
    private int id;
    private String name;
    private String description;
    
    public Category(){
        this("","");
    }
    
    /**
     * Creates a new product category
     * @param name the category's name;
     * @param description the category's description.
     */
    public Category(String name, String description){
        this.name = name;
        this.description = description;
    }
   
    /**
     * Creates a new product category
     * @param id the category's identifier.
     * @param name the category's name;
     * @param description the category's description.
     */
    public Category(int id, String name, String description) {        
        this.id = id;
        this.name = name;
        this.description = description;
    }

    /**   
     * @return an Integer representing the category's identifier.
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the category's identifier.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return a String representing the category's name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the category's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return a String representing the category's detailed explanation.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the category's detailed explanation.
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    @Override
    public String toString(){
        return name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Category other = (Category) obj;
        return this.getId() == other.getId();
    }
    
}

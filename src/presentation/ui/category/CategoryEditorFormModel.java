package presentation.ui.category;

import entities.Category;
import javax.swing.JOptionPane;
import presentation.exceptions.OperationErrorException;
import presentation.resources.StringResources;
import presentation.components.forms.FormDialog;
import presentation.control.status.StatusDialog;

public class CategoryEditorFormModel extends CategoryFormModel{

    public CategoryEditorFormModel(FormDialog dialog) {
        super(dialog);
        castedForm.setEditMode();
    }

    @Override
    public void executeOperation() throws OperationErrorException {
        Category toEdit = compose();
        if(categoryCoordinator.edit(toEdit)){
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.EDIT_CATEGORY_SUCCESS_MESSAGE, toEdit.getName()),
                    null, 
                    JOptionPane.INFORMATION_MESSAGE
            ).setVisible(true);
        } else {
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.EDIT_PRODUCT_FAIL_MESSAGE, toEdit.getName()),
                    null, 
                    JOptionPane.ERROR_MESSAGE
            ).setVisible(true);
            throw new OperationErrorException();
        }
    }
    
}

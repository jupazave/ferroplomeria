package presentation.ui.category;

import business.coordinators.BusinessManagement;
import business.coordinators.CategoryCoordinator;
import business.interfaces.business.Coordinator;
import entities.Category;
import presentation.components.forms.FormDialog;
import presentation.components.forms.FormModel;

public abstract class CategoryFormModel extends FormModel<Category> {

    protected CategoryFormDialog castedForm;
    protected Coordinator<Category> categoryCoordinator;
    protected int categoryID;

    public CategoryFormModel(FormDialog dialog) {
        super(dialog);
        castedForm = (CategoryFormDialog) dialog;
        categoryCoordinator = BusinessManagement.getManager().getCategoryCoordinator();
        castedForm.clearForm();
    }

    @Override
    public Category compose() {
        return new Category(
                categoryID,
                castedForm.nameField.getText(),
                castedForm.descriptionField.getText()
        );
    }

    @Override
    public void fill(Category entity) {
        categoryID = entity.getId();
        castedForm.nameField.setText(entity.getName());
        castedForm.descriptionField.setText(entity.getDescription());
    }

}

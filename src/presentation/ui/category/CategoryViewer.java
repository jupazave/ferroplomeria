package presentation.ui.category;

import presentation.components.viewers.Viewer;
import presentation.resources.StringResources;

/**
 *
 * @author luisburgos
 */
public class CategoryViewer extends Viewer {

    private final String[] tabHeaderContent = new String[]{
        "ID", "Nombre", "Descripción"           
    };
    
    private final boolean[] columsVisibility = new boolean[]{
        false
    };
    
    public CategoryViewer() {
       super(StringResources.CATEGORY_VIEWER_TITLE);
       handler = new CategoryViewerHandler(this);
       setTableHeader(tabHeaderContent,columsVisibility);
       updateEntitiesTable();
    }   
}

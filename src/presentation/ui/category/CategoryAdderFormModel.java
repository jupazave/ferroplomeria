package presentation.ui.category;

import entities.Category;
import javax.swing.JOptionPane;
import presentation.exceptions.OperationErrorException;
import presentation.resources.StringResources;
import presentation.components.forms.FormDialog;
import presentation.control.status.StatusDialog;

public class CategoryAdderFormModel extends CategoryFormModel {
    
    public CategoryAdderFormModel(FormDialog dialog) {
        super(dialog);
    }

    @Override
    public void executeOperation() throws OperationErrorException {
        Category toAdd = compose();
        if(categoryCoordinator.add(toAdd)){
            new StatusDialog(castedForm,
                    StringResources.getResource(StringResources.ADD_CATEGORY_SUCCESS_MESSAGE, toAdd.getName()),
                    null, JOptionPane.INFORMATION_MESSAGE).setVisible(true);
        }else {
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.ADD_CATEGORY_FAIL_MESSAGE, toAdd.getName()),
                    null, JOptionPane.ERROR_MESSAGE
            ).setVisible(true);
            throw new OperationErrorException();
        }
    }
    
}

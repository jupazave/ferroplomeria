
package presentation.ui.category;

import business.coordinators.BusinessManagement;
import business.coordinators.CategoryCoordinator;
import business.interfaces.business.Coordinator;
import entities.Category;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import presentation.resources.StringResources;
import presentation.components.viewers.Viewer;
import presentation.components.viewers.ViewerHandler;
import presentation.components.forms.FormModel;
import presentation.control.DialogReference;
import presentation.control.status.StatusDialog;

/**
 *
 * @author luisburgos
 */
public class CategoryViewerHandler extends ViewerHandler {

    private Coordinator<Category> coordinator;

    public CategoryViewerHandler(Viewer viewer) {
        super(viewer);
        coordinator = BusinessManagement.getManager().getCategoryCoordinator();
        viewer.addButton(createAddButton(), Viewer.ADD_BUTTON);
        viewer.addButton(createRemoveButton(), Viewer.REMOVE_BUTTON);
        viewer.addButton(createEditButton(), Viewer.EDIT_BUTTON);

        //FEATURES TESTS
        //viewer.addSearchPanel(createSearchPanel(), createSearchTextField(), createSearchButton());
        //viewer.addButton(createButton("Prueba", "test", "cart_add.png"), Viewer.CUSTOM_BUTTON);
    }

    @Override
    public void handleAddActtion() {
        FormModel model = presentation.control.WindowManager.getInstance().getForm(DialogReference.CATEGORY_ADDER);
        model.showDialog();
        System.out.println(String.valueOf(model.isSubmitSucessfull()));
        if ( model.isSubmitSucessfull() ) {
            fillTable();
        }
    }

    @Override
    public void handleEditAction() {
        int id = ( int ) viewer.getEntitiesTable().getDataFromModel(viewer.getEntitiesTable().getSelectedRow(), 0);
        FormModel model = presentation.control.WindowManager.getInstance().getForm(DialogReference.CATEGORY_EDITOR);
        model.fill(coordinator.get(id));
        model.showDialog();
        if ( model.isSubmitSucessfull() ) {
            fillTable();
        }
    }

    @Override
    public void handleRemoveAction() {
        int id = ( int ) viewer.getEntitiesTable().getDataFromModel(viewer.getEntitiesTable().getSelectedRow(), 0);
        String name = ( String ) viewer.getEntitiesTable().getDataFromModel(viewer.getEntitiesTable().getSelectedRow(), 1);
        int isConfirmed = JOptionPane.showConfirmDialog(
                null, StringResources.getResource(StringResources.DELETE_CATEGORY_CONFIRMATION_MESSAGE, name, String.valueOf(id)),
                null, JOptionPane.YES_NO_OPTION);
        if ( JOptionPane.YES_OPTION == isConfirmed ) {
            if (  BusinessManagement.getManager().getCategoryCoordinator().isCategoryEmpty(id) ) {
                if ( coordinator.remove(id) ) {
                    new StatusDialog(null, StringResources.getResource(StringResources.DELETE_CATEGORY_SUCCESS_MESSAGE, name)).setVisible(true);
                    fillTable();
                }
                else {
                    new StatusDialog(null, StringResources.getResource(StringResources.DELETE_CATEGORY_FAIL_MESSAGE, name),
                                                  "Error", JOptionPane.ERROR_MESSAGE).setVisible(true);
                }
            }
            else {
                new StatusDialog(null, StringResources.getResource(StringResources.DELETE_CATEGORY_IS_NOT_EMPTY_MESSAGE, name)).setVisible(true);
            }
        }
    }

    @Override
    public void fillTable() {
        viewer.getEntitiesTable().clearRows();
        DefaultTableModel tableModel = ( DefaultTableModel ) viewer.getEntitiesTable().getModel();
        Category[] categories = coordinator.getList();
        for ( Category current : categories ) {
            tableModel.addRow(new Object[]{
                current.getId(),
                current.getName(),
                current.getDescription(), });
        }
    }

}

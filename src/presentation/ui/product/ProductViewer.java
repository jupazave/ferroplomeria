package presentation.ui.product;

import presentation.components.viewers.Viewer;
import presentation.resources.StringResources;

public class ProductViewer extends Viewer {
    
    private final String[] tabHeaderContent = new String[]{
        "ID", "Nombre", "Descripción", "Marca", "Categoria", "Costo", "Precio",
    };
    
    private final boolean[] columsVisibility = new boolean[]{
        false
    };
    
    public ProductViewer() {
       super(StringResources.PRODUCT_VIEWER_TITLE);
       handler = new ProductViewerHandler(this);
       setTableHeader(tabHeaderContent,columsVisibility);
       updateEntitiesTable();
    }   
}

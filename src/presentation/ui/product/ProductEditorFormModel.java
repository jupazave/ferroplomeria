package presentation.ui.product;

import entities.Product;
import javax.swing.JOptionPane;
import presentation.exceptions.OperationErrorException;
import presentation.resources.StringResources;
import presentation.components.forms.FormDialog;
import presentation.control.status.StatusDialog;

public class ProductEditorFormModel extends ProductFormModel {
    
    public ProductEditorFormModel(FormDialog dialog) {
        super(dialog);
        castedForm.setEditMode();
    }

    @Override
    public void executeOperation() throws OperationErrorException{
        Product toEdit = compose();
        if(productCoordinator.edit(toEdit)){
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.EDIT_PRODUCT_SUCCESS_MESSAGE, toEdit.getName()),
                    null, 
                    JOptionPane.INFORMATION_MESSAGE
            ).setVisible(true);
        } else {
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.EDIT_PRODUCT_FAIL_MESSAGE, toEdit.getName()),
                    null, 
                    JOptionPane.ERROR_MESSAGE
            ).setVisible(true);
            throw new OperationErrorException();
        }
    }
    
    
}

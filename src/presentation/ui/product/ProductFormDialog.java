
package presentation.ui.product;

import java.awt.event.ItemEvent;
import javax.swing.JFrame;
import presentation.components.forms.EditionForm;
import presentation.resources.StringResources;
import presentation.components.forms.FormDialog;

public class ProductFormDialog extends FormDialog implements EditionForm {

    public ProductFormDialog(JFrame window, boolean modal) {
        super(window,modal);
        initComponents();
    }
    
    @Override
    public void clearForm() {
        nameField.setText(StringResources.ADD_PRODUCT_DEFAULT_NAME);
        descriptionField.setText(StringResources.ADD_PRODUCT_DEFAULT_DESCRIPTION);
        brandField.setText(StringResources.ADD_PRODUCT_DEFAULT_BRAND);
        costField.setText(StringResources.ZERO_DOUBLE);
        priceField.setText(StringResources.ZERO_DOUBLE);
        profitValueLabel.setText(StringResources.ZERO_DOUBLE);
        stockField.setText(StringResources.ZERO_INT);
        categoryOptions.setSelectedIndex(0);
    }

    @Override
    public void submitForm(java.awt.event.ActionListener event) {
        doButton.addActionListener(event);
    }

    @Override
    public void setEditMode() {
        doButton.setText(StringResources.EDIT_TEXT);
        this.setTitle(StringResources.EDIT_PRODUCT_TITLE);
        this.stockField.setEnabled(false);
    }
    
    public int getStock(){
        return Integer.parseInt(stockField.getText());
    }
    
    public Double getCost(){
        return Double.parseDouble(costField.getText());
    }
    
    public Double getPrice(){
        return Double.parseDouble(priceField.getText());
    }
    
    public void fillOptions(Object[] list){
        categoryOptions.removeAllItems();
        categoryOptions.addItem("Selecionar");
        for(Object current: list){
            categoryOptions.addItem(current);
        }
    }
    
    public void setOption(Object seteable){
        categoryOptions.setSelectedItem(seteable);
    }
    
    public Object getOption(){
        return categoryOptions.getSelectedItem();
    }

    //<editor-fold defaultstate="collapsed" desc="NetBeans Auto-Generated Code">    
    @SuppressWarnings( "unchecked" )
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        categoryLabel = new javax.swing.JLabel();
        categoryOptions = new javax.swing.JComboBox();
        nameLabel = new javax.swing.JLabel();
        descriptionLabel = new javax.swing.JLabel();
        priceLabel = new javax.swing.JLabel();
        brandLabel = new javax.swing.JLabel();
        stockLabel = new javax.swing.JLabel();
        nameField = new javax.swing.JTextField();
        descriptionField = new javax.swing.JTextField();
        brandField = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        doButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        costLabel = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        profitLabel = new javax.swing.JLabel();
        profitValueLabel = new javax.swing.JLabel();
        stockField = new javax.swing.JTextField();
        costField = new javax.swing.JTextField();
        priceField = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Añadir Producto");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setLayout(new java.awt.GridBagLayout());

        categoryLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        categoryLabel.setText("Categoría Asociada:");
        categoryLabel.setName("categoryLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(categoryLabel, gridBagConstraints);

        categoryOptions.setName("categoryOptions"); // NOI18N
        categoryOptions.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                categoryOptionsItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(categoryOptions, gridBagConstraints);

        nameLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        nameLabel.setText("Nombre del artículo:");
        nameLabel.setName("nameLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(nameLabel, gridBagConstraints);

        descriptionLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        descriptionLabel.setText("Descripción:");
        descriptionLabel.setName("descriptionLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(descriptionLabel, gridBagConstraints);

        priceLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        priceLabel.setText("Precio de Venta:");
        priceLabel.setName("priceLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(priceLabel, gridBagConstraints);

        brandLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        brandLabel.setText("Marca:");
        brandLabel.setName("brandLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(brandLabel, gridBagConstraints);

        stockLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        stockLabel.setText("Existencias:");
        stockLabel.setName("stockLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(stockLabel, gridBagConstraints);

        nameField.setName("nameField"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(nameField, gridBagConstraints);

        descriptionField.setName("descriptionField"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(descriptionField, gridBagConstraints);

        brandField.setName("brandField"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(brandField, gridBagConstraints);

        jSeparator1.setName("jSeparator1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(jSeparator1, gridBagConstraints);

        doButton.setText("Añadir");
        doButton.setEnabled(false);
        doButton.setName("doButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(doButton, gridBagConstraints);

        cancelButton.setText("Cancelar");
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(cancelButton, gridBagConstraints);

        costLabel.setText("Costo de adquisición:");
        costLabel.setName("costLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(costLabel, gridBagConstraints);

        jSeparator2.setName("jSeparator2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(jSeparator2, gridBagConstraints);

        profitLabel.setForeground(new java.awt.Color(102, 102, 102));
        profitLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        profitLabel.setText("Ganancia estimada:");
        profitLabel.setName("profitLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(profitLabel, gridBagConstraints);

        profitValueLabel.setForeground(new java.awt.Color(102, 102, 102));
        profitValueLabel.setText("$0.00");
        profitValueLabel.setName("profitValueLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(profitValueLabel, gridBagConstraints);

        stockField.setName("stockField"); // NOI18N
        stockField.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                stockFieldCaretUpdate(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(stockField, gridBagConstraints);

        costField.setName("costField"); // NOI18N
        costField.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                costFieldCaretUpdate(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(costField, gridBagConstraints);

        priceField.setName("priceField"); // NOI18N
        priceField.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                priceFieldCaretUpdate(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(priceField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(jPanel1, gridBagConstraints);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void categoryOptionsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_categoryOptionsItemStateChanged
       if(evt.getStateChange() == ItemEvent.SELECTED){
           activateDoButton();
       }
    }//GEN-LAST:event_categoryOptionsItemStateChanged

    private void stockFieldCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_stockFieldCaretUpdate
        activateDoButton();
    }//GEN-LAST:event_stockFieldCaretUpdate

    private void costFieldCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_costFieldCaretUpdate
        activateDoButton();
        updateProfitValue();
    }//GEN-LAST:event_costFieldCaretUpdate

    private void priceFieldCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_priceFieldCaretUpdate
        activateDoButton();
        updateProfitValue();
    }//GEN-LAST:event_priceFieldCaretUpdate

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTextField brandField;
    private javax.swing.JLabel brandLabel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel categoryLabel;
    public javax.swing.JComboBox categoryOptions;
    public javax.swing.JTextField costField;
    private javax.swing.JLabel costLabel;
    public javax.swing.JTextField descriptionField;
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JButton doButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    public javax.swing.JTextField nameField;
    private javax.swing.JLabel nameLabel;
    public javax.swing.JTextField priceField;
    private javax.swing.JLabel priceLabel;
    private javax.swing.JLabel profitLabel;
    public javax.swing.JLabel profitValueLabel;
    public javax.swing.JTextField stockField;
    private javax.swing.JLabel stockLabel;
    // End of variables declaration//GEN-END:variables

    //</editor-fold>
    
    private void activateDoButton(){
            doButton.setEnabled(areConditionsFulfill());
    }
    
    private boolean areConditionsFulfill(){
        return (
                    (categoryOptions.getSelectedIndex() != 0)
                    && (isDoubleParseableFrom(costField.getText()))
                    && (isDoubleParseableFrom(priceField.getText()))
                    && (isIntegerParseableFrom(stockField.getText()))
                );
    }
    
    private boolean isDoubleParseableFrom(String toParse){
        try{
            Double.parseDouble(toParse);
            return true;
        } catch(NumberFormatException ex){
            return false;
        }
    }
    
    private boolean isIntegerParseableFrom(String toParse){
        try{
            Integer.parseInt(toParse);
            return true;
        } catch(NumberFormatException ex){
            return false;
        }
    }
    
    private void updateProfitValue(){
        if( isDoubleParseableFrom(costField.getText())
            && isDoubleParseableFrom(priceField.getText())){
                profitValueLabel.setText(String.valueOf( getPrice() - getCost() ));
            }
    }
    
}

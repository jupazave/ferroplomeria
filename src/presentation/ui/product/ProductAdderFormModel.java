package presentation.ui.product;

import entities.Product;
import javax.swing.JOptionPane;
import presentation.exceptions.OperationErrorException;
import presentation.resources.StringResources;
import presentation.components.forms.FormDialog;
import presentation.control.status.StatusDialog;

public class ProductAdderFormModel extends ProductFormModel{

    public ProductAdderFormModel(FormDialog dialog) {
        super(dialog);
    }

    @Override
    public void executeOperation() throws OperationErrorException {
        Product toAdd = compose();
        if(productCoordinator.add(toAdd)){
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.ADD_PRODUCT_SUCCESS_MESSAGE, toAdd.getName()),
                    null, 
                    JOptionPane.INFORMATION_MESSAGE
            ).setVisible(true);
        } else {
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.ADD_PRODUCT_FAIL_MESSAGE, toAdd.getName()),
                    null, 
                    JOptionPane.INFORMATION_MESSAGE
            ).setVisible(true);
            throw new OperationErrorException();
        }
    }
    
}

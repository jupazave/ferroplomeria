package presentation.ui.product;

import business.coordinators.BusinessManagement;
import business.coordinators.CategoryCoordinator;
import business.coordinators.ProductCoordinator;
import business.interfaces.business.Coordinator;
import entities.Category;
import entities.Item;
import entities.Product;
import presentation.components.forms.FormDialog;
import presentation.components.forms.FormModel;

public abstract class ProductFormModel extends FormModel<Product> {

    protected ProductFormDialog castedForm;
    protected Coordinator<Product> productCoordinator;
    protected Coordinator<Category> categoryCoordinator;
    protected int productID;
    protected int productItemID;
    
    public ProductFormModel(FormDialog dialog) {
        super(dialog);
        castedForm = (ProductFormDialog) dialog;
        productCoordinator = new ProductCoordinator();
        categoryCoordinator = BusinessManagement.getManager().getCategoryCoordinator();
        castedForm.fillOptions(categoryCoordinator.getList());
        castedForm.clearForm();
    }

    @Override
    public Product compose() {
        Item productItem = new Item(
                productItemID,
                castedForm.nameField.getText(),
                castedForm.brandField.getText()
        );
        return new Product(
                productID,
                productItem,
                castedForm.descriptionField.getText(),
                castedForm.getCost(),
                castedForm.getPrice(),
                castedForm.getStock(),
                ((Category)castedForm.getOption())
        );
    }

    @Override
    public void fill(Product entity) {
        productID = entity.getId();
        productItemID = entity.getItem().getID();
        castedForm.nameField.setText(entity.getName());
        castedForm.descriptionField.setText(entity.getDescription());
        castedForm.brandField.setText(entity.getBrand());
        castedForm.costField.setText(String.valueOf(entity.getCost()));
        castedForm.priceField.setText(String.valueOf(entity.getPrice()));
        castedForm.stockField.setText(String.valueOf(entity.getStock()));
        castedForm.setOption(entity.getCategory());
    }

    @Override
    public void showDialog() {
        castedForm.fillOptions(categoryCoordinator.getList());
        if(productID > 0){
            castedForm.setOption(productCoordinator.get(productID).getCategory());
        }
        super.showDialog();
    }
    
    
    
}

package presentation.ui.product;

import presentation.components.viewers.Viewer;
import presentation.components.viewers.ViewerHandler;
import business.coordinators.ProductCoordinator;
import business.interfaces.business.Coordinator;
import entities.Product;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import presentation.resources.StringResources;
import presentation.components.forms.FormModel;
import presentation.control.DialogReference;
import presentation.control.status.StatusDialog;

public class ProductViewerHandler extends ViewerHandler {

    private Coordinator<Product> coordinator;

    public ProductViewerHandler(Viewer viewer) {
        super(viewer);
        coordinator = new ProductCoordinator();
        viewer.addButton(createAddButton(), Viewer.ADD_BUTTON);
        viewer.addButton(createRemoveButton(), Viewer.REMOVE_BUTTON);
        viewer.addButton(createEditButton(), Viewer.EDIT_BUTTON);
    }
    
    @Override
    public void handleAddActtion() {
        FormModel model = presentation.control.WindowManager.getInstance().getForm(DialogReference.PRODUCT_ADDER);
        model.showDialog();
        if( model.isSubmitSucessfull() ){
            fillTable();
        }
    }

    @Override
    public void handleEditAction() {
        int id = ( int ) viewer.getEntitiesTable().getDataFromModel(viewer.getEntitiesTable().getSelectedRow(),0);
        FormModel model = presentation.control.WindowManager.getInstance().getForm(DialogReference.PRODUCT_EDITOR);
        model.fill(coordinator.get(id));
        model.showDialog();
        if( model.isSubmitSucessfull() ){
            fillTable();
        }
    }

    @Override
    public void handleRemoveAction() {
        int id = ( int ) viewer.getEntitiesTable().getDataFromModel(viewer.getEntitiesTable().getSelectedRow(),0);
        String name = ( String ) viewer.getEntitiesTable().getDataFromModel(viewer.getEntitiesTable().getSelectedRow(),1);
        int option = JOptionPane.showConfirmDialog(
                viewer.getRootPane(),
                StringResources.getResource(StringResources.DELETE_PRODUCT_CONFIRMATION_MESSAGE, name, String.valueOf(id)),
                null,
                JOptionPane.YES_NO_OPTION
        );
        if(JOptionPane.YES_OPTION == option){
            if(coordinator.remove(id)){
                new StatusDialog(
                        viewer,
                        StringResources.getResource(StringResources.DELETE_PRODUCT_SUCCESS_MESSAGE, name)
                ).setVisible(true);
                fillTable();
            }
            else{
                new StatusDialog(
                        viewer,
                        StringResources.getResource(StringResources.DELETE_PRODUCT_FAIL_MESSAGE, name),
                        "Error",
                        JOptionPane.ERROR_MESSAGE
                ).setVisible(true);
            }
        }
    }

    @Override
    public void fillTable() {
        viewer.getEntitiesTable().clearRows();
        DefaultTableModel tableModel = (DefaultTableModel) viewer.getEntitiesTable().getModel();
        Product[] products = coordinator.getList();
        for(Product current:products){
            tableModel.addRow(new Object[]{
                current.getId(),
                current.getName(),
                current.getDescription(),
                current.getBrand(),
                current.getCategory().getName(),
                current.getCost(),
                current.getPrice(),
            });
        }
    }
    
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.ui.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import presentation.control.WindowManager;
import presentation.control.WindowReference;

/**
 *
 * @author luisburgos
 */
public class MainWindowHandler implements ActionListener{

    private MainWindow mainWindow;
    
    public MainWindowHandler(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        this.mainWindow.getReportsButton().addActionListener(this);
        mainWindow.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == mainWindow.getReportsButton()){            
            WindowManager.getInstance().getWindow(WindowReference.REPORTS).setVisible(true);
        }
    }
    
}

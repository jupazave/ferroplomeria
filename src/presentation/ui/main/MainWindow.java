
package presentation.ui.main;

import javax.swing.JButton;
import javax.swing.JFrame;
import presentation.control.LooksLoader;
import presentation.control.WindowManager;
import presentation.control.WindowReference;

public class MainWindow extends JFrame {

    private MainWindowHandler windowHandler;

    public MainWindow() {
        initComponents();
        windowHandler = new MainWindowHandler(this);
    }

    //<editor-fold defaultstate="collapsed" desc="NetBeans Auto-Generated Code">
    @SuppressWarnings( "unchecked" )
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        desktopPanel = new javax.swing.JPanel();
        mainDesktopPanel = new javax.swing.JPanel();
        mainActionsPanel = new javax.swing.JPanel();
        registerTicketSaleButton = new javax.swing.JButton();
        generateOrderButton = new javax.swing.JButton();
        generateReportButton = new javax.swing.JButton();
        jToggleButton1 = new javax.swing.JToggleButton();
        jPanel1 = new javax.swing.JPanel();
        registerOrderButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        mainWindowMenuBar = new javax.swing.JMenuBar();
        manageMenu = new javax.swing.JMenu();
        categoriesMenuItem = new javax.swing.JMenuItem();
        invetoryMenuItem = new javax.swing.JMenu();
        invetoryProductsMenuItem = new javax.swing.JMenuItem();
        invetoryStockMenuItem = new javax.swing.JMenuItem();
        suppliersMenuItem = new javax.swing.JMenuItem();
        demandsMenuItem = new javax.swing.JMenuItem();
        toolsMenu = new javax.swing.JMenu();
        reportsMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Ferroplomería Díaz Aguilar");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        desktopPanel.setName("desktopPanel"); // NOI18N
        desktopPanel.setLayout(new java.awt.GridBagLayout());

        mainDesktopPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        mainDesktopPanel.setName("mainDesktopPanel"); // NOI18N
        mainDesktopPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        desktopPanel.add(mainDesktopPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(desktopPanel, gridBagConstraints);

        mainActionsPanel.setName("mainActionsPanel"); // NOI18N
        mainActionsPanel.setLayout(new java.awt.GridBagLayout());

        registerTicketSaleButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentation/resources/icon/cash_register.png"))); // NOI18N
        registerTicketSaleButton.setText("Registrar Venta");
        registerTicketSaleButton.setIconTextGap(5);
        registerTicketSaleButton.setName("registerTicketSaleButton"); // NOI18N
        registerTicketSaleButton.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        registerTicketSaleButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        registerTicketSaleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerTicketSaleButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 5, 0);
        mainActionsPanel.add(registerTicketSaleButton, gridBagConstraints);

        generateOrderButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentation/resources/icon/task_list.png"))); // NOI18N
        generateOrderButton.setText("Generar Cotización");
        generateOrderButton.setIconTextGap(5);
        generateOrderButton.setName("generateOrderButton"); // NOI18N
        generateOrderButton.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        generateOrderButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        generateOrderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateOrderButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 5, 0);
        mainActionsPanel.add(generateOrderButton, gridBagConstraints);

        generateReportButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentation/resources/icon/histogram-linear.png"))); // NOI18N
        generateReportButton.setText("Generar Reporte");
        generateReportButton.setIconTextGap(5);
        generateReportButton.setName("generateReportButton"); // NOI18N
        generateReportButton.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        generateReportButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        generateReportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateReportButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 5, 0);
        mainActionsPanel.add(generateReportButton, gridBagConstraints);

        jToggleButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentation/resources/icon/shopping_wallet.png"))); // NOI18N
        jToggleButton1.setText("Pedidos");
        jToggleButton1.setIconTextGap(5);
        jToggleButton1.setName("jToggleButton1"); // NOI18N
        jToggleButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 5, 0);
        mainActionsPanel.add(jToggleButton1, gridBagConstraints);

        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setLayout(new java.awt.GridBagLayout());

        registerOrderButton.setText("Levantar Pedido");
        registerOrderButton.setHideActionText(true);
        registerOrderButton.setIconTextGap(5);
        registerOrderButton.setName("registerOrderButton"); // NOI18N
        registerOrderButton.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        registerOrderButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 5, 0);
        jPanel1.add(registerOrderButton, gridBagConstraints);

        jButton1.setText("Surtir Pedido");
        jButton1.setName("jButton1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 5, 0);
        jPanel1.add(jButton1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        mainActionsPanel.add(jPanel1, gridBagConstraints);
        jPanel1.setVisible(false);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        getContentPane().add(mainActionsPanel, gridBagConstraints);

        mainWindowMenuBar.setName("mainWindowMenuBar"); // NOI18N

        manageMenu.setText("Administrar");
        manageMenu.setName("manageMenu"); // NOI18N

        categoriesMenuItem.setText("Categorias");
        categoriesMenuItem.setName("categoriesMenuItem"); // NOI18N
        categoriesMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                categoriesMenuItemActionPerformed(evt);
            }
        });
        manageMenu.add(categoriesMenuItem);

        invetoryMenuItem.setText("Inventario");
        invetoryMenuItem.setName("invetoryMenuItem"); // NOI18N

        invetoryProductsMenuItem.setText("Productos");
        invetoryProductsMenuItem.setName("invetoryProductsMenuItem"); // NOI18N
        invetoryProductsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invetoryProductsMenuItemActionPerformed(evt);
            }
        });
        invetoryMenuItem.add(invetoryProductsMenuItem);

        invetoryStockMenuItem.setText("Existencias");
        invetoryStockMenuItem.setName("invetoryStockMenuItem"); // NOI18N
        invetoryStockMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invetoryStockMenuItemActionPerformed(evt);
            }
        });
        invetoryMenuItem.add(invetoryStockMenuItem);

        manageMenu.add(invetoryMenuItem);

        suppliersMenuItem.setText("Proveedores");
        suppliersMenuItem.setName("suppliersMenuItem"); // NOI18N
        suppliersMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                suppliersMenuItemActionPerformed(evt);
            }
        });
        manageMenu.add(suppliersMenuItem);

        demandsMenuItem.setText("Faltantes");
        demandsMenuItem.setName("demandsMenuItem"); // NOI18N
        demandsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                demandsMenuItemActionPerformed(evt);
            }
        });
        manageMenu.add(demandsMenuItem);

        mainWindowMenuBar.add(manageMenu);

        toolsMenu.setText("Herramientas");
        toolsMenu.setName("toolsMenu"); // NOI18N

        reportsMenuItem.setText("Reportes");
        reportsMenuItem.setName("reportsMenuItem"); // NOI18N
        reportsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportsMenuItemActionPerformed(evt);
            }
        });
        toolsMenu.add(reportsMenuItem);

        mainWindowMenuBar.add(toolsMenu);

        setJMenuBar(mainWindowMenuBar);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void categoriesMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_categoriesMenuItemActionPerformed
        WindowManager.getInstance().getWindow(WindowReference.CATEGORY_VIEWER).setVisible(true);
    }//GEN-LAST:event_categoriesMenuItemActionPerformed

    private void suppliersMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_suppliersMenuItemActionPerformed
        WindowManager.getInstance().getWindow(WindowReference.SUPPLIER_VIEWER).setVisible(true);
    }//GEN-LAST:event_suppliersMenuItemActionPerformed

    private void invetoryProductsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invetoryProductsMenuItemActionPerformed
        WindowManager.getInstance().getWindow(WindowReference.INVENTORY_VIEWER).setVisible(true);
    }//GEN-LAST:event_invetoryProductsMenuItemActionPerformed

    private void invetoryStockMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invetoryStockMenuItemActionPerformed
        WindowManager.getInstance().getWindow(WindowReference.STOCK_VIEWER).setVisible(true);
    }//GEN-LAST:event_invetoryStockMenuItemActionPerformed

    private void demandsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_demandsMenuItemActionPerformed
        WindowManager.getInstance().getWindow(WindowReference.DEMAND_VIEWER).setVisible(true);
    }//GEN-LAST:event_demandsMenuItemActionPerformed

    private void registerTicketSaleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerTicketSaleButtonActionPerformed
        WindowManager.getInstance().getWindow(WindowReference.SALES_VIEWER).setVisible(true);
    }//GEN-LAST:event_registerTicketSaleButtonActionPerformed

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
        WindowManager.getInstance().getWindow(WindowReference.ORDER_VIEWER).setVisible(true);
    }//GEN-LAST:event_jToggleButton1ActionPerformed

    private void generateOrderButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateOrderButtonActionPerformed
        WindowManager.getInstance().getWindow(WindowReference.COST_ESTIMATOR).setVisible(true);
    }//GEN-LAST:event_generateOrderButtonActionPerformed

    private void reportsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportsMenuItemActionPerformed
        WindowManager.getInstance().getWindow(WindowReference.REPORTS).setVisible(true);
    }//GEN-LAST:event_reportsMenuItemActionPerformed

    private void generateReportButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateReportButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_generateReportButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem categoriesMenuItem;
    private javax.swing.JMenuItem demandsMenuItem;
    private javax.swing.JPanel desktopPanel;
    private javax.swing.JButton generateOrderButton;
    private javax.swing.JButton generateReportButton;
    private javax.swing.JMenu invetoryMenuItem;
    private javax.swing.JMenuItem invetoryProductsMenuItem;
    private javax.swing.JMenuItem invetoryStockMenuItem;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JPanel mainActionsPanel;
    private javax.swing.JPanel mainDesktopPanel;
    private javax.swing.JMenuBar mainWindowMenuBar;
    private javax.swing.JMenu manageMenu;
    private javax.swing.JButton registerOrderButton;
    private javax.swing.JButton registerTicketSaleButton;
    private javax.swing.JMenuItem reportsMenuItem;
    private javax.swing.JMenuItem suppliersMenuItem;
    private javax.swing.JMenu toolsMenu;
    // End of variables declaration//GEN-END:variables
    //</editor-fold>

    public JButton getReportsButton() {
        return generateReportButton;
    }

}

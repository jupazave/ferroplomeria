
package presentation.ui.valuation;

import business.coordinators.BusinessManagement;
import presentation.ui.sales.*;
import business.coordinators.TicketSaleCoordinator;
import entities.Product;
import entities.TicketSale;
import entities.TicketSaleItem;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import presentation.components.tables.TableModel;
import presentation.control.DialogReference;
import presentation.control.WindowManager;

public class SaleEstimation extends JFrame {
    private TicketSale newSale;

    public SaleEstimation() {
        initComponents();
        newSale = new TicketSale();
    }

    //<editor-fold defaultstate="collapsed" desc="NetBeans Generated Code">
    @SuppressWarnings( "unchecked" )
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        mainPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        String[] tableHeader = new String[]{ "Nombre", "Cantidad", "Precio" };
        boolean[] tablevisibility = { true, true, true };
        productsTable = new presentation.components.tables.Table();
        salePanel = new javax.swing.JPanel();
        totalLabel = new javax.swing.JLabel();
        totalAmountLabel = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        finishButton = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();

        setTitle("Nueva Venta");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setLayout(new java.awt.GridBagLayout());

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        productsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        productsTable.setName("productsTable"); // NOI18N
        jScrollPane1.setViewportView(productsTable);
        productsTable.loadDefaultSettings();
        productsTable.setHeader(tableHeader, tablevisibility);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 0);
        mainPanel.add(jScrollPane1, gridBagConstraints);

        salePanel.setName("salePanel"); // NOI18N
        salePanel.setLayout(new java.awt.GridBagLayout());

        totalLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        totalLabel.setText("Total:");
        totalLabel.setName("totalLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        salePanel.add(totalLabel, gridBagConstraints);

        totalAmountLabel.setText("$0.00");
        totalAmountLabel.setName("totalAmountLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(10, 2, 0, 10);
        salePanel.add(totalAmountLabel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        mainPanel.add(salePanel, gridBagConstraints);

        jButton1.setText("+");
        jButton1.setName("jButton1"); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        mainPanel.add(jButton1, gridBagConstraints);

        jButton2.setText("-");
        jButton2.setName("jButton2"); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        mainPanel.add(jButton2, gridBagConstraints);

        finishButton.setText("Salir");
        finishButton.setName("finishButton"); // NOI18N
        finishButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finishButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        mainPanel.add(finishButton, gridBagConstraints);

        jButton3.setText("Reiniciar Cotización");
        jButton3.setName("jButton3"); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        mainPanel.add(jButton3, gridBagConstraints);

        jSeparator1.setName("jSeparator1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        mainPanel.add(jSeparator1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(mainPanel, gridBagConstraints);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        appendItemToPurchase();
        updateTotalPayment();
    }//GEN-LAST:event_jButton1ActionPerformed


    private void finishButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finishButtonActionPerformed
        clear();
        dispose();
    }//GEN-LAST:event_finishButtonActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        removeItemFromPurchase();
        updateTotalPayment();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        clear();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void finishSale() {
        boolean thereIsSOmethingToPurchase = newSale.getItems().size() > 0;
        if ( thereIsSOmethingToPurchase ) {
            BusinessManagement.getManager().getTicketSaleCoordinator().add(newSale);
            clear();
        }
    }

    private void appendItemToPurchase() {
        EstimationFormDialog saleAdder = ( EstimationFormDialog ) WindowManager.getInstance().getForm(DialogReference.ESTIMATION_ADDER).getDialog();
        saleAdder.setModal(true);
        saleAdder.setVisible(true);
        TicketSaleItem ticketSaleItem = new TicketSaleItem();
        Product selectedProduct;
        selectedProduct = saleAdder.getSelectedProduct();
        if ( selectedProduct != null ) {
            ticketSaleItem.setProduct(selectedProduct);
            ticketSaleItem.setQuantity(saleAdder.getQuantity());
            ticketSaleItem.setUnitPrice(selectedProduct.getPrice());
            newSale.addItem(ticketSaleItem);
            ( ( DefaultTableModel ) productsTable.getModel() ).addRow(new String[]{ ticketSaleItem.getProduct().getName(), String.valueOf(ticketSaleItem.getQuantity()), String.valueOf(ticketSaleItem.getProduct().getPrice()) });
        }
    }

    private void removeItemFromPurchase() {
        for ( TicketSaleItem ticketSaleItem : newSale.getItems() ) {
            if ( productsTable.getSelectedRow() > 0 ) {
                if ( String.valueOf(productsTable.getModel().getValueAt(productsTable.getSelectedRow(), 0)).equalsIgnoreCase(ticketSaleItem.getProduct().getName()) ) {
                    newSale.removeItem(ticketSaleItem);
                    ( ( TableModel ) productsTable.getModel() ).removeRow(productsTable.getSelectedRow());
                }
            }
        }
    }

    private void updateTotalPayment() {
        Double amount = 0.0;
        for ( TicketSaleItem ticketSaleItem : newSale.getItems() ) {
            amount += ( ticketSaleItem.getQuantity() ) * ( ticketSaleItem.getProduct().getPrice() );
        }
        totalAmountLabel.setText(String.valueOf(amount));
    }

    private void clear() {
        newSale = new TicketSale();
        while ( productsTable.getRowCount() > 0 ) {
            ( ( TableModel ) productsTable.getModel() ).removeRow(0);
        }
        updateTotalPayment();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton finishButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPanel mainPanel;
    private presentation.components.tables.Table productsTable;
    private javax.swing.JPanel salePanel;
    private javax.swing.JLabel totalAmountLabel;
    private javax.swing.JLabel totalLabel;
    // End of variables declaration//GEN-END:variables
    //</editor-fold>

}

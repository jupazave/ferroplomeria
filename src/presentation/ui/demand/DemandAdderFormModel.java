package presentation.ui.demand;

import entities.Item;
import javax.swing.JOptionPane;
import presentation.exceptions.OperationErrorException;
import presentation.resources.StringResources;
import presentation.components.forms.FormDialog;
import presentation.control.status.StatusDialog;

public class DemandAdderFormModel extends DemandFormModel {
    
    public DemandAdderFormModel(FormDialog dialog) {
        super(dialog);
    }

    @Override
    public void executeOperation() throws OperationErrorException {
        Item toAdd = compose();
        if(itemCoordinator.add(toAdd)){
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.ADD_DEMAND_SUCCESS_MESSAGE, toAdd.getName()),
                    null, JOptionPane.INFORMATION_MESSAGE
            ).setVisible(true);
        }else {
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.ADD_DEMAND_FAIL_MESSAGE, toAdd.getName()),
                    null, JOptionPane.ERROR_MESSAGE
            ).setVisible(true);
            throw new OperationErrorException();
        }
    }
    
}

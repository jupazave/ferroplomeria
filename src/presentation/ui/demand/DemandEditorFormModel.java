package presentation.ui.demand;

import entities.Item;
import javax.swing.JOptionPane;
import presentation.exceptions.OperationErrorException;
import presentation.resources.StringResources;
import presentation.components.forms.FormDialog;
import presentation.control.status.StatusDialog;

public class DemandEditorFormModel extends DemandFormModel{

    public DemandEditorFormModel(FormDialog dialog) {
        super(dialog);
        castedForm.setEditMode();
    }

    @Override
    public void executeOperation() throws OperationErrorException {
        Item toEdit = compose();
        if(itemCoordinator.edit(toEdit)){
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.EDIT_DEMAND_SUCCESS_MESSAGE, toEdit.getName()),
                    null, 
                    JOptionPane.INFORMATION_MESSAGE
            ).setVisible(true);
        } else {
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.EDIT_DEMAND_FAIL_MESSAGE, toEdit.getName()),
                    null, 
                    JOptionPane.ERROR_MESSAGE
            ).setVisible(true);
            throw new OperationErrorException();
        }
    }
    
}

package presentation.ui.demand;

import business.coordinators.BusinessManagement;
import business.coordinators.ItemCoordinator;
import business.interfaces.business.Coordinator;
import entities.Item;
import javax.swing.table.DefaultTableModel;
import presentation.components.viewers.Viewer;
import presentation.components.viewers.ViewerHandler;
import presentation.components.forms.FormModel;
import presentation.control.DialogReference;

public class DemandViewerHandler extends ViewerHandler {
   
    private final Coordinator<Item> coordinator;

    public DemandViewerHandler(Viewer viewer) {
        super(viewer);
        coordinator = BusinessManagement.getManager().getItemCoordinator();
        viewer.addButton(createAddButton(), Viewer.ADD_BUTTON);
        viewer.addButton(createEditButton(), Viewer.EDIT_BUTTON);
    }
    
     @Override
    public void handleAddActtion() {
        FormModel model = presentation.control.WindowManager.getInstance().getForm(DialogReference.DEMAND_ADDER);
        model.showDialog();
        if ( model.isSubmitSucessfull() ) {
            fillTable();
        }
    }

    @Override
    public void handleEditAction() {
        int id = ( int ) viewer.getEntitiesTable().getDataFromModel(viewer.getEntitiesTable().getSelectedRow(),0);
        FormModel model = presentation.control.WindowManager.getInstance().getForm(DialogReference.DEMAND_EDITOR);
        model.fill(coordinator.get(id));
        model.showDialog();
        if ( model.isSubmitSucessfull()) {
            fillTable();
        }
    }
    
    @Override
    public void fillTable() {
        viewer.getEntitiesTable().clearRows();
        DefaultTableModel tableModel = ( DefaultTableModel ) viewer.getEntitiesTable().getModel();
        Item[] items = ((ItemCoordinator)coordinator).getDemands();
        for ( Item current : items ) {
            tableModel.addRow(new Object[]{
                current.getID(),
                current.getName(),
                current.getBrand(),
            });
        }
    }
    
}

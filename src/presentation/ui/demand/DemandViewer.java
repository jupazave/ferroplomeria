package presentation.ui.demand;

import presentation.components.viewers.Viewer;
import presentation.resources.StringResources;

public class DemandViewer extends Viewer {
    
    private final String[] tabHeaderContent = new String[]{
        "ID", "Nombre", "Marca"           
    };
    
    private final boolean[] columsVisibility = new boolean[]{
        false
    };
    
    public DemandViewer(){
        super(StringResources.DEMAND_VIEWER_TITLE);
        handler = new DemandViewerHandler(this);
        setTableHeader(tabHeaderContent,columsVisibility);
        updateEntitiesTable();
    }         
}

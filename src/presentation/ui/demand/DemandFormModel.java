package presentation.ui.demand;

import business.coordinators.BusinessManagement;
import business.coordinators.ItemCoordinator;
import business.interfaces.business.Coordinator;
import entities.Item;
import presentation.components.forms.FormDialog;
import presentation.components.forms.FormModel;

public abstract class DemandFormModel extends FormModel<Item> {

    protected DemandFormDialog castedForm;
    protected Coordinator<Item> itemCoordinator;
    protected int itemID;

    public DemandFormModel(FormDialog dialog) {
        super(dialog);
        castedForm = (DemandFormDialog) dialog;
        itemCoordinator =  BusinessManagement.getManager().getItemCoordinator();
        castedForm.clearForm();
    }

    @Override
    public Item compose() {
        return new Item(
                itemID,
                castedForm.nameField.getText(),
                castedForm.brandField.getText()
        );
    }

    @Override
    public void fill(Item entity) {
        itemID = entity.getID();
        castedForm.nameField.setText(entity.getName());
        castedForm.brandField.setText(entity.getBrand());
    }

}

package presentation.ui.stock;

import presentation.components.viewers.Viewer;
import presentation.components.viewers.ViewerHandler;
import business.coordinators.ProductCoordinator;
import business.interfaces.business.Coordinator;
import entities.Product;
import javax.swing.table.DefaultTableModel;
import presentation.components.forms.FormModel;
import presentation.control.DialogReference;

public class StockViewerHandler extends ViewerHandler {

    private Coordinator<Product> coordinator;

    public StockViewerHandler(Viewer viewer) {
        super(viewer);
        coordinator = new ProductCoordinator();
        viewer.addButton(createEditButton(), Viewer.EDIT_BUTTON);
    }

    @Override
    public void handleEditAction() {
        int id = ( int ) viewer.getEntitiesTable().getDataFromModel(viewer.getEntitiesTable().getSelectedRow(),0);
        FormModel model = presentation.control.WindowManager.getInstance().getForm(DialogReference.STOCK_EDITOR);
        model.fill(coordinator.get(id));
        model.showDialog();
        if( model.isSubmitSucessfull() ){
            fillTable();
        }
    }

    @Override
    public void fillTable() {
        viewer.getEntitiesTable().clearRows();
        DefaultTableModel tableModel = (DefaultTableModel) viewer.getEntitiesTable().getModel();
        Product[] products = coordinator.getList();
        for(Product current:products){
            tableModel.addRow(new Object[]{
                current.getId(),
                current.getName(),
                current.getBrand(),
                current.getCategory().getName(),
                current.getStock()
            });
        }
    }
    
}
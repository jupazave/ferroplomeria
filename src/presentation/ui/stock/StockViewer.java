package presentation.ui.stock;

import presentation.components.viewers.Viewer;
import presentation.resources.StringResources;

public class StockViewer extends Viewer {
    
    private final String[] tabHeaderContent = new String[]{
        "ID", "Nombre", "Marca", "Categoría", "Existencias"
    };
    
    private final boolean[] columsVisibility = new boolean[]{
        false
    };
    
    public StockViewer() {
       super(StringResources.STOCK_VIEWER_TITLE);
       handler = new StockViewerHandler(this);
       setTableHeader(tabHeaderContent,columsVisibility);
       updateEntitiesTable();
    }        
}

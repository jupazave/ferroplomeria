package presentation.ui.stock;

import java.awt.event.ItemEvent;
import javax.swing.JFrame;
import presentation.components.forms.EditionForm;
import presentation.resources.StringResources;
import presentation.components.forms.FormDialog;

public class StockFormDialog extends FormDialog implements EditionForm {

    public StockFormDialog(JFrame window, boolean modal) {
        super(window,modal);
        initComponents();
    }
    
    @Override
    public void clearForm(){}

    @Override
    public void submitForm(java.awt.event.ActionListener event) {
        doButton.addActionListener(event);
    }

    @Override
    public void setEditMode() {
        doButton.setText(StringResources.EDIT_TEXT);
        this.setTitle(StringResources.EDIT_STOCK_TITLE);
        this.brandField.setEnabled(false);
        this.categoryOptions.setEnabled(false);
        this.nameField.setEnabled(false);
    }
    
    public int getStock(){
        return Integer.parseInt(stockField.getText());
    }
    
    public void fillOptions(Object[] list){
        categoryOptions.removeAllItems();
        for(Object current: list){
            categoryOptions.addItem(current);
        }
    }
    
    public void setOption(Object seteable){
        categoryOptions.setSelectedItem(seteable);
    }
    
    public Object getOption(){
        return categoryOptions.getSelectedItem();
    }

    //<editor-fold defaultstate="collapsed" desc="NetBeans Auto-Generated Code">    
    @SuppressWarnings( "unchecked" )
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        categoryLabel = new javax.swing.JLabel();
        categoryOptions = new javax.swing.JComboBox();
        nameLabel = new javax.swing.JLabel();
        brandLabel = new javax.swing.JLabel();
        stockLabel = new javax.swing.JLabel();
        nameField = new javax.swing.JTextField();
        brandField = new javax.swing.JTextField();
        doButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        stockField = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Añadir Producto");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setLayout(new java.awt.GridBagLayout());

        categoryLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        categoryLabel.setText("Categoría Asociada:");
        categoryLabel.setName("categoryLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(categoryLabel, gridBagConstraints);

        categoryOptions.setName("categoryOptions"); // NOI18N
        categoryOptions.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                categoryOptionsItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(categoryOptions, gridBagConstraints);

        nameLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        nameLabel.setText("Nombre del artículo:");
        nameLabel.setName("nameLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(nameLabel, gridBagConstraints);

        brandLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        brandLabel.setText("Marca:");
        brandLabel.setName("brandLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(brandLabel, gridBagConstraints);

        stockLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        stockLabel.setText("Existencias:");
        stockLabel.setName("stockLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(stockLabel, gridBagConstraints);

        nameField.setName("nameField"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(nameField, gridBagConstraints);

        brandField.setName("brandField"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(brandField, gridBagConstraints);

        doButton.setText("Añadir");
        doButton.setEnabled(false);
        doButton.setName("doButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(doButton, gridBagConstraints);

        cancelButton.setText("Cancelar");
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(cancelButton, gridBagConstraints);

        jSeparator2.setName("jSeparator2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(jSeparator2, gridBagConstraints);

        stockField.setName("stockField"); // NOI18N
        stockField.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                stockFieldCaretUpdate(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(stockField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(jPanel1, gridBagConstraints);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void categoryOptionsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_categoryOptionsItemStateChanged
       if(evt.getStateChange() == ItemEvent.SELECTED){
           activateDoButton();
       }
    }//GEN-LAST:event_categoryOptionsItemStateChanged

    private void stockFieldCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_stockFieldCaretUpdate
        activateDoButton();
    }//GEN-LAST:event_stockFieldCaretUpdate

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTextField brandField;
    private javax.swing.JLabel brandLabel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel categoryLabel;
    public javax.swing.JComboBox categoryOptions;
    private javax.swing.JButton doButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator2;
    public javax.swing.JTextField nameField;
    private javax.swing.JLabel nameLabel;
    public javax.swing.JTextField stockField;
    private javax.swing.JLabel stockLabel;
    // End of variables declaration//GEN-END:variables

    //</editor-fold>
    
    private void activateDoButton(){
            doButton.setEnabled(areConditionsDone());
    }
    
    private boolean areConditionsDone(){
        return (
                    (categoryOptions.getSelectedIndex() != -1)
                    && (isIntegerParseableFrom(stockField.getText()))
                );
    }
    
    private boolean isIntegerParseableFrom(String toParse){
        try{
            Integer.parseInt(toParse);
            return true;
        } catch(NumberFormatException ex){
            return false;
        }
    }
    
}

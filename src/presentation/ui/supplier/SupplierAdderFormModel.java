package presentation.ui.supplier;

import entities.Supplier;
import javax.swing.JOptionPane;
import presentation.exceptions.OperationErrorException;
import presentation.resources.StringResources;
import presentation.components.forms.FormDialog;
import presentation.control.status.StatusDialog;

public class SupplierAdderFormModel extends SupplierFormModel{

    public SupplierAdderFormModel(FormDialog dialog) {
        super(dialog);
    }

    @Override
    public void executeOperation() throws OperationErrorException{
        Supplier toAdd = compose();
        if(supplierCoordinator.add(toAdd)){
            new StatusDialog(castedForm,
                    StringResources.getResource(StringResources.ADD_SUPPLIER_SUCCESS_MESSAGE, toAdd.getName()),
                    null, JOptionPane.INFORMATION_MESSAGE).setVisible(true);
        }
        else {
            new StatusDialog(castedForm,
                    StringResources.getResource(StringResources.ADD_SUPPLIER_FAIL_MESSAGE, toAdd.getName()),
                    null, JOptionPane.INFORMATION_MESSAGE).setVisible(true);
            throw new OperationErrorException();
        }
    }
    
}

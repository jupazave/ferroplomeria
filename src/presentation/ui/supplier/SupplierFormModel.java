package presentation.ui.supplier;

import business.coordinators.BusinessManagement;
import business.coordinators.SupplierCoordinator;
import business.interfaces.business.Coordinator;
import entities.Supplier;
import presentation.components.forms.FormDialog;
import presentation.components.forms.FormModel;

public abstract class SupplierFormModel extends FormModel<Supplier> {
    
    protected SupplierFormDialog castedForm;
    protected Coordinator<Supplier> supplierCoordinator;
    protected int supplierID;

    public SupplierFormModel(FormDialog dialog) {
        super(dialog);
        castedForm = (SupplierFormDialog) dialog;
        supplierCoordinator = BusinessManagement.getManager().getSupplierCoordinator();
        castedForm.clearForm();
    }

    @Override
    public Supplier compose() {
        return new Supplier(
                supplierID,
                castedForm.nameField.getText(),
                castedForm.phoneField.getText(),
                castedForm.mailField.getText(),
                castedForm.addressField.getText()
        );
    }

    @Override
    public void fill(Supplier entity) {
        supplierID = entity.getId();
        castedForm.nameField.setText(entity.getName());
        castedForm.phoneField.setText(entity.getPhone());
        castedForm.mailField.setText(entity.getEmail());
        castedForm.addressField.setText(entity.getAddress());
    }
    
}

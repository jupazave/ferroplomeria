
package presentation.ui.supplier;

import business.coordinators.BusinessManagement;
import presentation.components.viewers.Viewer;
import presentation.components.viewers.ViewerHandler;
import business.coordinators.SupplierCoordinator;
import business.interfaces.business.Coordinator;
import entities.Supplier;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import presentation.resources.StringResources;
import presentation.components.forms.FormModel;
import presentation.control.DialogReference;
import presentation.control.status.StatusDialog;

public class SupplierViewerHandler extends ViewerHandler {

    private Coordinator<Supplier> coordinator;

    public SupplierViewerHandler(Viewer viewer) {
        super(viewer);
        coordinator = BusinessManagement.getManager().getSupplierCoordinator();
        viewer.addButton(createAddButton(), Viewer.ADD_BUTTON);
        viewer.addButton(createRemoveButton(), Viewer.REMOVE_BUTTON);
        viewer.addButton(createEditButton(), Viewer.EDIT_BUTTON);
    }

    @Override
    public void handleAddActtion() {
        FormModel model = presentation.control.WindowManager.getInstance().getForm(DialogReference.SUPPLIER_ADDER);
        model.showDialog();
        if ( model.isSubmitSucessfull() ) {
            fillTable();
        }
    }

    @Override
    public void handleEditAction() {
        int id = ( int ) viewer.getEntitiesTable().getDataFromModel(viewer.getEntitiesTable().getSelectedRow(), 0);
        FormModel model = presentation.control.WindowManager.getInstance().getForm(DialogReference.SUPPLIER_EDITOR);
        model.fill(coordinator.get(id));
        model.showDialog();
        if ( model.isSubmitSucessfull() ) {
            fillTable();
        }
    }

    @Override
    public void handleRemoveAction() {
        int id = ( int ) viewer.getEntitiesTable().getDataFromModel(viewer.getEntitiesTable().getSelectedRow(), 0);
        String name = ( String ) viewer.getEntitiesTable().getDataFromModel(viewer.getEntitiesTable().getSelectedRow(), 1);
        int option = JOptionPane.showConfirmDialog(
                viewer.getRootPane(),
                StringResources.getResource(StringResources.DELETE_SUPPLIER_CONFIRMATION_MESSAGE, name, String.valueOf(id)),
                null,
                JOptionPane.YES_NO_OPTION
        );
        if ( JOptionPane.YES_OPTION == option ) {
            if ( coordinator.remove(id) ) {
                new StatusDialog(
                        viewer,
                        StringResources.getResource(StringResources.DELETE_SUPPLIER_SUCCESS_MESSAGE, name)
                ).setVisible(true);
                fillTable();
            }
            else {
                new StatusDialog(
                        viewer,
                        StringResources.getResource(StringResources.DELETE_SUPPLIER_FAIL_MESSAGE, name)
                ).setVisible(true);
            }
        }
    }

    @Override
    public void fillTable() {
        viewer.getEntitiesTable().clearRows();
        DefaultTableModel tableModel = ( DefaultTableModel ) viewer.getEntitiesTable().getModel();
        Supplier[] suppliers = coordinator.getList();
        for ( Supplier current : suppliers ) {
            tableModel.addRow(new Object[]{
                current.getId(),
                current.getName(),
                current.getPhone(),
                current.getEmail(),
                current.getAddress(), });
        }
    }

}

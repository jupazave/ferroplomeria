package presentation.ui.supplier;

import presentation.components.viewers.Viewer;
import presentation.resources.StringResources;

public class SupplierViewer extends Viewer {
    
    private final String[] tabHeaderContent = new String[]{
        "ID", "Nombre", "Telefono", "eMail", "Dirección"      
    };
    
    private final boolean[] columsVisibility = new boolean[]{
        false
    };
    
    public SupplierViewer() {
       super(StringResources.SUPPLIER_VIEWER_TITLE);
       handler = new SupplierViewerHandler(this);
       setTableHeader(tabHeaderContent,columsVisibility);
       updateEntitiesTable();
    }    
}

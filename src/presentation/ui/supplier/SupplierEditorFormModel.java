package presentation.ui.supplier;

import entities.Supplier;
import javax.swing.JOptionPane;
import presentation.exceptions.OperationErrorException;
import presentation.resources.StringResources;
import presentation.components.forms.FormDialog;
import presentation.control.status.StatusDialog;

public class SupplierEditorFormModel extends SupplierFormModel {
    
    public SupplierEditorFormModel(FormDialog dialog) {
        super(dialog);
        castedForm.setEditMode();
    }

    @Override
    public void executeOperation() throws OperationErrorException {
        Supplier toEdit = compose();
        if(supplierCoordinator.edit(toEdit)){
            new StatusDialog(castedForm,
                    StringResources.getResource(StringResources.EDIT_SUPPLIER_SUCCESS_MESSAGE, toEdit.getName()),
                    null, JOptionPane.INFORMATION_MESSAGE).setVisible(true);
        }
        else {
            new StatusDialog(castedForm,
                    StringResources.getResource(StringResources.EDIT_SUPPLIER_FAIL_MESSAGE, toEdit.getName()),
                    null, JOptionPane.ERROR_MESSAGE).setVisible(true);
            throw new OperationErrorException();
        }
    }
    
    
}

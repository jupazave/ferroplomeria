package presentation.ui.orders;

import business.coordinators.BusinessManagement;
import business.coordinators.OrderCoordinator;
import business.interfaces.business.Coordinator;
import entities.Order;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import presentation.components.forms.FormModel;
import presentation.components.viewers.Viewer;
import presentation.components.viewers.ViewerHandler;
import presentation.control.DialogReference;
import presentation.control.status.StatusDialog;
import presentation.resources.StringResources;

public class OrderViewerHandler extends ViewerHandler {
   
    private Coordinator<Order> coordinator;

    public OrderViewerHandler(Viewer viewer) {
        super(viewer);
        coordinator = BusinessManagement.getManager().getOrderCoordinator();
        viewer.addButton(createAddButton(), Viewer.ADD_BUTTON);
        viewer.addButton(createRemoveButton(), Viewer.REMOVE_BUTTON);
        viewer.addButton(createButton("Detalles", "details", "details.png"), Viewer.CUSTOM_BUTTON);
        
        prepareDetailsEvent();
    }
    
    @Override
    public void handleAddActtion() {
        FormModel model = presentation.control.WindowManager.getInstance().getForm(DialogReference.ORDER_ADDER);
        model.showDialog();
        if( model.isSubmitSucessfull() ){
            fillTable();
        }
    }

    @Override
    public void handleRemoveAction() {
        int orderId = ( int ) viewer.getEntitiesTable().getDataFromModel(viewer.getEntitiesTable().getSelectedRow(), 0);
        int isConfirmed = JOptionPane.showConfirmDialog(
                null, StringResources.getResource(StringResources.DELETE_ORDER_CONFIRMATION_MESSAGE, String.valueOf(orderId)),
                null, JOptionPane.YES_NO_OPTION);
        if ( JOptionPane.YES_OPTION == isConfirmed ) {
            if(coordinator.remove(orderId)){
                new StatusDialog(null, StringResources.getResource(StringResources.DELETE_ORDER_SUCCESS_MESSAGE, String.valueOf(orderId))).setVisible(true);
                    fillTable();
            } else {
                new StatusDialog(null, StringResources.getResource(StringResources.DELETE_ORDER_FAIL_MESSAGE, String.valueOf(orderId)),
                            "Error", JOptionPane.ERROR_MESSAGE).setVisible(true);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if(event.getSource() == viewer.getButton(Viewer.CUSTOM_BUTTON, "details")){
            handleDetailsAction();
        } else {
            super.actionPerformed(event);
        }
    }
    
    @Override
    public void fillTable() {
        viewer.getEntitiesTable().clearRows();
        DefaultTableModel tableModel = ( DefaultTableModel ) viewer.getEntitiesTable().getModel();
        Order[] orders = coordinator.getList();
        for ( Order current : orders ) {
            tableModel.addRow(new Object[]{
                current.getId(),
                current.getSupplier().getName(),
                ((OrderCoordinator)coordinator).getSummary(current),
            });
        }
    }
    
    private void prepareDetailsEvent(){
        disableDetailsButton();
        viewer.getEntitiesTable().addMouseListener(new java.awt.event.MouseAdapter() {
            
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                activateDetailsButton();
            }
            
        });
    }
    
    private void disableDetailsButton(){
        viewer.getButton(Viewer.CUSTOM_BUTTON, "details").setEnabled(false);
    }
    
    private void activateDetailsButton(){
        viewer.getButton(Viewer.CUSTOM_BUTTON, "details").setEnabled(isValidSelectedRow());
    }
    
    private boolean isValidSelectedRow(){
        return viewer.getEntitiesTable().getSelectedRow() >= 0;
    }

    private void handleDetailsAction() {
        int selectedRow = viewer.getEntitiesTable().getSelectedRow();
        int OrderID = (int)viewer.getEntitiesTable().getDataFromModel(selectedRow, 0);
        FormModel model = presentation.control.WindowManager.getInstance().getForm(DialogReference.ORDER_DETAILS);
        model.fill(coordinator.get(OrderID));
        model.showDialog();
    }
    
}

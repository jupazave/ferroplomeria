package presentation.ui.orders;

import presentation.components.viewers.Viewer;
import presentation.resources.StringResources;

public class OrderViewer extends Viewer {

    private final String[] tabHeaderContent = new String[]{
        "Pedido #", "Proveedor", "Productos"           
    };
    
    private final boolean[] columsVisibility = new boolean[]{
    };
    
    public OrderViewer() {
       super(StringResources.ORDER_VIEWER_TITLE);
       handler = new OrderViewerHandler(this);
       setTableHeader(tabHeaderContent,columsVisibility);
       updateEntitiesTable();
    }
    
}

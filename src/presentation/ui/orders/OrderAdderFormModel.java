package presentation.ui.orders;

import business.coordinators.OrderCoordinator;
import entities.Order;
import javax.swing.JOptionPane;
import presentation.exceptions.OperationErrorException;
import presentation.components.forms.FormDialog;
import presentation.control.status.StatusDialog;
import presentation.resources.StringResources;

public class OrderAdderFormModel extends OrderFormModel {
    
    public OrderAdderFormModel(FormDialog dialog) {
        super(dialog);
    }

    @Override
    public void executeOperation() throws OperationErrorException {
        Order toAdd = compose();
        System.out.println(((OrderCoordinator)orderCoordinator).getSummary(toAdd));
        if(orderCoordinator.add(toAdd)){
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.ADD_ORDER_SUCCESS_MESSAGE, toAdd.getSupplier().getName()),
                    null, JOptionPane.INFORMATION_MESSAGE
            ).setVisible(true);
        } else {
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.ADD_ORDER_FAIL_MESSAGE, toAdd.getSupplier().getName()),
                    null, JOptionPane.ERROR_MESSAGE
            ).setVisible(true);
            throw new OperationErrorException();
        }
    }
    
}

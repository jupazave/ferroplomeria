package presentation.ui.orders;

import presentation.components.forms.EditionForm;
import presentation.exceptions.OperationErrorException;
import presentation.components.forms.FormDialog;

public class OrderDetailsFormModel extends OrderFormModel implements EditionForm {
    
    public OrderDetailsFormModel(FormDialog dialog) {
        super(dialog);
        setEditMode();
    }

    @Override
    public void executeOperation() throws OperationErrorException {
    }

    @Override
    public void setEditMode(){
        castedForm.getCategoryOptions().setEnabled(false);
        castedForm.getItemOptions().setEnabled(false);
        castedForm.getProductOptions().setEnabled(false);
        castedForm.getSupplierOptions().setEnabled(false);
        castedForm.getDoButton().setText("Aceptar");
    }
    
}

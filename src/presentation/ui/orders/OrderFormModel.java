package presentation.ui.orders;

import business.coordinators.BusinessManagement;
import business.coordinators.CategoryCoordinator;
import business.coordinators.ItemCoordinator;
import business.coordinators.OrderCoordinator;
import business.coordinators.ProductCoordinator;
import business.coordinators.SupplierCoordinator;
import business.interfaces.business.Coordinator;
import entities.Category;
import entities.Item;
import entities.Order;
import entities.Product;
import entities.Supplier;
import presentation.components.forms.FormDialog;
import presentation.components.forms.FormModel;

public abstract class OrderFormModel extends FormModel<Order> {

    protected OrderFormDialog castedForm;
    
    protected Coordinator<Product> productCoordinator;
    protected Coordinator<Category> categoryCoordinator;
    protected Coordinator<Item> itemCoordinator;
    protected Coordinator<Supplier> supplierCoordinator;
    protected Coordinator<Order> orderCoordinator;
    
    protected Supplier supplier;
    
    public OrderFormModel(FormDialog dialog) {
        super(dialog);
        castedForm = (OrderFormDialog) dialog;
        productCoordinator = BusinessManagement.getManager().getProductCoordinator();
        categoryCoordinator = BusinessManagement.getManager().getCategoryCoordinator();
        itemCoordinator = BusinessManagement.getManager().getItemCoordinator();
        supplierCoordinator = BusinessManagement.getManager().getSupplierCoordinator();
        orderCoordinator = BusinessManagement.getManager().getOrderCoordinator();
        setViewerData();
        castedForm.clearForm();
    }

    @Override
    public Order compose() {
        Order newOrder = new Order();
        newOrder.setSupplier(castedForm.getSupplier());
        newOrder.addAllItems(castedForm.getOrderItems());
        return newOrder;
    }

    @Override
    public void fill(Order entity) {
        supplier = entity.getSupplier();
        System.out.println(entity.getSupplier());
        castedForm.setOrderContents(entity.getItems());
    }

    @Override
    public void showDialog() {
        setViewerData();
        castedForm.refreshOptionsData();
        if(supplier != null && supplier.getId() > 0){
            castedForm.setSupplierOption(supplier);
        }
        super.showDialog();
    }
    
    private void setViewerData(){
        Object[][] viewerData= new Object[][]{
            supplierCoordinator.getList(),
            categoryCoordinator.getList(),
            productCoordinator.getList(),
            ((ItemCoordinator)itemCoordinator).getDemands()
        };
        
        castedForm.setOptionsData(viewerData);
    }
    
}

package presentation.ui.orders;

import entities.Category;
import entities.Item;
import entities.OrderItem;
import entities.Product;
import entities.Supplier;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import presentation.components.forms.EditionForm;
import presentation.resources.StringResources;
import presentation.components.forms.FormDialog;
import presentation.components.tables.Table;

public class OrderFormDialog extends FormDialog implements EditionForm {
    
    private Object[][] optionsData = new Object[4][];
    private final int SUPPLIER_OPTIONS = 0;
    private final int CATEGORY_OPTIONS = 1;
    private final int PRODUCT_OPTIONS = 2;
    private final int ITEM_OPTIONS = 3;
    private final int INVALID_ROW = -1;
    
    private final String[] tabHeaderContent = new String[]{
        "ItemID", "Artículo", "Marca", "Cantidad"           
    };
    
    private final boolean[] columsVisibility = new boolean[]{
        false
    };
    
    public OrderFormDialog(JFrame window, boolean modal) {
        super(window,modal);
        initComponents();
        orderContents.loadDefaultSettings();
        orderContents.setHeader(tabHeaderContent, columsVisibility);
        pack();
        revalidate();
    }
    
    @Override
    public void clearForm() {
        clearSupplier();
        clearProductsAdder();
        clearItemsAdder();
    }

    @Override
    public void submitForm(java.awt.event.ActionListener event) {
        doButton.addActionListener(event);
    }

    @Override
    public void setEditMode() {
        doButton.setText(StringResources.EDIT_TEXT);
        setTitle(StringResources.EDIT_CATEGORY_TITLE);
    }
    
    public void setOptionsData(Object[][] data){
        System.arraycopy(data, 0, this.optionsData, 0, this.optionsData.length);
    }
    
    public void refreshOptionsData(){
        fillSupplierOptions();
        fillCategoryOptions();
        fillItemOptions();
    }
    
    public void setSupplierOption(Object option){
        supplierOptions.setSelectedItem(option);
    }
    
    public Supplier getSupplier(){
        return (Supplier) supplierOptions.getSelectedItem();
    }
    
    public ArrayList<OrderItem> getOrderItems(){
        ArrayList<OrderItem> allItems = new ArrayList<>();
        while ( orderContents.getRowCount() > 0 ) {
            OrderItem current = new OrderItem();
            
            Item associatedItem = new Item();
            associatedItem.setID((int)orderContents.getDataFromModel(0,0));
            current.setItem(associatedItem);
            
            current.setQuantity((int)orderContents.getDataFromModel(0,3));
           
            orderContents.getModel().removeRow(0);
            allItems.add(current);
        }
        return allItems;
    }
    
    public void setOrderContents(List<OrderItem> allOrderItems){
        for(OrderItem current: allOrderItems){
            addOrderItemToContents(current);
        }
    }
    
    public JComboBox getCategoryOptions(){
        return categoryOptions;
    }
    
    public JComboBox getItemOptions(){
        return itemOptions;
    }
    
    public JComboBox getProductOptions(){
        return productOptions;
    }
    
    public JComboBox getSupplierOptions(){
        return supplierOptions;
    }
    
    public JButton getDoButton(){
        return doButton;
    }
    
    public Table getOrderContentsTable(){
        return orderContents;
    }
    

    //<editor-fold defaultstate="collapsed" desc="NetBeans Auto-Generated Code">
    @SuppressWarnings( "unchecked" )
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        doButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        supplierLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        supplierOptions = new javax.swing.JComboBox();
        categoryLabel = new javax.swing.JLabel();
        addProductLabel = new javax.swing.JLabel();
        addItemLabel = new javax.swing.JLabel();
        itemLabel = new javax.swing.JLabel();
        categoryOptions = new javax.swing.JComboBox();
        productLabel = new javax.swing.JLabel();
        productOptions = new javax.swing.JComboBox();
        addProductButton = new javax.swing.JButton();
        addItemButton = new javax.swing.JButton();
        itemOptions = new javax.swing.JComboBox();
        jSeparator4 = new javax.swing.JSeparator();
        removeButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        orderContents = new presentation.components.tables.Table();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Añadir una categoría");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setLayout(new java.awt.GridBagLayout());

        doButton.setText("Añadir");
        doButton.setEnabled(false);
        doButton.setName("doButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(doButton, gridBagConstraints);

        cancelButton.setText("Cancelar");
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(cancelButton, gridBagConstraints);

        supplierLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        supplierLabel.setText("Provedores:");
        supplierLabel.setName("supplierLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(supplierLabel, gridBagConstraints);

        jSeparator1.setName("jSeparator1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jSeparator1, gridBagConstraints);

        jSeparator2.setName("jSeparator2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jSeparator2, gridBagConstraints);

        jSeparator3.setName("jSeparator3"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jSeparator3, gridBagConstraints);

        supplierOptions.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        supplierOptions.setName("supplierOptions"); // NOI18N
        supplierOptions.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                supplierOptionsItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(supplierOptions, gridBagConstraints);

        categoryLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        categoryLabel.setText("Categoría:");
        categoryLabel.setName("categoryLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(categoryLabel, gridBagConstraints);

        addProductLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        addProductLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addProductLabel.setText("Añadir Producto");
        addProductLabel.setName("addProductLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(addProductLabel, gridBagConstraints);

        addItemLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        addItemLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addItemLabel.setText("Añadir Artículo");
        addItemLabel.setName("addItemLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(addItemLabel, gridBagConstraints);

        itemLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        itemLabel.setText("Artículo:");
        itemLabel.setName("itemLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(itemLabel, gridBagConstraints);

        categoryOptions.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        categoryOptions.setName("categoryOptions"); // NOI18N
        categoryOptions.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                categoryOptionsItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(categoryOptions, gridBagConstraints);

        productLabel.setText("Producto:");
        productLabel.setName("productLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(productLabel, gridBagConstraints);

        productOptions.setName("productOptions"); // NOI18N
        productOptions.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                productOptionsItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(productOptions, gridBagConstraints);

        addProductButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentation/resources/icon/add.png"))); // NOI18N
        addProductButton.setText("Añadir Producto");
        addProductButton.setEnabled(false);
        addProductButton.setName("addProductButton"); // NOI18N
        addProductButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addProductButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(addProductButton, gridBagConstraints);

        addItemButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentation/resources/icon/add.png"))); // NOI18N
        addItemButton.setText("Añadir Artículo");
        addItemButton.setEnabled(false);
        addItemButton.setName("addItemButton"); // NOI18N
        addItemButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addItemButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(addItemButton, gridBagConstraints);

        itemOptions.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        itemOptions.setName("itemOptions"); // NOI18N
        itemOptions.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                itemOptionsItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(itemOptions, gridBagConstraints);

        jSeparator4.setName("jSeparator4"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jSeparator4, gridBagConstraints);

        removeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/presentation/resources/icon/remove.png"))); // NOI18N
        removeButton.setText("Remover");
        removeButton.setEnabled(false);
        removeButton.setName("removeButton"); // NOI18N
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(removeButton, gridBagConstraints);

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        orderContents.setName("orderContents"); // NOI18N
        orderContents.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                orderContentsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(orderContents);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jScrollPane1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(jPanel1, gridBagConstraints);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void supplierOptionsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_supplierOptionsItemStateChanged
        handleOptionEvents(evt);
    }//GEN-LAST:event_supplierOptionsItemStateChanged

    private void categoryOptionsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_categoryOptionsItemStateChanged
        if(categoryOptions.getSelectedIndex() !=0){
            handleOptionEvents(evt);
        } else {
            fillDefaultProductOptions();
        }
    }//GEN-LAST:event_categoryOptionsItemStateChanged

    private void productOptionsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_productOptionsItemStateChanged
        handleOptionEvents(evt);
    }//GEN-LAST:event_productOptionsItemStateChanged

    private void itemOptionsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_itemOptionsItemStateChanged
        handleOptionEvents(evt);
    }//GEN-LAST:event_itemOptionsItemStateChanged

    private void addProductButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addProductButtonActionPerformed
        addProduct();
    }//GEN-LAST:event_addProductButtonActionPerformed

    private void addItemButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addItemButtonActionPerformed
        addItem();
    }//GEN-LAST:event_addItemButtonActionPerformed

    private void orderContentsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_orderContentsMouseClicked
        activateRemoveButton();
    }//GEN-LAST:event_orderContentsMouseClicked

    private void removeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButtonActionPerformed
        removeItem();
    }//GEN-LAST:event_removeButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addItemButton;
    private javax.swing.JLabel addItemLabel;
    private javax.swing.JButton addProductButton;
    private javax.swing.JLabel addProductLabel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel categoryLabel;
    private javax.swing.JComboBox categoryOptions;
    private javax.swing.JButton doButton;
    private javax.swing.JLabel itemLabel;
    private javax.swing.JComboBox itemOptions;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private presentation.components.tables.Table orderContents;
    private javax.swing.JLabel productLabel;
    private javax.swing.JComboBox productOptions;
    private javax.swing.JButton removeButton;
    private javax.swing.JLabel supplierLabel;
    private javax.swing.JComboBox supplierOptions;
    // End of variables declaration//GEN-END:variables

//</editor-fold>

    private void fillSupplierOptions(){
        supplierOptions.removeAllItems();
        supplierOptions.addItem("Selecionar");
        for(Object current: optionsData[SUPPLIER_OPTIONS]){
            supplierOptions.addItem(current);
        }
    }
    
    private void fillCategoryOptions(){
        categoryOptions.removeAllItems();
        categoryOptions.addItem("Selecionar");
        for(Object current: optionsData[CATEGORY_OPTIONS]){
            categoryOptions.addItem(current);
        }
    }
    
    private void updateProdutOptionsData(int filter){
        productOptions.removeAllItems();;
        productOptions.addItem("Selecionar");
        for(Object current: optionsData[PRODUCT_OPTIONS]){
            if(isPassingIDFilter(current, filter)){
                productOptions.addItem(current);
            }
        }
    }
    
    private void fillDefaultProductOptions(){
        productOptions.removeAllItems();;
        productOptions.addItem("Selecionar");
    }
        
    private void fillItemOptions(){
        itemOptions.removeAllItems();
        itemOptions.addItem("Selecionar");
        for(Object current: optionsData[ITEM_OPTIONS]){
            itemOptions.addItem(current);
        }
    }
    
    private boolean isPassingIDFilter(Object object,int id){
        return ((Product)object).getCategory().getId() == id;
    }
    
    private void handleOptionEvents(ItemEvent evt){
        if(evt.getStateChange() == ItemEvent.SELECTED){
            if(evt.getSource() == categoryOptions || evt.getSource() == productOptions){
                if(evt.getSource() == categoryOptions){
                    int categoryID = ((Category)categoryOptions.getSelectedItem()).getId();
                    updateProdutOptionsData(categoryID);
                }
                activateAddProductButton();
            } else if(evt.getSource() == itemOptions){
                activateAddItemButton();
            } else {
                activateDoButton();
            }
        }
    }
    
    private void activateDoButton(){
        doButton.setEnabled(areDoConditionsFulfill());
    }
    
    private boolean areDoConditionsFulfill(){
        return  (
                    (supplierOptions.getSelectedIndex() != 0)
                    && (orderContents.getRowCount() > 0)
                );
    }
    
    private void activateAddProductButton(){
        addProductButton.setEnabled(areAddProductConditionsFulfill());
    }
    
    private boolean areAddProductConditionsFulfill(){
        return  (
                    (categoryOptions.getSelectedIndex() != 0)
                    && (productOptions.getSelectedIndex() != 0)
                );
    }
    
    private void activateAddItemButton(){
        addItemButton.setEnabled(areAddItemConditionsFulfill());
    }
    
    private boolean areAddItemConditionsFulfill(){
        return  (
                    (itemOptions.getSelectedIndex() != 0)
                );
    }
    
    private void activateRemoveButton(){
        removeButton.setEnabled(areRemoveConditionsFulfill());
    }
    
    private boolean areRemoveConditionsFulfill(){
        return  (
                    (orderContents.getSelectedRow() != INVALID_ROW)
                );
    }
    
    private void addProduct(){
        addProductToOrder();
        clearProductsAdder();
        activateDoButton();
    }
    
    private void addProductToOrder(){
        Product toAdd = (Product)productOptions.getSelectedItem();
        int quantity = requestQuantity();
        orderContents.getModel().addRow(new Object[]{
            toAdd.getItem().getID(),
            toAdd.getName(),
            toAdd.getBrand(),
            quantity,
        });
    }
    
    private void addItem(){
        addItemToOrder();
        clearItemsAdder();
        activateDoButton();
    }
    
    private void addItemToOrder(){
        Item toAdd = (Item)itemOptions.getSelectedItem();
        int quantity = requestQuantity();
        orderContents.getModel().addRow(new Object[]{
            toAdd.getID(),
            toAdd.getName(),
            toAdd.getBrand(),
            quantity,
        });
    }
    
    private void removeItem(){
        removeFromOrder();
        activateDoButton();
        activateRemoveButton();
    }
    
    private void removeFromOrder(){
        orderContents.getModel().removeRow(orderContents.getSelectedRow());
    }
    
    private void addOrderItemToContents(OrderItem toAdd){
        orderContents.getModel().addRow(new Object[]{
            toAdd.getID(),
            toAdd.getName(),
            toAdd.getBrand(),
            toAdd.getQuantity(),
        });
    }
    
    private void clearProductsAdder(){
        categoryOptions.setSelectedIndex(0);
    }
    
    private void clearItemsAdder(){
        itemOptions.setSelectedIndex(0);
    }
    
    private void clearSupplier(){
        supplierOptions.setSelectedIndex(0);
    }
    
    private int requestQuantity(){
        String quantity = null;
        do{
            quantity = JOptionPane.showInputDialog(StringResources.REQUEST_QUANTITY);
        }while(quantity == null || !isIntegerParseableFrom(quantity));
        return Integer.parseInt(quantity);
    }
    
    private boolean isIntegerParseableFrom(String toParse){
        try{
            Integer.parseInt(toParse);
            return true;
        } catch(NumberFormatException ex){
            return false;
        }
    }
}

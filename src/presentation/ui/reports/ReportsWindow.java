/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.ui.reports;

import com.toedter.calendar.JDateChooser;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import static presentation.resources.StringResources.REPORTS_WINDOW_TITLE;

/**
 *
 * @author luisburgos
 */
public class ReportsWindow extends JFrame{

    private final ReportsWindowHandler handler;
    
    public ReportsWindow() {        
        initComponents();
        this.setTitle(REPORTS_WINDOW_TITLE);
        handler = new ReportsWindowHandler(this);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        mainActionsPanel = new javax.swing.JPanel();
        dayReportButton = new javax.swing.JButton();
        monthReportButton = new javax.swing.JButton();
        selectDatesButton = new javax.swing.JToggleButton();
        dateSelectionPanel = new javax.swing.JPanel();
        fromDateChooser = new com.toedter.calendar.JDateChooser();
        toDateChooser = new com.toedter.calendar.JDateChooser();
        fromLabel = new javax.swing.JLabel();
        toLabel = new javax.swing.JLabel();
        reportFromDatesButton = new javax.swing.JButton();
        topSeparator = new javax.swing.JSeparator();
        bottomSeparator = new javax.swing.JSeparator();

        getContentPane().setLayout(new java.awt.GridBagLayout());

        mainActionsPanel.setLayout(new java.awt.GridBagLayout());

        dayReportButton.setText("Reporte del día");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        mainActionsPanel.add(dayReportButton, gridBagConstraints);

        monthReportButton.setText("Reporte del mes");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        mainActionsPanel.add(monthReportButton, gridBagConstraints);

        selectDatesButton.setText("Seleccionar Fechas");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        mainActionsPanel.add(selectDatesButton, gridBagConstraints);

        dateSelectionPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        dateSelectionPanel.add(fromDateChooser, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        dateSelectionPanel.add(toDateChooser, gridBagConstraints);

        fromLabel.setText("Desde:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        dateSelectionPanel.add(fromLabel, gridBagConstraints);

        toLabel.setText("Hasta: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        dateSelectionPanel.add(toLabel, gridBagConstraints);

        reportFromDatesButton.setText("Generar");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        dateSelectionPanel.add(reportFromDatesButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        dateSelectionPanel.add(topSeparator, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        dateSelectionPanel.add(bottomSeparator, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        mainActionsPanel.add(dateSelectionPanel, gridBagConstraints);
        dateSelectionPanel.setVisible(false);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(mainActionsPanel, gridBagConstraints);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSeparator bottomSeparator;
    private javax.swing.JPanel dateSelectionPanel;
    private javax.swing.JButton dayReportButton;
    private com.toedter.calendar.JDateChooser fromDateChooser;
    private javax.swing.JLabel fromLabel;
    private javax.swing.JPanel mainActionsPanel;
    private javax.swing.JButton monthReportButton;
    private javax.swing.JButton reportFromDatesButton;
    private javax.swing.JToggleButton selectDatesButton;
    private com.toedter.calendar.JDateChooser toDateChooser;
    private javax.swing.JLabel toLabel;
    private javax.swing.JSeparator topSeparator;
    // End of variables declaration//GEN-END:variables
   
    public JButton getGenerateDayReportButton(){
        return dayReportButton;
    }
    
    public JButton getGenerateMonthReportButton(){
        return monthReportButton;
    }
    
    public JToggleButton getSelectDatesButton(){
        return selectDatesButton;
    }
    
    public JButton getGenerateReportFromDatesButton(){
        return reportFromDatesButton;
    }
    
    public JDateChooser getDateChooserFrom(){
        return fromDateChooser;
    }
    
    public JDateChooser getDateChooserTo(){
        return toDateChooser;
    }

    public JPanel getDateSelectionPanel() {
        return dateSelectionPanel;
    }
}


package presentation.ui.reports;

import business.reports.SalesReportGenerator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Date;
import javax.swing.JFileChooser;
import presentation.control.status.StatusDialog;
import presentation.exceptions.InvalidDatesSelectionException;
import presentation.exceptions.ReportGeneratioErrorException;
import presentation.ui.reports.ranges.DateRange;
import presentation.ui.reports.ranges.DayDateRange;
import presentation.ui.reports.ranges.MonthDateRange;

public class ReportsWindowHandler implements ActionListener {

    public static String FOLDER_CHOOSER_TITTLE = "Seleccione un directorio destino para el reporte";
    private final ReportsWindow reportsWindow;
    private SalesReportGenerator reportsGenerator;

    public ReportsWindowHandler(ReportsWindow window) {
        this.reportsWindow = window;
        reportsWindow.getGenerateDayReportButton().addActionListener(this);
        reportsWindow.getGenerateMonthReportButton().addActionListener(this);
        reportsWindow.getSelectDatesButton().addActionListener(this);
        reportsWindow.getGenerateReportFromDatesButton().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ( e.getSource() == reportsWindow.getSelectDatesButton() ) {
            boolean isToggleSelected = reportsWindow.getSelectDatesButton().isSelected();
            reportsWindow.getDateSelectionPanel().setVisible(isToggleSelected);
            reportsWindow.pack();
        }
        else {
            handleReportTypeCreation(e);
        }
    }

    private void startReportGeneration(DateRange report) {
        File destinyFolder = requestReportDestinationFolder();
        if ( isValidDestinyFolder(destinyFolder) ) {
            String destinyPath = destinyFolder.getAbsolutePath();
            generateReportIntoDestinyPath(report, destinyPath);
        }
    }

    private void generateReportIntoDestinyPath(DateRange dateRange, String destinyPath) {
        reportsGenerator = new SalesReportGenerator();
        try {
            reportsGenerator.createReport(dateRange.getInitialDate(), dateRange.getFinalDate(), destinyPath);
            showSucessMessageDialog();
        }
        catch ( InvalidDatesSelectionException | ReportGeneratioErrorException ex ) {
            showFailureMessageDialog(ex.getMessage());
        }
    }

    private File requestReportDestinationFolder() {
        JFileChooser destinationFolderChooser = new JFileChooser();
        destinationFolderChooser.setDialogTitle(FOLDER_CHOOSER_TITTLE);
        destinationFolderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        destinationFolderChooser.showOpenDialog(null);
        File destinationFolder = destinationFolderChooser.getSelectedFile();
        return destinationFolder;
    }

    private boolean isValidDestinyFolder(File destinyFolder) {
        return destinyFolder != null;
    }

    private void showFailureMessageDialog(String message) {
        new StatusDialog(reportsWindow, message).setVisible(true);
    }

    private void showSucessMessageDialog() {
        new StatusDialog(
                reportsWindow,
                "Reporte Generado con éxito"
        ).setVisible(true);
    }

    private void handleReportTypeCreation(ActionEvent e) {
        try {
            prepareReportGeneration(e);
        }
        catch ( InvalidDatesSelectionException ex ) {
            showFailureMessageDialog(ex.getMessage());
        }
    }

    private void prepareReportGeneration(ActionEvent e) throws InvalidDatesSelectionException {
        DateRange dateRange = null;

        if ( e.getSource() == reportsWindow.getGenerateDayReportButton() ) {
            dateRange = new DayDateRange();
        }

        if ( e.getSource() == reportsWindow.getGenerateMonthReportButton() ) {
            dateRange = new MonthDateRange();
        }

        if ( e.getSource() == reportsWindow.getGenerateReportFromDatesButton() ) {
            Date dateFrom = reportsWindow.getDateChooserFrom().getDate();
            Date dateTo = reportsWindow.getDateChooserTo().getDate();
            if ( dateFrom == null || dateTo == null ) {
                throw new InvalidDatesSelectionException("Selecciona ambas fechas");
            }
            dateRange = new DateRange(dateFrom, dateTo);
        }

        startReportGeneration(dateRange);
    }

}

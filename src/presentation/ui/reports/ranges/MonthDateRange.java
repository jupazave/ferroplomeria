/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.ui.reports.ranges;

import business.reports.date.DateCalculator;
import java.util.Date;

/**
 *
 * @author luisburgos
 */
public class MonthDateRange extends DateRange {

    public MonthDateRange(){        
        super();        
        initializeMonthReport();     
    }

    private void initializeMonthReport() {
        Date mothBeginning = DateCalculator.getMonthBeginningDate();
        setInitialDate(mothBeginning);
        Date today = DateCalculator.getTodayDate();
        setFinalDate(today);   
    }
    
}

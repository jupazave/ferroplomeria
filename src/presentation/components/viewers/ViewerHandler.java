package presentation.components.viewers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import presentation.control.status.StatusDialog;
import presentation.resources.StringResources;

/**
 *
 * @author luisburgos
 */
public abstract class ViewerHandler implements ActionListener {
    
    protected Viewer viewer;
    protected final static int INVALID_TABLEROW = -1;

    public ViewerHandler(Viewer viewer) {
        this.viewer = viewer;
    }
    
    protected void handleAddActtion(){};    
    protected void handleEditAction(){};
    protected void handleRemoveAction(){}; 
    protected abstract void fillTable();
    
    protected void handleCustomAction(ActionEvent event){
        System.out.println("XXX");
    }
    
    protected void handleSearchAction(){
        System.out.println("Buscar algo...");
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if(event.getSource() == viewer.getButton(Viewer.ADD_BUTTON)){
            handleAddActtion();
        } else 
        
        if(event.getSource() == viewer.getButton(Viewer.REMOVE_BUTTON)){
            if(viewer.getEntitiesTable().getSelectedRow() != INVALID_TABLEROW){
                handleRemoveAction();
            }
        } else 
        
        if(event.getSource() == viewer.getButton(Viewer.EDIT_BUTTON)){
            if(viewer.getEntitiesTable().getSelectedRow() != INVALID_TABLEROW){
                handleEditAction();
            }
        } else
        
        if(event.getSource() == viewer.getSearchButton()){
            handleSearchAction();
        }
        
        else{
            handleCustomAction(event);
        }
    }
    
    protected boolean isValidSearch(String toSearch) {
        return toSearch.length() > 3;
    }

    protected void showInvalidSearchMessage() {
        new StatusDialog(
                viewer, 
                StringResources.getResource(StringResources.ERROR_TITLE)).setVisible(true);
    }

    protected void showSuccessMessageDialog() {
        new StatusDialog(
                viewer, "Exito").setVisible(true);
    }

    protected void showFailureMessageDialog() {
        new StatusDialog(
                viewer, 
                StringResources.getResource(StringResources.ERROR_TITLE)).setVisible(true);
    }    
    
    protected JPanel createSearchPanel(){
        JPanel searchPanel = new JPanel();
        searchPanel.setName("searchPanel"); // NOI18N
        searchPanel.setLayout(new java.awt.GridBagLayout());
        return searchPanel;
    }
    
    protected JTextField createSearchTextField(){
        JTextField searchTextField = new JTextField();
        searchTextField.setText("Buscar");
        searchTextField.setName("searchTextField"); // NOI18N
        return searchTextField;
    }
    
    protected JButton createAddButton(){
        return createButton(StringResources.ADD_TEXT, "addButton", "add.png");
    }
    
    protected JButton createRemoveButton(){
        return createButton(StringResources.REMOVE_TEXT, "removeButton", "trash_can_delete.png");
    }
    
    protected JButton createEditButton(){
        return createButton(StringResources.EDIT_TEXT, "editButton", "edit.png");
    }
    
    protected JButton createSearchButton(){
        return createButton("", "searchButton", "search.png");
    }
    
    protected JButton createButton(String text, String name, String iconFile){
        JButton button = new JButton();
        button.setIcon(new javax.swing.ImageIcon(getClass().getResource(StringResources.ICONS_CLASSPATH + iconFile)));
        button.setText(text);
        button.setName(name);
        button.addActionListener(this);
        return button;
    }
    
}

package presentation.components.viewers;

import java.awt.GridBagConstraints;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import presentation.components.tables.Table;
import presentation.resources.StringResources;

public abstract class Viewer extends JFrame {

    protected ViewerHandler handler;
    private JButton addButton;
    private JButton removeButton;
    private JButton editButton;
    private JPanel searchPanel;
    private JTextField searchTextField;
    private JButton searchButton;
    private ArrayList<JButton> customMenuButtons;
    
    public static final int ADD_BUTTON = 0;
    public static final int EDIT_BUTTON = 1;
    public static final int REMOVE_BUTTON = 2;
    public static final int CUSTOM_BUTTON = 3;
    
    protected Viewer() {
        this(StringResources.VIEWER_DEFAULT_TITLE);
    }
    
    protected Viewer(String title){
        initComponents();
        customMenuButtons = new ArrayList<>();
        entitiesTable.loadDefaultSettings();
        setTitle(title);
    }
    
    public void setTableHeader(String[] tabHeaderContent, boolean[] visibilities){
        entitiesTable.setHeader(tabHeaderContent, visibilities);
    }
    
    public String getSearchText(){
        return searchTextField.getText().trim();
    }

    public ViewerHandler getHandler() {
        return handler;
    }

    public void setHandler(ViewerHandler handler) {
        this.handler = handler;
    }

    public Table getEntitiesTable() {
        return entitiesTable;
    }

    public void setEntitiesTable(Table entitiesTable) {
        this.entitiesTable = entitiesTable;
    }

    public JButton getSearchButton() {
        return searchButton;
    }

    public void setSearchButton(JButton searchButton) {
        this.searchButton = searchButton;
        addSearchButtonToPane(searchButton);
    }

    public JTextField getSearchTextField() {
        return searchTextField;
    }

    public void setSearchTextField(JTextField searchTextField) {
        this.searchTextField = searchTextField;
        addSearchTextFieldToPane(searchTextField);
    }
    
    public void updateEntitiesTable(){
        if(handler!=null){
            handler.fillTable();
        }
    }
    
    public void addButton(JButton button, int button_type){
        switch(button_type){
            case ADD_BUTTON:
                addAddButton(button);
                break;
            case REMOVE_BUTTON:
                addRemoveButton(button);
                break;
            case EDIT_BUTTON:
                addEditButton(button);
                break;
            case CUSTOM_BUTTON:
                addCustomButton(button);
                break;
        }
    }
    
    public JButton getButton(int buttonType){
        switch(buttonType){
            case ADD_BUTTON:
                return getAddButton();
            case REMOVE_BUTTON:
                return getRemoveButton();
            case EDIT_BUTTON:
                return getEditButton();
            default:
                return null;
        }
    }
    
    public JButton getButton(int buttonType, String name){
        switch(buttonType){
            case CUSTOM_BUTTON:
                return getCustomButton(name);
            default:
                return getButton(buttonType);
        }
    }
    
    public void addSearchPanel(JPanel searchPanel, JTextField searchField, JButton searchButton){
        this.searchPanel = searchPanel;
        setSearchTextField(searchField);
        setSearchButton(searchButton);
        
        GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        basePanel.add(searchPanel, gridBagConstraints);
        
    }
    
    public JPanel getSearchPanel(){
        return searchPanel;
    }
    

    @Override
    public void setVisible(boolean b) {
        updateEntitiesTable();
        pack();
        super.setVisible(b);
    }

    //<editor-fold defaultstate="collapsed" desc="NetBeans Auto-Generated Code">   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        basePanel = new javax.swing.JPanel();
        tableScrollPane = new javax.swing.JScrollPane();
        entitiesTable = new presentation.components.tables.Table();
        toolBar = new javax.swing.JToolBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("VISOR DEFAULT");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        basePanel.setName("basePanel"); // NOI18N
        basePanel.setLayout(new java.awt.GridBagLayout());

        tableScrollPane.setName("tableScrollPane"); // NOI18N

        entitiesTable.setName("entitiesTable"); // NOI18N
        tableScrollPane.setViewportView(entitiesTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        basePanel.add(tableScrollPane, gridBagConstraints);

        toolBar.setFloatable(false);
        toolBar.setRollover(true);
        toolBar.setMargin(new java.awt.Insets(2, 0, 2, 0));
        toolBar.setName("toolBar"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        basePanel.add(toolBar, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(basePanel, gridBagConstraints);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel basePanel;
    private presentation.components.tables.Table entitiesTable;
    private javax.swing.JScrollPane tableScrollPane;
    private javax.swing.JToolBar toolBar;
    // End of variables declaration//GEN-END:variables
    //</editor-fold>

    private JButton getAddButton() {
        return addButton;
    }

    private void addAddButton(JButton addButton) {
        this.addButton = addButton;
        toolBar.add(getAddButton());
    }
    
    private JButton getEditButton() {
        return editButton;
    }

    private void addEditButton(JButton editButton) {
        this.editButton = editButton;
        toolBar.add(getEditButton());
    }
    
    private JButton getRemoveButton(){
        return removeButton;
    }

    private void addRemoveButton(JButton removeButton) {
        this.removeButton = removeButton;
        toolBar.add(getRemoveButton());
    }
    
    private void addCustomButton(JButton customButton){
        customMenuButtons.add(customButton);
        toolBar.add(customMenuButtons.get(customMenuButtons.indexOf(customButton)));
    }
    
    private JButton getCustomButton(String name){
        for(JButton current : customMenuButtons){
            if(current.getName() == null ? name == null : current.getName().equals(name)){
                return current;
            }
        }
        return null;
    }
    
    private void addSearchTextFieldToPane(JTextField searchTextField){
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.9;
        gridBagConstraints.weighty = 1.0;
        searchPanel.add(searchTextField, gridBagConstraints);
    }
    
    private void addSearchButtonToPane(JButton searchButton){
        GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 1, 0);
        searchPanel.add(searchButton, gridBagConstraints);
    }
    
}

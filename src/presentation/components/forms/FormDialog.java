
package presentation.components.forms;

import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JFrame;

public abstract class FormDialog extends JDialog {

    public FormDialog(JFrame window, boolean modal) {
        super(window, modal);
    }
    
    public abstract void clearForm(); //How to clean the Dialog
    
    public abstract void submitForm(ActionListener event); //Link Operation to Action

    //public abstract void setEditMode(); //What changes are done on EditMode
    
    public void updateDynamicData() {
        //Implementation on subclasses it's optional, that's why this is empty
    }

}

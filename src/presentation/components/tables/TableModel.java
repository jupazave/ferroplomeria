package presentation.components.tables;

import javax.swing.table.DefaultTableModel;

/**
 * Establishes the table non-visual behavior including the data model management.
 *
 * @author kirebyte
 */
public class TableModel extends DefaultTableModel {

   private boolean editable;

   public TableModel() {
      editable = true;
   }

   //<editor-fold defaultstate="collapsed" desc="Cell editing methods">
   public boolean isEditable() {
      return editable;
   }

   public void setEditable(boolean editable) {
      this.editable = editable;
   }

   @Override
   public boolean isCellEditable(int row, int column) {
      return editable;
   }
   
//</editor-fold>

}

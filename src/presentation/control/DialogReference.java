package presentation.control;

public enum DialogReference {
    CATEGORY_ADDER,
    CATEGORY_EDITOR,
    PRODUCT_ADDER,
    PRODUCT_EDITOR,
    SUPPLIER_ADDER,
    SUPPLIER_EDITOR,
    STOCK_EDITOR,
    SALE_ADDER, //TODO: rename this?
    DEMAND_ADDER,
    DEMAND_EDITOR,
    ORDER_ADDER,
    ESTIMATION_ADDER,
    ORDER_DETAILS,
}

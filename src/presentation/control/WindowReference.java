package presentation.control;

public enum WindowReference {
    MAIN,
    REPORTS,
    CATEGORY_VIEWER,
    INVENTORY_VIEWER,
    SUPPLIER_VIEWER,
    STOCK_VIEWER,
    DEMAND_VIEWER, 
    SALES_VIEWER, //TODO: Reconsider this name
    SALES_ADDER,
    ORDER_VIEWER,
    COST_ESTIMATOR,
}

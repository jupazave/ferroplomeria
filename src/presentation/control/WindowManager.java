
package presentation.control;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.JFrame;
import presentation.ui.category.CategoryFormDialog;
import presentation.ui.product.ProductFormDialog;
import presentation.ui.main.MainWindow;
import presentation.ui.supplier.SupplierFormDialog;
import presentation.ui.category.CategoryViewer;
import presentation.ui.product.ProductViewer;
import presentation.ui.supplier.SupplierViewer;
import presentation.ui.category.CategoryAdderFormModel;
import presentation.ui.category.CategoryEditorFormModel;
import presentation.components.forms.FormModel;
import presentation.ui.demand.DemandFormDialog;
import presentation.ui.demand.DemandAdderFormModel;
import presentation.ui.demand.DemandEditorFormModel;
import presentation.ui.demand.DemandViewer;
import presentation.ui.orders.OrderFormDialog;
import presentation.ui.orders.OrderAdderFormModel;
import presentation.ui.orders.OrderDetailsFormModel;
import presentation.ui.orders.OrderViewer;
import presentation.ui.product.ProductAdderFormModel;
import presentation.ui.product.ProductEditorFormModel;
import presentation.ui.reports.ReportsWindow;
import presentation.ui.sales.SaleFormDialog;
import presentation.ui.sales.SaleAdderFormModel;
import presentation.ui.sales.SalesViewer;
import presentation.ui.stock.StockFormDialog;
import presentation.ui.stock.StockViewer;
import presentation.ui.stock.StockEditorModel;
import presentation.ui.supplier.SupplierAdderFormModel;
import presentation.ui.supplier.SupplierEditorFormModel;
import presentation.ui.valuation.EstimationFormDialog;
import presentation.ui.valuation.EstimationModelAdder;
import presentation.ui.valuation.SaleEstimation;

public class WindowManager {
    private Map<WindowReference, JFrame> windows;
    private Map<DialogReference, FormModel> models;
    private static WindowManager instance;

    private WindowManager() {
        initWindows();
        initDialogs();
    }

    public static WindowManager getInstance() {
        if ( instance == null ) {
            instance = new WindowManager();
        }
        return instance;
    }

    private void initWindows() {
        windows = new ConcurrentHashMap<>();
        windows.put(WindowReference.REPORTS, new ReportsWindow());
        windows.put(WindowReference.CATEGORY_VIEWER, new CategoryViewer());
        windows.put(WindowReference.INVENTORY_VIEWER, new ProductViewer());
        windows.put(WindowReference.SUPPLIER_VIEWER, new SupplierViewer());
        windows.put(WindowReference.STOCK_VIEWER, new StockViewer());
        windows.put(WindowReference.DEMAND_VIEWER, new DemandViewer());
        windows.put(WindowReference.SALES_VIEWER, new SalesViewer());
        windows.put(WindowReference.ORDER_VIEWER, new OrderViewer());
        windows.put(WindowReference.COST_ESTIMATOR, new SaleEstimation());
        windows.put(WindowReference.MAIN, new MainWindow());
    }

    private void initDialogs() {
        models = new ConcurrentHashMap<>();
        //TODO: Buscar una forma de mejorar la lectura de estás lineas.
        models.put(DialogReference.CATEGORY_ADDER, new CategoryAdderFormModel(new CategoryFormDialog(getWindow(WindowReference.CATEGORY_VIEWER), true)));
        models.put(DialogReference.CATEGORY_EDITOR, new CategoryEditorFormModel(new CategoryFormDialog(getWindow(WindowReference.CATEGORY_VIEWER), true)));
        models.put(DialogReference.PRODUCT_ADDER, new ProductAdderFormModel(new ProductFormDialog(getWindow(WindowReference.INVENTORY_VIEWER), true)));
        models.put(DialogReference.PRODUCT_EDITOR, new ProductEditorFormModel(new ProductFormDialog(getWindow(WindowReference.INVENTORY_VIEWER), true)));
        models.put(DialogReference.SUPPLIER_ADDER, new SupplierAdderFormModel(new SupplierFormDialog(getWindow(WindowReference.SUPPLIER_VIEWER), true)));
        models.put(DialogReference.SUPPLIER_EDITOR, new SupplierEditorFormModel(new SupplierFormDialog(getWindow(WindowReference.SUPPLIER_VIEWER), true)));
        models.put(DialogReference.STOCK_EDITOR, new StockEditorModel(new StockFormDialog(getWindow(WindowReference.STOCK_VIEWER), true)));
        models.put(DialogReference.DEMAND_ADDER, new DemandAdderFormModel(new DemandFormDialog(getWindow(WindowReference.DEMAND_VIEWER), true)));
        models.put(DialogReference.DEMAND_EDITOR, new DemandEditorFormModel(new DemandFormDialog(getWindow(WindowReference.DEMAND_VIEWER), true)));
        models.put(DialogReference.ORDER_DETAILS, new OrderDetailsFormModel(new OrderFormDialog(getWindow(WindowReference.ORDER_VIEWER), true)));
        models.put(DialogReference.SALE_ADDER, new SaleAdderFormModel(new SaleFormDialog(getWindow(WindowReference.SALES_VIEWER), true)) {
        });
        models.put(DialogReference.ORDER_ADDER, new OrderAdderFormModel(new OrderFormDialog(getWindow(WindowReference.ORDER_VIEWER), true)) {
        });
        models.put(DialogReference.ESTIMATION_ADDER, new EstimationModelAdder(new EstimationFormDialog(getWindow(WindowReference.COST_ESTIMATOR), true)) {
        });
    }

    public JFrame getWindow(WindowReference windowReference) {
        return windows.get(windowReference);
    }

    public FormModel getForm(DialogReference dialogReference) {
        return models.get(dialogReference);
    }
}

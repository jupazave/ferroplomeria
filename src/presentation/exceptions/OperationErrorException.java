package presentation.exceptions;

import presentation.resources.StringResources;

public class OperationErrorException extends Exception {
    
    public OperationErrorException(){
        this(StringResources.OPERATION_ERROR_EXCEPTION_DEFAULT);
    }
    
    public OperationErrorException(String message) {
        super(message);
    }

    public OperationErrorException(String message, Throwable throwable) {
        super(message, throwable);
    }
    
}

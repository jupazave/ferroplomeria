/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.exceptions;

import presentation.resources.StringResources;

/**
 *
 * @author luisburgos
 */
public class ReportGeneratioErrorException extends Exception {
    
    public ReportGeneratioErrorException(){
        this(StringResources.REPORT_GENERATION_ERROR_EXCEPTION);
    }
    
    public ReportGeneratioErrorException(String message) {
        super(message);
    }

    public ReportGeneratioErrorException(String message, Throwable throwable) {
        super(message, throwable);
    }
    
}
package presentation.exceptions;

import presentation.resources.StringResources;

public class InvalidDatesSelectionException extends Exception {
    
    public InvalidDatesSelectionException(){
        this(StringResources.INVALID_DATE_SAELECTION_EXCEPTION);
    }
    
    public InvalidDatesSelectionException(String message) {
        super(message);
    }

    public InvalidDatesSelectionException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

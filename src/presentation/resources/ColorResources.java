/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.resources;

import java.awt.Color;

/**
 *
 * @author luisburgos
 */
public class ColorResources {
    /* Propuesta:
        final static Color PANE_COLOR = new Color(149,194,255); //BLUE PALE
    */
    public final static Color BLUE_LIGHT = new Color(123,171,238);
    public final static Color BLUE_PLAIN = new Color(43,121,229);
    public final static Color BLUE_PALE = new Color(149,194,255);
    public final static Color WHITE_SNOW = new Color(231,241,255);
}

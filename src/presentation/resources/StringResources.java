package presentation.resources;

public class StringResources {

    //<-- COMMONS --> 
    public static final String ZERO_DOUBLE = "0.0";
    public static final String ZERO_INT = "0";
    public static final String EDIT_TEXT = "Editar";
    public static final String ADD_TEXT = "Añadir";
    public static final String REMOVE_TEXT = "Remover";
    public static final String SELECT = "Seleccionar";
    public static final String DECIMAL_FORMAT = "#.##";
    public static final String ERROR_TITLE = "Error";
    public static final String INFO_TITLE = "Información";
    public static final String VIEWER_DEFAULT_TITLE = "VISTA POR DEFECTO";
    public static final String CATEGORY_VIEWER_TITLE = "Categorias";
    public static final String PRODUCT_VIEWER_TITLE = "Productos e Inventario";
    public static final String SUPPLIER_VIEWER_TITLE = "Provedores";
    public static final String STOCK_VIEWER_TITLE = "Existencias";
    public static final String DEMAND_VIEWER_TITLE = "Faltantes";
    public static final String ORDER_VIEWER_TITLE = "Pedidos";
    public static final String REPORTS_WINDOW_TITLE = "Generación de Reportes de Venta";
    public static final String ICONS_CLASSPATH = "/presentation/resources/icon/";
    public static final String REQUEST_QUANTITY = "Introduzca una cantidad: ";
    
    //<-- EXCEPTIONS -->        
    public static final String INVALID_DATE_SAELECTION_EXCEPTION = "Fechas seleccionas inválidas";
    public static final String REPORT_GENERATION_ERROR_EXCEPTION = "Error al generar report de ventas";
    public static final String OPERATION_ERROR_EXCEPTION_DEFAULT = "Operation not done";
    public static final String ITEM_NOT_FOUND_EXCEPTION = "Item not found";
    public static final String PRODUCT_NOT_FOUND_EXCEPTION = "Product not found";

    //<-- ADD CATEGORY -->
    public static final String ADD_CATEGORY_FAIL_MESSAGE = "Imposible agregar la categoría {0}";   
    public static final String ADD_CATEGORY_SUCCESS_MESSAGE = "Categoría {0} agregada con exito";
    public static final String ADD_CATEGORY_TITLE = "Agregar categoría";
    public static final String ADD_CATEGORY_DEFAULT_NAME = "NOMBRE DE LA CATEGORÍA";
    public static final String ADD_CATEGORY_DEFAULT_DESCRIPTION = "DESCRIPCIÓN DE LA CATEGORÍA";
    
    //<-- EDIT CATEGORY -->
    public static final String EDIT_CATEGORY_SUCCESS_MESSAGE = "Categoría {0} editada con exito";   
    public static final String EDIT_CATEGORY_FAIL_MESSAGE = "Imposible editar el producto {0}";
    public static final String EDIT_CATEGORY_CONFIRMATION_MESSAGE = "¿Realmente desea editar la categoria {0}?";
    public static final String EDIT_CATEGORY_TITLE = "Editar categoría";
    
    //<-- DELETE CATEGORY -->
    public static final String DELETE_CATEGORY_CONFIRMATION_MESSAGE = "¿Realmente deseas borrar la categoría {0} ({1})?"; 
    public static final String DELETE_CATEGORY_SUCCESS_MESSAGE = "Categoría {0} borrada con éxito?"; 
    public static final String DELETE_CATEGORY_FAIL_MESSAGE = "Imposible borrar la categoría {0}";
    public static final String DELETE_CATEGORY_IS_NOT_EMPTY_MESSAGE = "Imposible borrar la categoría {0}. No está vacia";
    
    //<-- ADD PRODUCT -->
    public static final String ADD_PRODUCT_SUCCESS_MESSAGE = "Producto {0} agregado con éxito";
    public static final String ADD_PRODUCT_FAIL_MESSAGE = "Imposible agregar el producto {0}";
    public static final String ADD_PRODUCT_TITLE = "Agregar producto";
    public static final String ADD_PRODUCT_DEFAULT_NAME = "NOMBRE DEL PRODUCTO";
    public static final String ADD_PRODUCT_DEFAULT_DESCRIPTION = "DESCRIPCIÓN DEL PRODUCTO";
    public static final String ADD_PRODUCT_DEFAULT_BRAND = "MARCA DEL PRODUCTO";
    
    //<-- EDIT PRODUCT -->
    public static final String EDIT_PRODUCT_SUCCESS_MESSAGE = "Producto {0} editado con éxito";
    public static final String EDIT_PRODUCT_FAIL_MESSAGE = "Imposible editar el producto {0}";
    public static final String EDIT_PRODUCT_TITLE = "Editar producto"; 
    public static final String EDIT_PRODUCT_CONFIRMATION_MESSAGE = "¿Realmente desea editar el producto {0}?"; 
    
    //<-- DELETE PRODUCT -->
    public static final String DELETE_PRODUCT_CONFIRMATION_MESSAGE = "¿Realmente deseas borrar el producto {0} ({1})?"; 
    public static final String DELETE_PRODUCT_SUCCESS_MESSAGE = "Producto {0} borrado con éxito?"; 
    public static final String DELETE_PRODUCT_FAIL_MESSAGE = "Imposible borrar el producto {0}"; 
    
    //<-- COMMONS PRODUCT -->
    public static final String PRODUCT_COST_ERROR_MESSAGE = "El costo no es correcto";
    public static final String PRODUCT_PRICE_ERROR_MESSAGE = "El precio no es correcto";
    
    //<-- EDIT STOCK -->
    public static final String EDIT_STOCK_TITLE = "Editar existencia";
    public static final String EDIT_STOCK_SUCCESS_MESSAGE = "El producto {0} ahora tiene {1} existencia(s)";
    public static final String EDIT_STOCK_FAIL_MESSAGE = "Imposible actualizar el producto {0}";
    public static final String EDIT_STOCK_CONFIRMATION_MESSAGE = "¿Realmente desea editar el producto %s?" + 
                "\nStock actual: %s | Nuevo Stock: %s";
    
    //<-- ADD SUPPLIER -->
    public static final String ADD_SUPPLIER_SUCCESS_MESSAGE = "Proveedor {0} agregado con éxito";
    public static final String ADD_SUPPLIER_FAIL_MESSAGE = "Imposible agregar el proveedor {0}";
    public static final String ADD_SUPPLIER_TITLE = "Agregar proveedor";
    public static final String ADD_SUPPLIER_DEFAULT_NAME = "NOMBRE DEL PROVEEDOR";
    public static final String ADD_SUPPLIER_DEFAULT_PHONE = "TELEFONO DEL PROVEEDOR";
    public static final String ADD_SUPPLIER_DEFAULT_MAIL = "EMAIL DEL PROVEEDOR";
    public static final String ADD_SUPPLIER_DEFAULT_ADDRESS = "DIRECCION DEL PROVEEDOR";
    
    //<-- EDIT SUPPLIER -->
    public static final String EDIT_SUPPLIER_SUCCESS_MESSAGE = "Proveedor {0} editado con éxito";
    public static final String EDIT_SUPPLIER_FAIL_MESSAGE = "Imposible editar el proveedor {0}";
    public static final String EDIT_SUPPLIER_TITLE = "Editar proveedor"; 
    public static final String EDIT_SUPPLIER_CONFIRMATION_MESSAGE = "¿Realmente desea editar el proveedor {0}?"; 
    
    //<-- DELETE SUPPLIER -->
    public static final String DELETE_SUPPLIER_CONFIRMATION_MESSAGE = "¿Realmente deseas borrar el proveedor {0} ({1})?"; 
    public static final String DELETE_SUPPLIER_SUCCESS_MESSAGE = "Proveedor {0} borrado con éxito?"; 
    public static final String DELETE_SUPPLIER_FAIL_MESSAGE = "Imposible borrar el proveedor {0}"; 
    
    //<-- ADD DEMAND -->
    public static final String ADD_DEMAND_SUCCESS_MESSAGE = "El artículo {0} fue agregado con éxito";
    public static final String ADD_DEMAND_FAIL_MESSAGE = "Imposible agregar el artículo {0}";
    public static final String ADD_DEMAND_TITLE = "Agregar artículo";
    public static final String ADD_DEMAND_DEFAULT_NAME = "NOMBRE DEL ARTÍCULO";
    public static final String ADD_DEMAND_DEFAULT_DESCRIPTION = "DESCRIPCION DEL ARTÍCULO";
    
    //<-- EDIT DEMAND -->
    public static final String EDIT_DEMAND_SUCCESS_MESSAGE = "Artículo {0} editado con éxito";
    public static final String EDIT_DEMAND_FAIL_MESSAGE = "Imposible editar el artículo {0}";
    public static final String EDIT_DEMAND_TITLE = "Editar artículo"; 
    public static final String EDIT_DEMAND_CONFIRMATION_MESSAGE = "¿Realmente desea editar el artículo {0}?"; 
    
    //<-- ADD DEMAND -->
    public static final String ADD_ORDER_SUCCESS_MESSAGE = "La orden con el provedor {0} fue agregada con éxito";
    public static final String ADD_ORDER_FAIL_MESSAGE = "Imposible agregar la orden con el provedor {0}";
    public static final String ADD_ORDER_TITLE = "Agregar orden";
    
    //<-- DELETE SUPPLIER -->
    public static final String DELETE_ORDER_CONFIRMATION_MESSAGE = "¿Realmente deseas borrar el pedido número {0}?"; 
    public static final String DELETE_ORDER_SUCCESS_MESSAGE = "Pedido número {0} borrado con éxito?"; 
    public static final String DELETE_ORDER_FAIL_MESSAGE = "Imposible borrar el pedido {0}"; 
    
    public static String getResource(String resource, String... arguments){
        int counter = 0;
        for(String currentArgument : arguments){
            resource = resource.replace("{" + String.valueOf(counter) + "}", currentArgument);
            counter++;
        }
        return resource;
    }
}

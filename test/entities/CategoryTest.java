/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Dummy unit test
 * @author luisburgos
 */
public class CategoryTest {
    
    private Category category;
    
    @Before
    public void initCategory(){
        category = new Category();
        category.setId(1);
        category.setName("Clavos");
        category.setDescription("Clavos de media pulgada");
    }
    
    @Test
    public void categoryNameEqualsClavos(){
        assertEquals("Clavos", category.getName());
    }
    
}

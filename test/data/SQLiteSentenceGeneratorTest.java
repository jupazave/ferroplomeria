/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import data.daos.CategoryDAO;
import data.daos.ProductDAO;
import entities.Category;
import entities.Product;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author luisburgos
 */
public class SQLiteSentenceGeneratorTest {
    
    /*SQLSentenceGenerator builder;
    Category category;
    Category categoryToUpdate;
    Product product;
    
    @Before
    public void initCategory(){
        category = new Category("Clavos", "Media pulgada");               
        
        categoryToUpdate = new Category("Nuevo Nombre", "Nueva descripcion");       
        categoryToUpdate.setId(1);
               
        product = new Product(
                "Clavos", 
                "Media pulgada", 
                "Truper", 
                12.56, 
                20.52, //TODO: Verify when 20.50
                100, 
                category
        );
        
    }
    
    @Test
    public void insertQueryForCategoryObject(){   
        builder = new SQLSentenceGenerator(CategoryDAO.TABLE_CATEGORY);
        String matchQuery = "INSERT into "
                + CategoryDAO.TABLE_CATEGORY
                + " (name, description)"
                + " values "
                + "(\"Clavos\""
                + ", \"Media pulgada\")"
                + ";";
        
        String generated = builder.createInsertSentence(category);
        
        System.out.println(matchQuery.toUpperCase());
        System.out.println(generated.toUpperCase());
        assertEquals(matchQuery.toUpperCase(), generated.toUpperCase());
    }
    
    @Test
    public void insertQueryForProductObject(){      
        builder = new SQLSentenceGenerator(ProductDAO.TABLE_PRODUCTS);
        String matchQuery = "INSERT into "
                + ProductDAO.TABLE_PRODUCTS
                + " (name, description, brand, cost, price, stock, category)"
                + " values "
                + "(\"Clavos\""
                + ", \"Media pulgada\""
                + ", \"Truper\""
                + ", \"12.56\""
                + ", \"20.52\""
                + ", \"100\")"                
                + ";";
        
        String generated = builder.createInsertSentence(product);        
        System.out.println(matchQuery.toUpperCase());
        System.out.println(generated.toUpperCase());
        
        assertEquals(matchQuery.toUpperCase(), generated.toUpperCase());
    }
    
    @Test
    public void deleteQueryForCategory(){
        builder = new SQLSentenceGenerator(CategoryDAO.TABLE_CATEGORY);
        String matchQuery = "DELETE FROM "
                + CategoryDAO.TABLE_CATEGORY
                + " WHERE "
                + "id=1"
                + ";";
        
        String generated = builder.createDeleteByIdSentence(1);        
        System.out.println(matchQuery.toUpperCase());
        System.out.println(generated.toUpperCase());
        
        assertEquals(matchQuery.toUpperCase(), generated.toUpperCase());
    }    
    
    @Test
    public void findCategoryById(){
        builder = new SQLSentenceGenerator(CategoryDAO.TABLE_CATEGORY);
        String matchQuery = "SELECT " + "*"
                + " FROM "
                + CategoryDAO.TABLE_CATEGORY
                + " WHERE "
                + "id=1"
                + ";";
        
        String generated = builder.createFindByIdSentence(1);        
        System.out.println(matchQuery.toUpperCase());
        System.out.println(generated.toUpperCase());
        
        assertEquals(matchQuery.toUpperCase(), generated.toUpperCase());
    }
    
    @Test
    public void selectAllFromCategoryTable(){
        builder = new SQLSentenceGenerator(CategoryDAO.TABLE_CATEGORY);
        String matchQuery = "SELECT " + "*"
                + " FROM "
                + CategoryDAO.TABLE_CATEGORY
                + ";";
        
        String generated = builder.createSelectAllFromTableSentence();
        System.out.println(matchQuery.toUpperCase());
        System.out.println(generated.toUpperCase());
        
        assertEquals(matchQuery.toUpperCase(), generated.toUpperCase());
    }
    
    @Test
    public void selectAllFromProductTable(){
        builder = new SQLSentenceGenerator(ProductDAO.TABLE_PRODUCTS);
        String matchQuery = "SELECT " + "*"
                + " FROM "
                + ProductDAO.TABLE_PRODUCTS
                + ";";
        
        String generated = builder.createSelectAllFromTableSentence();
        System.out.println(matchQuery.toUpperCase());
        System.out.println(generated.toUpperCase());
        
        assertEquals(matchQuery.toUpperCase(), generated.toUpperCase());
    }
    
    @Test
    public void updateCategory(){
        builder = new SQLSentenceGenerator(CategoryDAO.TABLE_CATEGORY);
        String matchQuery = "UPDATE " + CategoryDAO.TABLE_CATEGORY
                + " SET "
                + "name=Nuevo Nombre" + ", "
                + "description=Nueva Descripcion"
                + " WHERE "
                + "id="
                + ";";
        
        String generated = builder.createUpdateEntitySentence(categoryToUpdate);
        System.out.println(matchQuery.toUpperCase());
        System.out.println(generated.toUpperCase());
        
        assertEquals(matchQuery.toUpperCase(), generated.toUpperCase());
    }*/
    
}
